%% guideline: https://www.nature.com/srep/publish/guidelines#cover-letter
%% https://www.nature.com/srep/publish/checklist
% TODO: fix bib references
% TODO: fix references

\documentclass[fleqn,10pt]{wlscirep}
\usepackage{subfig}
\usepackage{hyperref} 
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{pgfplots}
\usepackage{pdflscape}

% Hurenkinder und Schusterjungen verhindern
\clubpenalty10000
\widowpenalty10000
\displaywidowpenalty=10000

\title{Comparison of Deep Learning Approaches for Multi-Label Chest X-Ray Classification}

\author[1,2,$\bigstar$]{Ivo M. Baltruschat}
\author[3]{Hannes Nickisch}
\author[3]{Michael Grass}
\author[1,2]{Tobias Knopp}
\author[3]{Axel Saalbach}
\affil[1]{Section for Biomedical Imaging, University Medical Center Hamburg-Eppendorf, Hamburg, Germany}
\affil[2]{Institute for Biomedical Imaging, Hamburg University of Technology, Hamburg, Germany}
\affil[3]{Philips Research, Hamburg, Germany}

\affil[$\bigstar$]{i.baltruschat@uke.de}


%\keywords{Keyword1, Keyword2, Keyword3}

\begin{abstract}
	The increased availability of X-ray image archives (e.g. the ChestX-ray14 dataset from the NIH Clinical Center) has triggered a growing interest in deep learning techniques. To provide better insight into the different approaches, and their applications to chest X-ray classification, we investigate a powerful network architecture in detail: the ResNet-50. Building on prior work in this domain, we consider transfer learning with and without fine-tuning as well as the training of a dedicated X-ray network from scratch. To leverage the high spatial resolutions of X-ray data, we also include an extended ResNet-50 architecture, and a network integrating non-image data (patient age, gender and acquisition type) in the classification process.
	
	In a systematic evaluation, using 5-fold re-sampling and a multi-label loss function, we evaluate the performance of the different approaches for pathology classification by ROC statistics and analyze differences between the classifiers using rank correlation. Furthermore, we demonstrate one major problem of the ChestX-ray14 dataset by analyzing our models with Grad-CAM. We observe a considerable spread in the achieved performance and conclude that the X-ray-specific ResNet-50, integrating non-image data yields the best overall results.
\end{abstract}
\begin{document}
	
	\flushbottom
	\maketitle
	% * <john.hammersley@gmail.com> 2015-02-09T12:07:31.197Z:
	%
	%  Click the title above to edit the author information and abstract
	%
	\thispagestyle{empty}
	
	\section{Introduction}
	\begin{figure}[hb]
		\centering
		\subfloat["No Finding"]{\includegraphics[width=0.24\textwidth]{fig/00000005_003_no_finding.png}}\hfill
		\subfloat["Cardiomegaly"]{\includegraphics[width=0.24\textwidth]{fig/00000001_000_cardiomegaly.png}}\hfill
		\subfloat["Pneumothorax"]{\includegraphics[width=0.24\textwidth]{fig/00004876_004_real-pneumo.png}}\hfill
		\subfloat["Pneumothorax"]{\includegraphics[width=0.24\textwidth]{fig/00000041_002_pneumo_with_drain.png}}
		%\vspace{-0.1cm}
		\caption[Titel des Bildes]{Four examples of the ChestX-ray14 dataset consisting of 112,120 frontal chest X-rays from 30,805 patients. All images are labelled with up to 14 pathologies or "No Finding". The dataset does not only include acute findings, as the pneumothorax in figure (c), but also treated patients with a drain as "pneumothorax" (d).}
		\label{fig:cxr14}
	\end{figure}
	\label{sec:intro}
	In the United Kingdom, the care quality commission recently reported that -- over the preceding 12 months -- a total of 23,000 chest X-rays (CXRs) were not formally reviewed by a radiologist or clinician at Queen Alexandra Hospital alone. Furthermore, three patients with lung cancer suffered significant harm because their CXRs had not been properly assessed \cite{CQC2017}. The Queen Alexandra Hospital is probably not the only hospital having problems with providing expert readings for every CXR. Increasing populations and life expectancies, is expected to drive an increase in demand for CXR readings. 
	
	In computer vision, deep learning has already shown its power for image classification with superhuman accuracy \cite{Krizhevsky2012,Szegedy2015,Simonyan2014,He2016}. In addition, the medical image processing field is vividly exploring deep learning. However, one major problem in the medical domain is the availability of large datasets with reliable ground-truth annotation. 
	\newpage
	Two larger X-ray datasets have recently become available: the CXR dataset from Open-i \cite{Demner-Fushman2015} and ChestX-ray14 from the National Institutes of Health (NIH) Clinical Center \cite{Wang2017}. Figure~\ref{fig:cxr14} illustrates four selected examples from ChestX-ray14. Due to its size, the ChestX-ray14 consisting of 112,120 frontal CXR images from 30,805 unique patients attracted considerable attention in the deep learning community. Triggered by the work of Wang et al. \cite{Wang2017} using convolution neural networks (CNNs) from the computer vision domain, several research groups have begun to address the application of CNNs for CXR classification. In the work of Yao et al.\cite{Yao2017}, they presented a combination of a CNN and a recurrent neural network to exploit label dependencies. As a CNN backbone, they used a DenseNet \cite{Huang2017} model which was adapted and trained entirely on X-ray data. Li et al. \cite{Li2017} presented a framework for pathology classification and localization using CNNs. More recently, Rajpurkar et al. \cite{Rajpurkar2017} proposed transfer-learning with fine tuning, using a DenseNet-121 \cite{Huang2017} and raised the AUC results on ChestX-ray14 for multi-label classification even higher. 
	
	Unfortunately comparison of approaches remains difficult. Most reported results were obtained with differing experimental setups. This includes (among others) the employed network architecture, loss function and data augmentation. In addition, differing dataset splits were used and only Li et al. \cite{Li2017} reported 5-fold cross-validated results. In contrast to these results, our experiments (Sec.~\ref{sec:empirical}) demonstrate that performance of a network depends significantly on the selected split.
	
	Henceforth, to provide better insights into the effects of distinct design decisions for deep learning, we perform a systematic evaluation using a 5-fold re-sampling scheme. We empirically analyze three major topics:
	\begin{enumerate}
		\item weight initialization, pre-training and transfer learning (Section~\ref{sec:weight})
		\item network architectures such as ResNet-50 with large input size (Section~\ref{sec:archi})
		\item non-image features such as age, gender, and view position (Section~\ref{sec:meta})
	\end{enumerate}
	Prior work on ChestX-ray14 has been limited to the analysis of image data. In clinical practice however, radiologists employ a broad range of additional features during the diagnosis. To leverage the complete information of the dataset (i.e. age, gender, and view position), we propose in Section~\ref{sec:meta} a novel architecture integrating this information in addition to the learned image representation.
	
	\section{Methods}
	\label{sec:methods}
	%(with the probability distribution $P(X)$) 
	In the following, we consider pathology detection as a multi-label classification problem. All images $X = \{\vec{x}_1, \dots, \vec{x}_N\}, \vec{x}_i \in \mathcal{X}$ are associated with a ground truth label $\vec{y}_i$, while we seek a classification function $\vec{f}: \mathcal{X} \rightarrow \mathcal{Y}$ that minimizes a specific loss function $\ell$ using $N$ training sample-label pairs $(\vec{x}_i,\vec{y}_i), i=1 \dots N$. Here, we encode the label for each image as a binary vector $\vec{y} \in \{0,1\}^M = \mathcal{Y}$  (with $M$ labels). We encode "No Finding" as an explicit additional label and hence have $M = 15$ labels. After initial investigation of weighting loss functions such as positive/negative balancing \cite{Wang2017} and class balancing, we noticed no significant differences and decided to employ the class-averaged binary cross entropy as a loss function:
	\begin{equation}
	\ell (\vec{y}, \vec{f})= \frac{1}{M}\sum_{m=1}^{M} H[y_{m}, f_{m}], \; \text{with} \; H[y,f] = -y \log f -(1-y)\log (1-f).
	\end{equation}
	
	Prior work on the ChestX-ray14 dataset focused primarily on ResNet-50 and DenseNet-121 architectures. Due to its outstanding performance in the computer vision domain \cite{Huang2017}, we focus in our experiments on the ResNet-50 architecture \cite{He2016a}. To adapt the network to the new task, we replace the last dense layer of the original architecture with a new dense layer matching the number of labels and add a sigmoid activation function.
	
	\subsection{Weight Initialization and Transfer Learning}
	\label{sec:weight}
	We investigate two distinct initialization strategies for the ResNet-50. First, we follow the scheme described in \cite{He2016}, where the network parameters are initialized with random values and thus the model is trained from scratch. Second, we initialize the network with pre-trained weights, where knowledge is gained in a different domain and task. Furthermore, we distinguish between \textit{off-the-shelf} (OTS) and \textit{fine-tuning} (FT) in the transfer-learning approach.
	
	A major drawback in medical image processing with deep learning is the limited size of datasets compared to the computer vision domain. Hence, training a CNN from scratch is often not feasible. One solution is transfer-learning. Following the notation in the work of Pan et al. \cite{Pan2010}, a source domain $\mathcal{D}_s = \{\mathcal{X}_s, P_s(X_s)\}$ with task $\mathcal{T}_s = \{\mathcal{Y}_s, f_s(\cdot)\}$ and a target domain $\mathcal{D}_t = \{\mathcal{X}_t, P_t(X_t)\}$ with task $\mathcal{T}_t = \{\mathcal{Y}_t, f_t(\cdot)\}$ are given with $\mathcal{D}_s\neq\mathcal{D}_t$ and/or $\mathcal{T}_s \neq \mathcal{T}_t$. 
	In transfer-learning, the knowledge gained in $\mathcal{D}_s$ and $\mathcal{T}_s$ is used to help learning of the prediction function $f_t(\cdot)$ in $\mathcal{D}_t$. 
	
	Employing an off-the-shelf approach \cite{Yosinski2014, SharifRazavian2014}, the pre-trained network is used as a feature extractor, and only the weights of the last (classifier) layer are adapted. In fine-tuning, one chooses to re-train one or more layers with samples from the new domain. For both approaches, we use the weights of a ResNet-50 network trained on ImageNet as a starting point \cite{Russakovsky2015}. 
	
	\subsection{Architectures}
	\label{sec:archi}
	In addition to the original ResNet-50 architecture, we employ two variants. First, we reduce the number of input channels to one (the ResNet-50 is designed for the processing of RGB images from the ImageNet dataset), which should facilitate the training of an X-ray specific CNN. Second, we increase the input size by a factor of two (i.e. $448 \times 448$). To keep the model architectures similar, we only add a new pooling layer after the first bottleneck block.
	In particular for the detection of small structures, which could be indicative of a pathology (e.g. masses and nodules), a higher effective resolution could be beneficial.
	
	\subsection{Non-Image Features}
	\label{sec:meta}
	ChestX-ray14 contains information about the patient age, gender, and view position (i.e. if the X-ray image is acquired posterior-anterior (PA) or anterior-posterior (AP)). Radiologists use information beyond the image to conclude which pathologies are present or not. 
	The view position changes the expected position of organs in the X-ray images (i.e. PA images are horizontally flipped compared to AP). In addition, organs (e.g. the heart) are magnified in an AP projection as the distance to the detector is increased.
	
	As illustrated in Figure~\ref{fig:model}, we concatenate the image feature vector (i.e. output of the last pooling layer with dimension $2024 \times 1$) with the new non-image feature vector (with dimension $3 \times 1$). Therefore, view position and gender is encoded as $\{0, 1\}$ and the age is linearly scaled $[\text{min}({X}_{\text{pa}}), \text{max}({X}_{\text{pa}})] \mapsto [0, 1]$, in order to avoid a bias towards features with a large range of values. 
	
	\begin{figure}[t]
		%\vspace{-1.5em}
		\begin{center}
			\includegraphics[width=\textwidth]{fig/model.pdf}
		\end{center}
		
		\caption{Patient-data adapted model architecture. Our architecture is based on the ResNet50 model. Because of the enlarged input size, we added a max polling layer after the first ResBlock. In addition, we fused image features and patient features at the end of our model to incorporate patient information.}
		\label{fig:model}
	\end{figure}
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\section{Experiments and Results}
	\label{sec:empirical}
	To evaluate our approaches for multi-label pathology classification, the entire corpus of ChestX-ray14 (Figure~\ref{fig:cxr14}) is employed. The dataset does not include the original DICOM images but Wang et al. \cite{Wang2017} performed a simple preprocessing where the intensity range was rescaled from a higher bit-depth down to 8-bit. In addition, each image was resized to $1024\times1024$ pixel without preserving the aspect ratio.
	
	For an assessment of the generalization performance, we perform a 5 times re-sampling scheme \cite{Molinaro2005}. Within each split, the data is divided into 70\% training, 10\% validation, and 20 \% testing. When working with deep learning, hyper-parameters, and tuning without a validation set and/or cross-validation can easily result in over-fitting. Since individual patient have multiple follow-up acquisitions, all data from a patient is assigned to a subset only. This leads to a large patient number diversity (e.g. split two has 5,817 patients and 22,420 images whereas split 5 has 6,245 patients and the same number of images). We estimate the average validation loss over all re-samples to determine the best models. Finally, our results are calculated for each fold on the test set and averaged afterwards.	
	
	\noindent \textbf{Implementation:} In all experiments, we use a fixed setup. To extend ChestX-ray14, we use the same geometric data augmentation as in the work of Szegedy et al. \cite{Szegedy2015}. At training, we sample various sized patches of the image with sizes between $8\%$ and $100\%$ of the image area. The aspect ratio is distributed evenly between $3:4$ and $4:3$. In addition, we employ random rotation between $\pm7^{\circ}$ and horizontal flipping. For validation and testing, we rescale images to $256 \times 256$ and $480 \times 480$ for small and large spatial size, respectively. Afterwards, we use the center crop as input image. As in the work of He et al. \cite{He2016}, dropout is not employed \cite{Srivastava2014}. As optimizer, we use ADAM \cite{Kingma2015} with default parameters for $\beta_1 = 0.9$ and $\beta_2 = 0.999$. The learning rate $lr$ is set to $lr = 0.001$ and $lr = 0.01$ for transfer-learning and from scratch, respectively. While training, we reduce the learning rate by a factor of 2 when the validation loss does not improve. Due to model architecture variations, we use batch sizes of 16 and 8 for transfer-learning and from scratch with a large input size, respectively. The models are implemented in CNTK and trained on GTX 1080 GPUs yielding a processing time of around $10 \text{ms}$ per image.
	
	\noindent \textbf{Results:} Table~\ref{tab:compare} summarizes the outcome of our evaluation and we show state-of-the-art reference results in Figure~\ref{fig:compare}. In total, we evaluate eight different experimental setups, with varying weight initialization schemes and network architectures as well as with and without non-image features. We perform a ROC analysis using the area under the curve (AUC) for all pathologies, compare the classifier scores by Spearman's pairwise rank correlation coefficient, and employed the state-of-the-art method Gradient-weighted Class Activation Mapping (Grad-CAM) \cite{Selvaraju2017} to get more insight into our CNNs. Grad-CAM is a method for visual explanations for predictions of CNN models. The method highlights important regions in the input image for a specific classification results by using the gradient of the final convolutional layer.
	
	The results indicate a high variability of the outcome with respect to the selected dataset split. Especially for "Hernia", which is the class with the smallest number of positive samples, we observe a standard deviation of up to 0.05. As a result, an assessment of existing approaches and comparing their performance is difficult, since prior work focused mostly on a single (random) split. 
	
	With respect to the different initialization schemes we observe already reasonable results for OTS networks that are optimized on natural images. Using fine-tuning techniques, the results are improved considerably, from $0.730$ to $0.819$ AUC on average. A complete training of the ResNet-50 using CXRs results in a rather comparable performance. Only the high-resolution variant of the ResNet-50 outperforms the FT approach by $0.002$ on average AUC. In particular, for smaller pathologies like masses and nodules an improvement is observed (i.e. 0.017 and 0.006 AUC increase, respectively), while for other pathologies a similar, or slightly lower performance is estimated.
	
	Finally, all our experiments with non-image features slightly increase the AUC on average to its counterpart (i.e. without non-image feature). Our from scratch trained ResNet-50 with an enlarged input size and integrated non-image data yields the best overall performance with $0.822$ average AUC.
	
	Furthermore, the similarity between the trained models in terms of their predictions was investigated. Therefore, Spearman’s rank correlation coefficient was computed for the predictions of all model pairs, and averaged over the folds. The pairwise correlations coefficients for the models are given in Table~\ref{tab:rankCorr}. Based on the degree of correlation, three groups can be identified. First, we note that the "from scratch models" (i.e. "1channel" and "large) without non-image features have the highest correlation of 0.93 amongst each other, followed by the fine-tuned models with 0.81 and 0.80 for "1channel" and "large", respectively. Second, the OTS model surprisingly has higher correlation with the from scratch models than the fine-tuned model. Third, for models with non-image feature, no such correlation is observed and their value is between 0.32 to 0.47. This indicates that models which have been trained exclusively on X-ray data achieve not only the highest accuracy, but are furthermore most consistent.
	
	While our proposed network architecture achieves high AUC values in all categories of the ChestX-ray14 dataset, the applicability of such a technology in a clinical environment depends considerably on the availability of data for model training and evaluation. In particular for the NIH dataset the reported label noise\cite{Wang2017} and the medical interpretation of the label are an important issue. As mention by Luke Oakden-Rayner \cite{Luke2017}, the class "pneumothorax" is often labeled for already treated cases (i.e. a drain is visible in the image which is used to tread the pneumothorax) in the ChestX-ray14 dataset. We employ Grad-CAM to get an insight, if our trained CNN picked up the drain as a main feature for "pneumothorax". Grad-CAM visualizes the areas which are most responsible for the final prediction as a heatmap. In Figure~\ref{fig:grad-cam}, we show two examples of our testset where the highest activations are around the drain. This indicates that the network learned not only to detect an acute pneumothorax but also the presence of chest drains. Therefore, the utility of the ChestXray14 dataset for the development of clinical applications is still an open issue.
	
	\begin{figure}[b]
		\centering
		\begin{minipage}{0.75\textwidth}
			\includegraphics[width=0.49\textwidth]{fig/00000827_019_pneumo0_cut.png}
			\quad
			\includegraphics[width=0.403\textwidth]{fig/00000827_019-pneumo0.jpg}
			\vspace{1em}
		\end{minipage}
		
		\begin{minipage}{0.75\textwidth}
			\includegraphics[width=0.49\textwidth]{fig/00029807_002_pneumo0_cut.png}
			\quad
			\includegraphics[width=0.403\textwidth]{fig/00029807_002-pneumo0.jpg}
		\end{minipage}
		\caption{Grad-CAM result for two example images. In both examples, the highest activation, which was responsible for the final predication "pneumothorax", is at the drain. This indicates that our trained CNN picked up drains as a main feature for "pneumothorax".}
		\label{fig:grad-cam}
	\end{figure}
	
	%\begin{landscape}
	\begin{table}
		\setlength{\tabcolsep}{5pt}
		\centering
		\caption{AUC result overview for all our experiments. In this table, we present averaged results over all five splits and the calculated standard deviation (std) for each pathology. We divide our experiments into three categories. First, without and with non-image features. Second, transfer-learning with off-the-shelf (OTS) and fine-tuned (FT) models. Third, from scratch where "1channel" refers to same input size as in transfer-learning but changed number of channels. "large" means we changed the input dimensions to $448 \times 448 \times 1$. For better comparison, we present the average AUC and the standard deviation over all pathologies in the last row. Bold text emphasizes the overall highest AUC value. Values are scaled by 100 for convenience.}
		\label{tab:compare}
		\begin{tabular}{l c c c c | c c c c }
			\noalign{\smallskip}
			& \multicolumn{4}{c|}{Without non-image features} & \multicolumn{4}{c}{With non-image features} \\
			\textbf{Pathology} & OTS & FT & 1channel & large & OTS & FT & 1channel & large \\
			\noalign{\smallskip}
			\hline
			\noalign{\smallskip}	
			\textbf{Cardiomegaly}
			& $72.7 \pm 1.8$
			& $88.5 \pm 0.7$
			& $88.9 \pm 0.5$
			& $89.7 \pm 0.3$
			& $75.9 \pm 1.4$
			& $88.4 \pm 0.8$
			& $\mathbf{90.2 \pm 0.4}$
			& $89.8 \pm 0.8$ \\
			
			\textbf{Emphysema}
			& $77.8 \pm 2.1$
			& $89.2 \pm 1.0$
			& $87.0 \pm 0.8$
			& $88.3 \pm 1.3$
			& $79.8 \pm 1.9$
			& $\mathbf{89.4 \pm 1.2}$
			& $87.4 \pm 1.3$
			& $89.1 \pm 1.2$ \\
			
			\textbf{Edema}
			& $84.4 \pm 0.6$
			& $\mathbf{89.1 \pm 0.4}$
			& $\mathbf{89.1 \pm 0.6}$
			& $88.8 \pm 0.5$
			& $85.7 \pm 0.5$
			& $\mathbf{89.1 \pm 0.7}$
			& $89.0 \pm 0.6$
			& $88.9 \pm 0.3$ \\
			
			\textbf{Hernia}
			& $78.8 \pm 1.4$
			& $85.5 \pm 3.8$
			& $88.1 \pm 4.2$
			& $87.5 \pm 4.5$
			& $81.9 \pm 2.5$
			& $88.2 \pm 3.2$
			& $89.3 \pm 4.4$
			& $\mathbf{89.6 \pm 4.4}$ \\
			
			\textbf{Pneumothorax}
			& $77.3 \pm 1.3$
			& $\mathbf{87.0 \pm 0.8}$
			& $85.7 \pm 0.9$
			& $85.9 \pm 0.9$
			& $79.1 \pm 1.2$
			& $86.5 \pm 0.6$
			& $85.4 \pm 0.7$
			& $85.9 \pm 1.1$ \\
			
			\textbf{Effusion}
			& $79.4 \pm 0.4$
			& $87.1 \pm 0.2$
			& $\mathbf{87.6 \pm 0.2}$
			& $\mathbf{87.6 \pm 0.2}$
			& $80.6 \pm 0.4$
			& $87.2 \pm 0.3$
			& $\mathbf{87.6 \pm 0.2}$
			& $87.3 \pm 0.3$ \\
			
			\textbf{Mass}
			& $66.8 \pm 0.6$
			& $82.2 \pm 1.0$
			& $83.3 \pm 0.6$
			& $\mathbf{83.9 \pm 0.9}$
			& $68.6 \pm 0.6$
			& $82.2 \pm 1.0$
			& $83.3 \pm 0.7$
			& $83.2 \pm 0.3$ \\
			
			\textbf{Fibrosis}
			& $72.0 \pm 0.9$
			& $\mathbf{80.0 \pm 0.9}$
			& $79.9 \pm 0.8$
			& $79.2 \pm 1.6$
			& $73.9 \pm 0.8$
			& $\mathbf{80.0 \pm 0.9}$
			& $79.6 \pm 0.5$
			& $78.9 \pm 0.5$ \\
			
			\textbf{Atelectasis}
			& $71.8 \pm 0.6$
			& $\mathbf{80.3 \pm 0.7}$
			& $79.9 \pm 0.4$
			& $79.2 \pm 0.7$
			& $73.2 \pm 0.7$
			& $80.1 \pm 0.6$
			& $79.3 \pm 0.6$
			& $79.1 \pm 0.4$ \\
			
			\textbf{Consolidation}
			& $74.3 \pm 0.3$
			& $79.5 \pm 0.5$
			& $\mathbf{80.6 \pm 0.4}$
			& $80.0 \pm 0.3$
			& $75.3 \pm 0.3$
			& $79.6 \pm 0.5$
			& $80.4 \pm 0.5$
			& $80.0 \pm 0.7$ \\
			
			\textbf{Pleural Thicken.}
			& $68.8 \pm 1.0$
			& $\mathbf{79.0 \pm 0.7}$
			& $78.4 \pm 0.9$
			& $78.0 \pm 1.1$
			& $70.8 \pm 1.1$
			& $78.6 \pm 1.1$
			& $78.2 \pm 1.3$
			& $77.1 \pm 1.3$ \\
			
			\textbf{Nodule}
			& $65.0 \pm 0.8$
			& $72.6 \pm 0.9$
			& $73.3 \pm 0.8$
			& $75.1 \pm 1.3$
			& $66.5 \pm 0.7$
			& $74.7 \pm 0.6$
			& $74.0 \pm 0.7$
			& $\mathbf{75.8 \pm 1.4}$ \\
			
			\textbf{Pneumonia}
			& $66.4 \pm 2.7$
			& $74.4 \pm 1.6$
			& $74.3 \pm 1.5$
			& $75.3 \pm 2.2$
			& $68.3 \pm 2.3$
			& $73.3 \pm 1.3$
			& $74.8 \pm 1.5$
			& $\mathbf{76.7 \pm 1.5}$ \\
			
			\textbf{Infiltration}
			& $65.9 \pm 0.2$
			& $69.9 \pm 0.6$
			& $\mathbf{70.2 \pm 0.3}$
			& $\mathbf{70.2 \pm 0.5}$
			& $67.0 \pm 0.4$
			& $\mathbf{70.2 \pm 0.2}$
			& $70.1 \pm 0.5$
			& $70.0 \pm 0.7$\\
			
			\hline
			\noalign{\smallskip}
			
			\textbf{Average}
			& $73.0 \pm 1.1$
			& $81.7 \pm 1.0$
			& $81.9 \pm 0.9$
			& $82.1 \pm 1.2$
			& $74.8 \pm 1.1$
			& $82.0 \pm 0.9$
			& $82.0 \pm 1.0$
			& $\mathbf{82.2 \pm 1.1}$\\
			
			\textbf{No Findings}
			& $71.6 \pm 0.3$
			& $76.9 \pm 0.5$
			& $\mathbf{77.3 \pm 0.3}$
			& $77.1 \pm 0.4$
			& $72.5 \pm 0.3$
			& $76.8 \pm 0.4$
			& $77.1 \pm 0.4$
			& $77.1 \pm 0.3$\\
			
		\end{tabular}
	\end{table}
	%\end{landscape}
	\begin{table}
		\centering
		\caption{Spearman's rank correlation coefficient is calculated between all model pairs and is averaged over all five splits. Our experiments are grouped into three categories. First, "Without" and "With" non-image features. Second, transfer-learning with off-the-shelf (OTS) and fine-tuned (FT) models. Third, from scratch where "1channel" refers to same input size as in transfer-learning but changed number of channels. "large" means we changed the input dimensions to $448 \times 448 \times 1$. We identify three clusters: all models under "With", models trained from scratch and "Without", and the "OTS" model.}
		\label{tab:rankCorr}
		\begin{tabular}{l l c c c c c c c c }
			\noalign{\smallskip}
			& & \multicolumn{4}{c}{Without} & \multicolumn{4}{c}{With} \\
			& & OTS & FT & 1channel & large & OTS & FT & 1channel & large \\
			\noalign{\smallskip}
			\hline
			\noalign{\smallskip}
			\multirow{4}{*}{Without\;}&
			OTS            & -  & 0.65      & 0.74   & 0.73      & 0.46     & 0.38           & 0.40        & 0.59\\
			&FT             & 0.65  & -      & 0.81   & 0.80      & 0.38     & 0.42           & 0.43        & 0.64\\
			&1channel       & 0.74  & 0.81      & -   & 0.93      & 0.41     & 0.43           & 0.47        & 0.71\\
			&large          & 0.73  & 0.80      & 0.93   & -      & 0.40     & 0.43           & 0.47        & 0.71\\
			%\noalign{\smallskip}
			\multirow{4}{*}{With}&
			OTS            & 0.46  & 0.38      & 0.41   & 0.40      & -     & 0.32           & 0.33        & 0.39\\
			&FT             & 0.38  & 0.42      & 0.43   & 0.43      & 0.32     & -           & 0.35        & 0.42\\
			&1channel       & 0.40  & 0.43      & 0.47   & 0.47      & 0.33     & 0.35           & -        & 0.45\\
			&large          & 0.59  & 0.64      & 0.71   & 0.71      & 0.39     & 0.42           & 0.45        & -\\
			\hline
		\end{tabular}
	\end{table}
	
	\begin{figure}
		%\vspace{-1.5em}
		\begin{center}
			\scalebox{0.9}{
				\input{fig/auc_compare_star.pgf}
			}
		\end{center}
		\vspace{-3em}
		\caption{Comparison of our best model to other groups. We sort the pathologies with increasing average AUC over all groups. For our model, we report the min and max over all folds as error bar to illustrate the effect of splitting.}
		\label{fig:compare}
	\end{figure}
	\clearpage
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\section{Conclusion and Discussion}
	\label{sec:discuss}
	We present a systematic evaluation of different approaches for CNN-based X-ray classification on ChestX-ray14 and gain a better understanding of medical image processing with deep learning. While surprisingly satisfactory results are obtained with networks optimized on the ImageNet dataset, the best overall results can be reported for the model that is exclusively trained with CXRs and incorporates non-image data (i.e. view position, patient age, and gender).
	
	Our optimized ResNet-50 architecture achieves state-of-the art results in four out of fourteen classes compared to Rajpurkar et al. \cite{Rajpurkar2017} (who had state-of-the-art results in all fourteen classes). At the same time, a substantial variability in the results can be observed when different splits are considered. This becomes especially apparent for “Hernia”, the class with the fewest samples in the dataset (see also Figure~\ref{fig:compare}). For other classes even higher scores are reported in the literature (see e.g. Rajpurkar et al.\cite{Rajpurkar2017}). However, a comparison of the different CNN methods with respect to their performance is inherently difficult, as most evaluations have been performed on individual (random) partitions of the datasets.
	
	While the obtained results suggests that the training of deep neural networks in the medical domain is a viable option as more and more public datasets become available, the practical use of deep learning in clinical practice is still an open issue.
	In particular for the ChestX-ray14 datasets, the rather high label noise\cite{Wang2017} of 10\% makes an assessment of the true network performance difficult. Therefore, a clean testset without label noise is needed for clinical impact evaluation. As discussed by Oakden-Rayner \cite{Luke2017}, the quality of the (automatically generated) labels and their precise medical interpretation may be a limiting factor addition to the presence of treated findings. Our Grad-CAM results proves Oakden-Rayner concerns about the "pneumothorax" label. In a clinical setting, i.e. for the detection of critical findings, the focus would be on the reliably identification of acute cases of pneumothorax, while a network trained on ChestX-ray14 would also respond to cases with a chest drain. 
	
	Future work, will include investigation of other model architectures, new architectures for leveraging label dependencies and incorporating segmentation information.
	
	\bibliography{../references/references}
	
	
	%\section*{Acknowledgements (not compulsory)}
	%
	%Acknowledgements should be brief, and should not include thanks to anonymous referees and editors, or effusive comments. Grant or contribution numbers may be acknowledged.
	
	\section*{Author contributions statement}
	
	%Must include all authors, identified by initials, for example:
	I.M.B, A.S, and H.N. conceived the experiments, I.M.B conducted the experiments, I.M.B, A.S, and H.N. analyzed the results. All authors reviewed the manuscript. 
	
	\section*{Additional information}
	
	\subsection*{Competing financial interests} 
	M.G., H.N. and A.S are employees of Philips Research, Hamburg Germany.
	
	\subsection*{Data availability statement}
	The datasets analyzed during the current study are available in the ChestXray-NIHCC repository, https://nihcc.app.box.com/v/ChestXray-NIHCC.
	
\end{document}