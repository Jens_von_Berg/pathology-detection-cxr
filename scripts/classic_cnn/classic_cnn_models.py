import cntk

#from cntk.logging import *
#from cntk.train.training_session import *
#from cntk import *
#from cntk.train.distributed import *

from cntk.io import ImageDeserializer, MinibatchSource, StreamDef, StreamDefs, FULL_DATA_SWEEP
import cntk.io.transforms as xforms
from cntk.layers import Convolution2D, Activation, MaxPooling, Dense, Dropout, default_options, Sequential, For, Convolution, BatchNormalization, AveragePooling
from cntk.ops import relu, splice
from cntk.initializer import normal, he_normal


# Local Response Normalization layer. See Section 3.3 of the paper:
# https://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf
# The mathematical equation is:
#   b_{x,y}^i=a_{x,y}^i/(k+\alpha\sum_{j=max(0,i-n)}^{min(N-1, i+n)}(a_{x,y}^j)^2)^\beta
# where a_{x,y}^i is the activity of a neuron comoputed by applying kernel i at position (x,y)
# N is the total number of kernals, n is half normalization width.
def LocalResponseNormalization(k, n, alpha, beta, name=''):
    x = cntk.placeholder(name='lrn_arg')
    x2 = cntk.square(x)
    # reshape to insert a fake singleton reduction dimension after the 3th axis (channel axis). Note Python axis order and BrainScript are reversed.
    x2s = cntk.reshape(x2, (1, cntk.InferredDimension), 0, 1)
    W = cntk.constant(alpha / (2 * n + 1), (1, 2 * n + 1, 1, 1), name='W')
    # 3D convolution with a filter that has a non 1-size only in the 3rd axis, and does not reduce since the reduction dimension is fake and 1
    y = cntk.convolution(W, x2s)
    # reshape back to remove the fake singleton reduction dimension
    b = cntk.reshape(y, cntk.InferredDimension, 0, 2)
    den = cntk.exp(beta * cntk.log(k + b))
    apply_x = cntk.element_divide(x, den)
    return apply_x


# Create the network.
def create_alexnet(input, num_classes):
    with default_options(activation=None, pad=True, bias=True):
        z = Sequential([
            # we separate Convolution and ReLU to name the output for feature extraction (usually before ReLU)
            Convolution2D((11, 11), 96, init=normal(0.01), pad=False, strides=(4, 4), name='conv1'),
            Activation(activation=relu, name='relu1'),
            LocalResponseNormalization(1.0, 2, 0.0001, 0.75, name='norm1'),
            MaxPooling((3, 3), (2, 2), name='pool1'),

            Convolution2D((5, 5), 192, init=normal(0.01), init_bias=0.1, name='conv2'),
            Activation(activation=relu, name='relu2'),
            LocalResponseNormalization(1.0, 2, 0.0001, 0.75, name='norm2'),
            MaxPooling((3, 3), (2, 2), name='pool2'),

            Convolution2D((3, 3), 384, init=normal(0.01), name='conv3'),
            Activation(activation=relu, name='relu3'),
            Convolution2D((3, 3), 384, init=normal(0.01), init_bias=0.1, name='conv4'),
            Activation(activation=relu, name='relu4'),
            Convolution2D((3, 3), 256, init=normal(0.01), init_bias=0.1, name='conv5'),
            Activation(activation=relu, name='relu5'),
            MaxPooling((3, 3), (2, 2), name='pool5'),

            Dense(4096, init=normal(0.005), init_bias=0.1, name='fc6'),
            Activation(activation=relu, name='relu6'),
            Dropout(0.5, name='drop6'),
            Dense(4096, init=normal(0.005), init_bias=0.1, name='fc7'),
            Activation(activation=relu, name='relu7'),
            Dropout(0.5, name='drop7'),
            Dense(num_classes, init=normal(0.01), name='fc8')
        ])(input)
    return z


def create_vgg16(input, num_classes):
    with default_options(activation=None, pad=True, bias=True):
        z = Sequential([
            # we separate Convolution and ReLU to name the output for feature extraction (usually before ReLU)
            For(range(2), lambda i: [
                Convolution2D((3,3), 64, name='conv1_{}'.format(i)),
                Activation(activation=relu, name='relu1_{}'.format(i)),
            ]),
            MaxPooling((2,2), (2,2), name='pool1'),

            For(range(2), lambda i: [
                Convolution2D((3,3), 128, name='conv2_{}'.format(i)),
                Activation(activation=relu, name='relu2_{}'.format(i)),
            ]),
            MaxPooling((2,2), (2,2), name='pool2'),

            For(range(3), lambda i: [
                Convolution2D((3,3), 256, name='conv3_{}'.format(i)),
                Activation(activation=relu, name='relu3_{}'.format(i)),
            ]),
            MaxPooling((2,2), (2,2), name='pool3'),

            For(range(3), lambda i: [
                Convolution2D((3,3), 512, name='conv4_{}'.format(i)),
                Activation(activation=relu, name='relu4_{}'.format(i)),
            ]),
            MaxPooling((2,2), (2,2), name='pool4'),

            For(range(3), lambda i: [
                Convolution2D((3,3), 512, name='conv5_{}'.format(i)),
                Activation(activation=relu, name='relu5_{}'.format(i)),
            ]),
            MaxPooling((2,2), (2,2), name='pool5'),

            Dense(4096, name='fc6'),
            Activation(activation=relu, name='relu6'),
            Dropout(0.5, name='drop6'),
            Dense(4096, name='fc7'),
            Activation(activation=relu, name='relu7'),
            Dropout(0.5, name='drop7'),
            Dense(num_classes, name='fc8')
            ])(input)
    return z


def inception_block_with_maxpool(input, num1x1, num3x3r, num3x3, num3x3dblr, num3x3dbl, numPool, bnTimeConst):
    # 1x1 Convolution
    branch1x1 = conv_bn_relu_layer(input, num1x1, (1, 1), (1, 1), True, bnTimeConst)

    # 3x3 Convolution
    branch3x3_reduce = conv_bn_relu_layer(input, num3x3r, (1, 1), (1, 1), True, bnTimeConst)
    branch3x3 = conv_bn_relu_layer(branch3x3_reduce, num3x3, (3, 3), (1, 1), True, bnTimeConst)

    # Double 3x3 Convolution
    branch3x3dbl_reduce = conv_bn_relu_layer(input, num3x3dblr, (1, 1), (1, 1), True, bnTimeConst)
    branch3x3dbl_conv = conv_bn_relu_layer(branch3x3dbl_reduce, num3x3dbl, (3, 3), (1, 1), True, bnTimeConst)
    branch3x3dbl = conv_bn_relu_layer(branch3x3dbl_conv, num3x3dbl, (3, 3), (1, 1), True, bnTimeConst)

    # Max Pooling
    branchPool_maxpool = MaxPooling((3, 3), strides=(1, 1), pad=True)(input)
    branchPool = conv_bn_relu_layer(branchPool_maxpool, numPool, (1, 1), (1, 1), True, bnTimeConst)

    out = splice(branch1x1, branch3x3, branch3x3dbl, branchPool, axis=0)

    return out


def inception_block_with_avgpool(input, num1x1, num3x3r, num3x3, num3x3dblr, num3x3dbl, numPool, bnTimeConst):

    # 1x1 Convolution
    branch1x1 = conv_bn_relu_layer(input, num1x1, (1,1), (1,1), True, bnTimeConst)

    # 3x3 Convolution
    branch3x3_reduce = conv_bn_relu_layer(input, num3x3r, (1,1), (1,1), True, bnTimeConst)
    branch3x3 = conv_bn_relu_layer(branch3x3_reduce, num3x3, (3,3), (1,1), True, bnTimeConst)

    # Double 3x3 Convolution
    branch3x3dbl_reduce = conv_bn_relu_layer(input, num3x3dblr, (1,1), (1,1), True, bnTimeConst)
    branch3x3dbl_conv = conv_bn_relu_layer(branch3x3dbl_reduce, num3x3dbl, (3,3), (1,1), True, bnTimeConst)
    branch3x3dbl = conv_bn_relu_layer(branch3x3dbl_conv, num3x3dbl, (3,3), (1,1), True, bnTimeConst)

    # Average Pooling
    branchPool_avgpool = AveragePooling((3,3), strides=(1,1), pad=True)(input)
    branchPool = conv_bn_relu_layer(branchPool_avgpool, numPool, (1,1), (1,1), True, bnTimeConst)

    out = splice(branch1x1, branch3x3, branch3x3dbl, branchPool, axis=0)

    return out


def inception_block_pass_through(input, num1x1, num3x3r, num3x3, num3x3dblr, num3x3dbl, numPool, bnTimeConst):
    # 3x3 Convolution
    branch3x3_reduce = conv_bn_relu_layer(input, num3x3r, (1, 1), (1, 1), True, bnTimeConst)
    branch3x3 = conv_bn_relu_layer(branch3x3_reduce, num3x3, (3, 3), (2, 2), True, bnTimeConst)

    # Double 3x3 Convolution
    branch3x3dbl_reduce = conv_bn_relu_layer(input, num3x3dblr, (1, 1), (1, 1), True, bnTimeConst)
    branch3x3dbl_conv = conv_bn_relu_layer(branch3x3dbl_reduce, num3x3dbl, (3, 3), (1, 1), True, bnTimeConst)
    branch3x3dbl = conv_bn_relu_layer(branch3x3dbl_conv, num3x3dbl, (3, 3), (2, 2), True, bnTimeConst)

    # Max Pooling
    branchPool = MaxPooling((3, 3), strides=(2, 2), pad=True)(input)

    out = splice(branch3x3, branch3x3dbl, branchPool, axis=0)

    return out


def conv_bn(input, filter_size, num_filters, strides=(1,1), pad=True, bnTimeConst=4096, init=he_normal()):
    c = Convolution(filter_size, num_filters, activation=None, init=init, pad=pad, strides=strides, bias=False)(input)
    r = BatchNormalization(map_rank=1, normalization_time_constant=bnTimeConst, use_cntk_engine=False)(c)
    return r


def conv_bn_relu(input, filter_size, num_filters, strides=(1,1), pad=True, bnTimeConst=4096, init=he_normal()):
    r = conv_bn(input, filter_size, num_filters, strides, pad, bnTimeConst, init)
    return relu(r)


def create_bn_inception(input, num_classes):
    bn_time_const = 4096

    # 224 x 224 x 3
    conv1 = conv_bn_relu(input, 64, (7, 7), (2, 2), True, bn_time_const)
    # 112 x 112 x 64
    pool1 = MaxPooling(filter_shape=(3, 3), strides=(2, 2), pad=True)(conv1)
    # 56 x 56 x 64
    conv2a = conv_bn_relu(pool1, 64, (1, 1), (1, 1), True, bn_time_const)
    # 56 x 56 x 64
    conv2b = conv_bn_relu(conv2a, 192, (3, 3), (1, 1), True, bn_time_const)
    # 56 x 56 x 192
    pool2 = MaxPooling(filter_shape=(3, 3), strides=(2, 2), pad=True)(conv2b)

    # Inception Blocks
    # 28 x 28 x 192
    inception3a = inception_block_with_avgpool(pool2, 64, 64, 64, 64, 96, 32, bn_time_const)
    # 28 x 28 x 256
    inception3b = inception_block_with_avgpool(inception3a, 64, 64, 96, 64, 96, 64, bn_time_const)
    # 28 x 28 x 320
    inception3c = inception_block_pass_through(inception3b, 0, 128, 160, 64, 96, 0, bn_time_const)
    # 14 x 14 x 576
    inception4a = inception_block_with_avgpool(inception3c, 224, 64, 96, 96, 128, 128, bn_time_const)
    # 14 x 14 x 576
    inception4b = inception_block_with_avgpool(inception4a, 192, 96, 128, 96, 128, 128, bn_time_const)
    # 14 x 14 x 576
    inception4c = inception_block_with_avgpool(inception4b, 160, 128, 160, 128, 160, 128, bn_time_const)
    # 14 x 14 x 576
    inception4d = inception_block_with_avgpool(inception4c, 96, 128, 192, 160, 192, 128, bn_time_const)
    # 14 x 14 x 576
    inception4e = inception_block_pass_through(inception4d, 0, 128, 192, 192, 256, 0, bn_time_const)
    # 7 x 7 x 1024
    inception5a = inception_block_with_avgpool(inception4e, 352, 192, 320, 160, 224, 128, bn_time_const)
    # 7 x 7 x 1024
    inception5b = inception_block_with_maxpool(inception5a, 352, 192, 320, 192, 224, 128, bn_time_const)

    # Global Average
    # 7 x 7 x 1024
    pool3 = AveragePooling(filter_shape=(7, 7))(inception5b)
    # 1 x 1 x 1024
    z = Dense(num_classes, init=he_normal())(pool3)
    return z


# A bottleneck ResNet block is attempting to reduce the amount of computation by replacing
# the two 3x3 convolutions by a 1x1 convolution, bottlenecked to `interOutChannels` feature
# maps (usually interOutChannels < outChannels, thus the name bottleneck), followed by a
# 3x3 convolution, and then a 1x1 convolution again, with `outChannels` feature maps.
def resnet_bottleneck(input, outChannels, interOutChannels):
    c1 = conv_bn_relu(input, (1, 1), interOutChannels, (1, 1))
    c2 = conv_bn_relu(c1, (3, 3), interOutChannels, (1, 1))
    c3 = conv_bn(c2, (1, 1), outChannels)
    p = c3 + input
    return relu(p)


# a block to reduce the feature map resolution using bottleneck. One can reduce the size
# either at the first 1x1 convolution by specifying "stride1x1=(2:2)" (original paper),
# or at the 3x3 convolution by specifying "stride3x3=(2:2)" (Facebook re-implementation).
def resnet_bottleneck_inc(input, outChannels, interOutChannels, stride1x1, stride3x3):
    c1 = conv_bn_relu(input, (1, 1), interOutChannels, stride1x1)
    c2 = conv_bn_relu(c1, (3, 3), interOutChannels, stride3x3)
    c3 = conv_bn(c2, (1,1), outChannels)

    stride = [0, 0]
    for i in range(0, len(stride1x1)):
        stride[i] = stride1x1[i] * stride3x3[i]

    s = conv_bn(input, (1, 1), outChannels, tuple(stride))
    p = c3 + s
    return relu(p)


def resnet_bottleneck_stack(input, num_stack_layers, outChannels, interOutChannels):
    assert (num_stack_layers >= 0)
    l = input
    for _ in range(num_stack_layers):
        l = resnet_bottleneck(l, outChannels, interOutChannels)
    return l


# The basic ResNet block contains two 3x3 convolutions, which is added to the orignal input
# of the block.
def resnet_basic(input, outChannels):
    c1 = conv_bn_relu(input, (3,3), outChannels)
    c2 = conv_bn(c1, (3,3), outChannels)
    p  = c2 + input
    return relu(p)


# A block to reduce the feature map resolution. Two 3x3 convolutions with stride, which is
# added to the original input with 1x1 convolution and stride
def resnet_basic_inc(input, outChannels, stride=(2,2)):
    c1 = conv_bn_relu(input, (3,3), outChannels, stride)
    c2 = conv_bn(c1, (3,3), outChannels, (1,1))
    s  = conv_bn(input, (1,1), outChannels, stride)
    p  = c2 + s
    return relu(p)


def resnet_basic_stack(input, num_stack_layers, outChannels):
    assert (num_stack_layers >= 0)
    l = input
    for _ in range(num_stack_layers):
        l = resnet_basic(l, outChannels)
    return l


def create_resnet18_model(input, num_classes):
    c_map = [64, 128, 256, 512]
    # conv1 and max pooling
    conv = conv_bn_relu(input, (7, 7), num_filters=c_map[0], strides=(2, 2))
    convMax = MaxPooling((3, 3), strides=2, pad=True)(conv)
    r1_1 = resnet_basic_stack(convMax, 2, c_map[0])

    r2_1 = resnet_basic_inc(r1_1, c_map[1])
    r2_2 = resnet_basic(r2_1, c_map[1])

    r3_1 = resnet_basic_inc(r2_2, c_map[2])
    r3_2 = resnet_basic(r3_1, c_map[2])

    r4_1 = resnet_basic_inc(r3_2, c_map[3])
    r4_2 = resnet_basic(r4_1, c_map[3])

    # Global average pooling and output
    pool = AveragePooling(filter_shape=(7, 7))(r4_2)
    z = Dense(num_classes, name='prediction')(pool)
    return z


def create_resnet34_model(input, num_classes):
    c_map = [64, 128, 256, 512]
    numLayers = [3, 3, 5, 2]
    # conv1 and max pooling
    conv = conv_bn_relu(input, (7, 7), num_filters=c_map[0], strides=(2, 2))
    convMax = MaxPooling((3, 3), strides=2, pad=True)(conv)
    r1_1 = resnet_basic_stack(convMax, numLayers[0], c_map[0])

    r2_1 = resnet_basic_inc(r1_1, c_map[1])
    r2_2 = resnet_basic_stack(r2_1, numLayers[1], c_map[1])

    r3_1 = resnet_basic_inc(r2_2, c_map[2])
    r3_2 = resnet_basic_stack(r3_1, numLayers[2], c_map[2])

    r4_1 = resnet_basic_inc(r3_2, c_map[3])
    r4_2 = resnet_basic_stack(r4_1, numLayers[3], c_map[3])

    # Global average pooling and output
    pool = AveragePooling(filter_shape=(7, 7))(r4_2)
    z = Dense(num_classes, name='prediction')(pool)
    return z


def create_resnet50_model(input, num_classes):
    c_map = [64, 128, 256, 512, 1024, 2048]
    numLayers = [2, 3, 5, 2]
    # stride in BottleneckInc
    stride1x1 = (2, 2)
    stride3x3 = (1, 1)

    # conv1 and max pooling
    conv = conv_bn_relu(input, (7, 7), num_filters=c_map[0], strides=(2, 2))
    convMax = MaxPooling((3, 3), strides=2, pad=True)(conv)

    # conv2_x
    r1_1 = resnet_bottleneck_inc(convMax, c_map[2], c_map[0], (1,1), (1,1))
    r1_2 = resnet_bottleneck_stack(r1_1, numLayers[0], c_map[2], c_map[0])

    # conv3_x
    r2_1 = resnet_bottleneck_inc(r1_2, c_map[3], c_map[1], stride1x1, stride3x3)
    r2_2 = resnet_bottleneck_stack(r2_1, numLayers[1], c_map[3], c_map[1])

    # conv4_x
    r3_1 = resnet_bottleneck_inc(r2_2, c_map[4], c_map[2], stride1x1, stride3x3)
    r3_2 = resnet_bottleneck_stack(r3_1, numLayers[2], c_map[4], c_map[2])

    # conv5_x
    r4_1 = resnet_bottleneck_inc(r3_2, c_map[5], c_map[3], stride1x1, stride3x3)
    r4_2 = resnet_bottleneck_stack(r4_1, numLayers[3], c_map[5], c_map[3])

    # Global average pooling and output
    pool = AveragePooling(filter_shape=(7,7))(r4_2)
    z = Dense(num_classes, name='prediction')(pool)
    return z


def create_resnet101_model(input, num_classes):
    c_map = [64, 128, 256, 512, 1024, 2048]
    numLayers = [2, 3, 22, 2]
    # stride in BottleneckInc
    stride1x1 = (2, 2)
    stride3x3 = (1, 1)

    # conv1 and max pooling
    conv = conv_bn_relu(input, (7, 7), num_filters=c_map[0], strides=(2, 2))
    convMax = MaxPooling((3, 3), strides=2, pad=True)(conv)

    # conv2_x
    r1_1 = resnet_bottleneck_inc(convMax, c_map[2], c_map[0], (1,1), (1,1))
    r1_2 = resnet_bottleneck_stack(r1_1, numLayers[0], c_map[2], c_map[0])

    # conv3_x
    r2_1 = resnet_bottleneck_inc(r1_2, c_map[3], c_map[1], stride1x1, stride3x3)
    r2_2 = resnet_bottleneck_stack(r2_1, numLayers[1], c_map[3], c_map[1])

    # conv4_x
    r3_1 = resnet_bottleneck_inc(r2_2, c_map[4], c_map[2], stride1x1, stride3x3)
    r3_2 = resnet_bottleneck_stack(r3_1, numLayers[2], c_map[4], c_map[2])

    # conv5_x
    r4_1 = resnet_bottleneck_inc(r3_2, c_map[5], c_map[3], stride1x1, stride3x3)
    r4_2 = resnet_bottleneck_stack(r4_1, numLayers[3], c_map[5], c_map[3])

    # Global average pooling and output
    pool = AveragePooling(filter_shape=(7,7))(r4_2)
    z = Dense(num_classes)(pool)
    return z


def create_resnet152_model(input, num_classes):
    c_map = [64, 128, 256, 512, 1024, 2048]
    numLayers = [2, 7, 35, 2]
    # stride in BottleneckInc
    stride1x1 = (2, 2)
    stride3x3 = (1, 1)

    # conv1 and max pooling
    conv = conv_bn_relu(input, (7, 7), num_filters=c_map[0], strides=(2, 2))
    convMax = MaxPooling((3, 3), strides=2, pad=True)(conv)

    # conv2_x
    r1_1 = resnet_bottleneck_inc(convMax, c_map[2], c_map[0], (1,1), (1,1))
    r1_2 = resnet_bottleneck_stack(r1_1, numLayers[0], c_map[2], c_map[0])

    # conv3_x
    r2_1 = resnet_bottleneck_inc(r1_2, c_map[3], c_map[1], stride1x1, stride3x3)
    r2_2 = resnet_bottleneck_stack(r2_1, numLayers[1], c_map[3], c_map[1])

    # conv4_x
    r3_1 = resnet_bottleneck_inc(r2_2, c_map[4], c_map[2], stride1x1, stride3x3)
    r3_2 = resnet_bottleneck_stack(r3_1, numLayers[2], c_map[4], c_map[2])

    # conv5_x
    r4_1 = resnet_bottleneck_inc(r3_2, c_map[5], c_map[3], stride1x1, stride3x3)
    r4_2 = resnet_bottleneck_stack(r4_1, numLayers[3], c_map[5], c_map[3])

    # Global average pooling and output
    pool = AveragePooling(filter_shape=(7,7))(r4_2)
    z = Dense(num_classes)(pool)
    return z