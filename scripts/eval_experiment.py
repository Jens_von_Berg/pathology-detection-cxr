print(__doc__)

import numpy as np
import json
import matplotlib.pyplot as plt

from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from scipy import interp
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score
from sklearn.metrics import confusion_matrix
import itertools
import seaborn as sns
import os
import re
import pandas as pd
import matplotlib as mpl

def plot_roc_multiclass(y_test, y_score, n_classes):
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    threshold = dict()
    roc_auc = dict()
    for i in range(n_classes):
        # TODO: CHANGE y_score[:, 1] to do multi class
        fpr[i], tpr[i], threshold[i] = roc_curve(y_test[:, i], y_score[:, 1])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    #fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    #roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    plt.figure()
    lw = 2
    for i in range(n_classes):
        plt.plot(fpr[i], tpr[i], lw=lw, label='ROC curve of class {0} (area = {1:0.2f})'.format(i, roc_auc[i]))
    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate or (1- specificity)')
    plt.ylabel('True Positive Rate or Sensitivity')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.grid()
    plt.show()


def plot_roc_crossval(project_path, model, y_test, y_score):
    sns.set()
    sns.set_palette("colorblind")
    sns.set_context("notebook")
    sns.set_style("darkgrid")

    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 100)
    for i in range(len(y_test)):
        # Compute ROC curve and ROC area for each class
        fpr, tpr, thresholds = roc_curve(y_test[i][:, 0],  y_score[i][:, 1])
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, linestyle='--', label='ROC fold %d (area = %0.2f)' % (i, roc_auc), lw=1)
    plt.plot([0, 1], [0, 1], linestyle='--', label='Random', lw=1)

    mean_tpr /= len(y_test)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    plt.plot(mean_fpr, mean_tpr, linestyle='-', label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate or (1- specificity)')
    plt.ylabel('True Positive Rate or Sensitivity')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.tight_layout()
    plt.savefig(os.path.join(project_path, 'ROC_' + model + '.pdf'))
    plt.show()


def plot_precision_recall(project_path, model, y_test, y_score):
    sns.set()
    sns.set_palette("colorblind")
    sns.set_context("notebook")
    sns.set_style("darkgrid")

    mean_precision = 0.0
    mean_sensitivity = np.linspace(0, 1, 100)
    for i in range(len(y_test)):
        # TODO: CHANGE y_score[:, 1] to do multi class
        precision, sensitivity, thresholds = precision_recall_curve(y_test[i][:, 0],  y_score[i][:, 1])
        mean_precision += interp(mean_sensitivity, sensitivity, precision)
        mean_precision[0] = 0.0

        average_precision = average_precision_score(y_test[i][:, 0],  y_score[i][:, 1])
        plt.plot(sensitivity, precision, linestyle='--', lw=1,
                 label='Precision-sensitivity fold {0} (area = {1:0.2f})'.format(i, average_precision))
    #mean_precision /= len(y_test)
    #mean_precision[-1] = 1.0
    #average_precision = average_precision_score(mean_sensitivity, mean_precision)
    #plt.plot(mean_sensitivity, mean_precision, linestyle='-', lw=2,
    #         label='Mean Precision-recall curve (area = {1:0.2f})'.format(i, average_precision))
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('Sensitivity (Recall)')
    plt.ylabel('Precision')
    plt.title('Precision-Sensitivity curve')
    plt.legend(loc="lower right")
    plt.tight_layout()
    plt.savefig(os.path.join(project_path, 'Precision-Sensitivity_' + model + '.pdf'))
    plt.show()


def plot_confusion_matrix(project_path, model, y_test, y_predict, class_mapping):
    cnf_matrix = None
    for i in range(len(y_test)):
        """ Compute confusion matrix
        C_{0, 0} true negatives
        C_{1, 0} false negatives
        C_{1, 1} true positives
        C_{0, 1} false positives
        """
        cnf_matrix = confusion_matrix(y_test[i], y_predict[i]) if cnf_matrix is None else cnf_matrix + confusion_matrix(y_test[i], y_predict[i])
    cnf_matrix = cnf_matrix / len(y_test)
    np.set_printoptions(precision=2)

    # Plot non-normalized and normalized confusion matrix
    plt.figure()
    title = 'Confusion matrix'
    cmap = plt.cm.Blues

    plt.imshow(cnf_matrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(class_mapping))
    plt.xticks(tick_marks, class_mapping, rotation=45)
    plt.yticks(tick_marks, class_mapping)

    # Recall / Sensitivity measures the proportion of positives that are correctly identified as such
    print('Sensitivity: {0:.2f}'.format((cnf_matrix[1, 1] / (cnf_matrix[1, 1] + cnf_matrix[1, 0])) * 100))
    # Specificity measures the proportion of negatives that are correctly identified as such
    print('Specificity: {0:.2f}'.format((cnf_matrix[0, 0] / (cnf_matrix[0, 0] + cnf_matrix[0, 1]))* 100))
    print('Precision: {0:.2f}'.format((cnf_matrix[1, 1] / (cnf_matrix[1, 1] + cnf_matrix[0, 1]))* 100))
    print('Accuracy: {0:.2f}'.format(((cnf_matrix[0, 0] + cnf_matrix[1, 1]) / (cnf_matrix[0, 0] + cnf_matrix[1, 1] + cnf_matrix[0, 1] + cnf_matrix[1, 0])) * 100))

    thresh = cnf_matrix.max() / 2.
    sum_mat = cnf_matrix.sum(axis=1)[:, np.newaxis]

    for i, j in itertools.product(range(cnf_matrix.shape[0]), range(cnf_matrix.shape[1])):
        plt.text(j, i, '{0}\n{1}'.format(cnf_matrix[i, j], cnf_matrix[i, j].astype('float') / sum_mat[i]),
                 horizontalalignment="center", color="white" if cnf_matrix[i, j] > thresh else "black")
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.savefig(os.path.join(project_path, 'Conv-Mat_' + model + '.pdf'))
    plt.show()


def check_rel_vs_syn(data, class_mapping):
    import pandas
    import seaborn as sns
    y_test = []
    y_score = []
    y_image = []

    for data_point in data:
        y_test.append(data_point['class'])
        y_score.append([data_point['predictions'][k] for k in class_mapping])
        y_image.append(data_point['image'])

    y_test = label_binarize(y_test, classes=class_mapping)
    syn_counter = [0, 0]
    real_counter = [0, 0]
    plot_data = {'class': [], 'prediction': [], 'correct': []}
    for i in range(len(y_test)):
        predicted = np.argmax(y_score[i])
        if y_test[i] == 1:
            plot_data['class'].append('Cardiomegaly')
            plot_data['prediction'].append(float(np.max(y_score[i])))
            if y_test[i] == predicted:
                plot_data['correct'].append(1)
            else:
                plot_data['correct'].append(0)
            if len(y_image[i].split('_')) > 2:
                if y_test[i] == predicted:
                    syn_counter[0] += 1
                else:
                    syn_counter[1] += 1
            else:
                if y_test[i] == predicted:
                    real_counter[0] += 1
                else:
                    real_counter[1] += 1
        else:
            plot_data['class'].append('Other')
            plot_data['prediction'].append(float(np.max(y_score[i])))
            if y_test[i] == predicted:
                plot_data['correct'].append(1)
            else:
                plot_data['correct'].append(0)
    plot_data_df = pandas.DataFrame(plot_data)

    print('syn correct: {0}'.format(syn_counter[0]), 'syn false: {0}'.format(syn_counter[1]))
    print('Real correct: {0}'.format(real_counter[0]), 'Real false: {0}'.format(real_counter[1]))
    sns.boxplot(x="class", y="prediction", data=plot_data_df, hue='correct')
    plt.show()


def load_data(project_path, model, num_cross_val, class_mapping):
    y_test = {}
    y_score = {}
    y_predict = {}
    y_images = {}
    data_path = {}
    for cv in range(num_cross_val):
        y_test[cv] = []
        y_score[cv] = []
        y_predict[cv] = []
        y_images[cv] = []
        data_path[cv] = None
        # find eval file
        evl_file_name = ''
        cv_path = os.path.join(project_path, 'eval', model + "_CV-{0}".format(cv))
        for dirName, subdirList, fileList in os.walk(cv_path):
            for filename in fileList:
                if '.evl' in filename.lower():# or '.p' in filename.lower():
                    evl_file_name = filename

        path_to_file = os.path.join(cv_path, evl_file_name)
        # Reading data
        with open(path_to_file, 'r') as f:
            data = json.load(f)
            for data_point in data:
                if data_path[cv] is None:
                    try:
                        data_path[cv] = data_point['data']
                    except:
                        data_path[cv] = ''
                        if data_point['image'] not in y_images[cv]:
                            y_test[cv].append(data_point['class'])
                            y_score[cv].append([data_point['predictions'][k] for k in class_mapping])
                            y_images[cv].append(data_point['image'])
                else:
                    # TODO:: Change this to real list
                    if data_point['image'] not in y_images[cv]:
                        y_test[cv].append(data_point['class'])
                        y_score[cv].append([data_point['predictions'][k] for k in class_mapping])
                        y_images[cv].append(data_point['image'])
            y_score[cv] = np.array(y_score[cv])
            # Binarize the output
            y_test[cv] = label_binarize(y_test[cv], classes=class_mapping)
            n_classes = y_test[cv].shape[1]

            for score in y_score[cv]:
                y_predict[cv].append(np.argmax(score))

    return y_test, y_score, y_predict, y_images, data_path, n_classes


def plot_log_curves(project_path, model, cross_val, smoothing=2):
    mean_x = None
    data_frame = {'Epoch': [], 'metric': [], 'cv': [], 'Phase': []}
    cv_count = 0
    for cv in cross_val:
        log_file_name = None
        if os.path.isdir(os.path.join(project_path, 'models', model + "_CV-{0}".format(cv))):
            ## finde log file
            for dirName, subdirList, fileList in os.walk(os.path.join(project_path, 'models', model + "_CV-{0}".format(cv))):
                for filename in fileList:
                    if '.log' in filename.lower():
                        log_file_name = filename
            if log_file_name is None:
                continue
            cv_count += 1
            training_loss = []
            training_metric = []
            training_x = []
            test_metric = []
            test_x = []
            with open(os.path.join(project_path, 'models', model + "_CV-{0}".format(cv), log_file_name)) as log_file:
                for row in log_file:
                    if "Finished Epoch[" in row:
                        m = re.search('\d+ of \d+', row)
                        training_x.append(int(m.group(0).split(' of ')[0]))
                        if mean_x is None:
                            mean_x = np.linspace(0, int(m.group(0).split(' of ')[1]), int(int(m.group(0).split(' of ')[1]) / smoothing)).tolist()

                        m = re.search('metric = \d+\.\d+', row)
                        training_metric.append(float(m.group(0).split('metric = ')[1]))
                        m = re.search('loss = \d+\.\d+', row)
                        training_loss.append(float(m.group(0).split('loss = ')[1]))
                    if "Finished Evaluation" in row:
                        validation_test_step = len(training_metric)
                        m = re.search('\[\d+\]', row)
                        str = m.group(0)
                        m = re.search('\d+', str)
                        test_x.append(validation_test_step)
                        m = re.search('metric = \d+\.\d+', row)
                        test_metric.append(float(m.group(0).split('metric = ')[1]))
        else:
            print("No file: {0}".format(os.path.join(project_path, 'models', model + "_CV-{0}".format(cv))))
        data_frame['Epoch'] += mean_x
        tmp = (interp(mean_x, training_x, training_metric)).tolist()
        tmp[0] = training_metric[0]
        tmp[-1] = training_metric[-1]
        data_frame['metric'] += tmp
        data_frame['cv'] += [cv for i in range(len(mean_x))]
        data_frame['Phase'] += ['training' for i in range(len(mean_x))]

        data_frame['Epoch'] += mean_x
        tmp = (interp(mean_x, test_x, test_metric)).tolist()
        tmp[0] = test_metric[0]
        tmp[-1] = test_metric[-1]
        data_frame['metric'] += tmp
        data_frame['cv'] += [cv for i in range(len(mean_x))]
        data_frame['Phase'] += ['testing' for i in range(len(mean_x))]


    df = pd.DataFrame(data_frame)
    sns.set()
    sns.set_style("darkgrid")
    sns.set_palette("colorblind", 5)
    sns.set_context("notebook")
    fig, ax = plt.subplots(1, 1)
    sns.tsplot(data=df[df['Phase'] == "training"], time='Epoch', value='metric', unit="cv", condition='Phase', err_style=["ci_band"], ci="sd")
    sns.tsplot(data=df[df['Phase'] == "testing"], time='Epoch', value='metric', unit="cv", condition='Phase', err_style=["ci_band", "unit_traces"], ci="sd", color='g')

    ax.get_xaxis().set_minor_locator(mpl.ticker.AutoMinorLocator())
    ax.get_yaxis().set_minor_locator(mpl.ticker.AutoMinorLocator())
    ax.grid(b=True, which='major', color='w', linewidth=1.0)
    ax.grid(b=True, which='minor', color='w', linewidth=0.5)

    #plt.ylim(0, 20)
    #plt.xlim(0, 500)
    plt.ylabel('Error [%]')
    plt.title('Log-File plot')
    plt.tight_layout()
    plt.savefig(os.path.join(project_path, 'Log-plot_' + model + '.pdf'))
    plt.show()


def save_false_predictions(project_path, model, y_test, y_predict, y_score, y_images):
    import SimpleITK
    for cv in range(len(y_test)):
        data_path = 'E:\\datasets\\Indiana_University\\NLMCXR_dcm'
        #data_path = os.path.join(project_path, 'data', 'images')
        cv_path = os.path.join(project_path, 'eval', model + "_CV-{0}".format(cv))
        # Make directory
        score_file = {}
        score_file_true = {}
        true_count = {}
        for c in class_mapping:
            if not os.path.exists(os.path.join(cv_path, "{0}_false".format(c))):
                os.makedirs(os.path.join(cv_path, "{0}_false".format(c)))
            score_file[c] = open(os.path.join(cv_path, "{0}_false".format(c), 'scores.txt'), "w")
            if not os.path.exists(os.path.join(cv_path, "{0}_true".format(c))):
                os.makedirs(os.path.join(cv_path, "{0}_true".format(c)))
            score_file_true[c] = open(os.path.join(cv_path, "{0}_true".format(c), 'scores.txt'), "w")
            true_count[c] = 0
        # Save false predictions
        for test, predict, score, image in zip(y_test[cv], y_predict[cv], y_score[cv], y_images[cv]):
            if test != predict:
                img = SimpleITK.ReadImage(os.path.join(data_path, image.split('_')[0], image.split('.')[0] + '.dcm'))
                for l in range(len(class_mapping)):
                    if l == test:
                        if not os.path.isfile(os.path.join(cv_path, "{0}_false".format(class_mapping[l]), image.split('.')[0] + '.png')):
                            SimpleITK.WriteImage(img, os.path.join(cv_path, "{0}_false".format(class_mapping[l]), image.split('.')[0] + '.nii'))
                            score_file[class_mapping[l]].write(image.split('.')[0] + '.nii\t{0}\n'.format(score))
            else:
                img = SimpleITK.ReadImage(os.path.join(data_path, image.split('_')[0], image.split('.')[0] + '.dcm'))
                for l in range(len(class_mapping)):
                    if l == test and true_count[class_mapping[l]] < 10:
                        if not os.path.isfile(os.path.join(cv_path, "{0}_true".format(class_mapping[l]), image.split('.')[0] + '.png')):
                            SimpleITK.WriteImage(img, os.path.join(cv_path, "{0}_true".format(class_mapping[l]), image.split('.')[0] + '.nii'))
                            score_file_true[class_mapping[l]].write(image.split('.')[0] + '.nii\t{0}\n'.format(score))
                        true_count[class_mapping[l]] += 1



project_path = "E:\\experiment\\Indiana_dataset\\ots_models"
project_path = "E:\\experiment\\cardio-vs-normal"
model = "Indiana_two-stream_front-lat_dataAug_inverted_float32_cardio-vs-normal_ResNet50_ImageNet_CNTK_Shape-3x224x224_adam_lr-0.01_mb-128_True"
cross_val = [0, 1, 2, 3, 4]
class_mapping = ['normal', 'Cardiomegaly']

plot_log_curves(project_path, model, cross_val)

y_test, y_score, y_predict, y_images, data_path, n_classes = load_data(project_path, model, 5, class_mapping)
#save_false_predictions(project_path, model, y_test, y_predict, y_score, y_images)

plot_confusion_matrix(project_path, model, y_test, y_predict, class_mapping)
plot_roc_crossval(project_path, model, y_test, y_score)
#plot_precision_recall(project_path, model, y_test, y_score)