# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 10:35:25 2018

@author: Hannes Nickisch
"""

import os
import glob
import shutil
from subprocess import call

arxiv = 'arxiv'

def strip(b):
    i = b.find('%')
    b = b.replace('raphy{../references/references}','raphy{references}')
    if i==0 or (i>0 and b[i-1]!='\\'): return b[:i]+'\n'
    else: return b

def softdel(f):
    if os.path.isdir(f): shutil.rmtree(f)
    else:                os.mkdir(f)

f = open('nih-pathology-classification.tex','r'); buf = f.readlines(); f.close()
buf = [strip(b) for b in buf]                                  # strip comments
buf = [b for b in buf if len(b)>1]
f = open('paper.tex','w'); f.writelines(buf); f.close()
shutil.copy('../references/references.bib','.')

call(["pdflatex","paper.tex"])
call(["pdflatex","paper.tex"])
call(["bibtex","paper"])
call(["pdflatex","paper.tex"])
call(["pdflatex","paper.tex"])

softdel(arxiv)                                                    # tabula rasa
for f in glob.glob(r'llncs*'): shutil.copy(f,arxiv)
shutil.move('references.bib',arxiv)
shutil.move('paper.tex',arxiv)
shutil.move('paper.bbl',arxiv)

os.mkdir(arxiv+'/fig')
for f in glob.glob(r'fig/*'): shutil.copy(f,arxiv+'/fig')
for f in glob.glob('paper.*'): os.remove(f)

shutil.make_archive(arxiv,'zip',arxiv)
softdel(arxiv)                                                    # tabula rasa