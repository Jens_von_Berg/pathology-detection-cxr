# -*- coding: utf-8 -*-
""" Test a simple CNN for classification of abdominal AC planes

:Authors: Alexander Schmidt-Richberg <alexander.schmidt-richberg@philips.com>
:Address: Philips GmbH Innovative Technologies\n
          Research Laboratories\n
          Roentgenstrasse 24-26\n
          22335 Hamburg / Germany
:Date: 2016-11
:Copyright: This file contains proprietary information of Philips GmbH
            Innovative Technologies.  Copying or reproduction without prior
            written approval is prohibited.

            Philips internal use only - no distribution outside
            Philips allowed
:Platform: Unix, Windows
"""
from __future__ import absolute_import, division, print_function, unicode_literals
import os.path
#from filecmp import cmp
#import argparse
#import numpy as np

#from cntk.initializer import glorot_normal
#from cntk.learners import learning_rate_schedule, UnitType, momentum_schedule

#import pair.cntk_tools as ct
#from examples.architectures.fcnn.fcnn_segment import fcnn_segment
#from examples.architectures.fcnn.fcnn_eval import fcnn_eval

from minibatch_source_ITK import *
from classic_cnn.train_transfer_learning import *
from imgaug import augmenters as iaa

from sacred import Experiment
from sacred.observers import MongoObserver

ex = Experiment('ResNet-config')
# ex.observers.append(MongoObserver.create())

@ex.config
def configuration():
    cross_val = 4
    # choices=['train', 'create', 'segment', 'eval', 'plot'], help='phase of the experiment')
    phase = 'eval'
    # the image domain for the training'
    domain = 'Indiana_front_featurNorm'
    # 'the pathology to be classified'
    pathology = 'Cardiomegaly-vs-other'
    # 'restore existing training'
    restore = False
    # 'rerun existing training'
    rerun = True
    # 'overwrite automatic model_folder'
    model_folder = None

    # Ensure we always get the same amount of randomness
    #np.random.seed(0)

    # --------------------------
    # NETWORK PARAMS
    # --------------------------
    # Sometimes(0.5, ...) applies the given augmenter in 50% of all cases,
    # e.g. Sometimes(0.9, GaussianBlur(0.3)) would blur roughly every second image.
    sometimes = lambda aug: iaa.Sometimes(0.9, aug)
    params = {
        # Image reading and augmentation parameters
        # Define data augmentation sequence
        'data_augmentation': [
                iaa.Crop(px=(0, 16)), # crop images by 0-16px of their height/width
                sometimes(iaa.Affine(
                    scale={"x": (0.9, 1.1), "y": (0.9, 1.1)},  # scale images to 90-110% of their size, individually per axis
                    translate_percent={"x": (-0.0, 0.0), "y": (-0.0, 0.0)},  # translate by -0 to +0 percent (per axis)
                    rotate=(-10, 10),  # rotate by -45 to +45 degrees
                    shear=(-0, 0),  # shear by -0 to +0 degrees
                    order=[1],  # use nearest neighbour or bilinear interpolation (fast)
                    cval=(255)#,  # if mode is constant, use a cval between 0 and 255
                    #mode=ia.ALL  # use any of scikit-image's warping modes (see 2nd image from the top for examples)
                )),
                sometimes(iaa.PiecewiseAffine(scale=0.03)), # move pixels locally around (with random strengths)
                iaa.GaussianBlur(sigma=(0, 2.0))  # blur images with a sigma between 0 and 3.0
        ],
        'mean_file': './data/training_crossVal0_meanImage.nii',

        # Network architecture, 3D shapes are always given in ([c, ] z, y, x)
        # define base model location and characteristics
        'feature_node_name': "features",
        'last_hidden_node_name': "z.x",
        # define if all layers are frozen while training the last layer
        'freeze': True,
        # choices=[AlexNet_ImageNet_CNTK.model, ResNet18_ImageNet_CNTK.model, ResNet34_ImageNet_CNTK,
        # ResNet50_ImageNet_CNTK, VGG16_ImageNet_Caffe, VGG19_ImageNet_Caffe, InceptionV3_ImageNet_CNTK]
        'model_type': 'ResNet18_ImageNet_CNTK',
        # Class numbers
        'num_classes': 2,
        'class_mapping': ['Other', 'Cardiomegaly'],
        # Input params (computed during 'compute_input_parameters()')
        'input_sizes': [3, 224, 224],

        'init_method': cntk.glorot_normal(0.01),
        'norm_time_constant': 50,

        # Training parameters
        'minibatch_size': 128,
        'num_epochs': 700,
        'learning_rate': 0.001,
        'momentum': cntk.momentum_schedule([0.9] * 10 + [0.99])
        # 'momentum': momentum_as_time_constant_schedule(-args.minibatch_size/np.log(0.9))
    }
    params['base_model_file'] = os.path.join("E:/pretrained-model", "{0}.model".format(params['model_type']))
    params['data_augmentation'] = None

    # --------------------------
    # EXPERIMENT DATA + PATHS
    # --------------------------
    project_dir = 'E:/experiment/cardiomegaly_classification'
    tensorboard_dir = 'E:/experiment/Tensorboard'
    data = {'model_domain': domain,
            'model_filename': pathology,
            'model_folder': '{0}_{1}_{2}_Shape-{3}_LR-{4}_MB-{5}_{6}_CV-{7}'.format(
                domain, pathology, params['model_type'], "{0}x{1}x{2}".format(params['input_sizes'][0], params['input_sizes'][1], params['input_sizes'][2]),
                params['learning_rate'], params['minibatch_size'], params['freeze'], cross_val) if model_folder is None else model_folder,
            'data_dir': os.path.join(project_dir, 'data'),
            'datafile_path_train': os.path.join(project_dir, 'data', 'Indiana_front_Cardiomegaly_training_{0}_balanced.txt'.format(cross_val)),
            'datafile_path_test': os.path.join(project_dir, 'data', 'Indiana_front_Cardiomegaly_testing_{0}_balanced.txt'.format(cross_val))
            }

    data['model_dir'] = os.path.join(project_dir, 'models', data['model_folder'])
    data['tensorboard_dir'] = os.path.join(tensorboard_dir, data['model_folder'])
    data['eval_dir'] = os.path.join(project_dir, 'eval', data['model_folder'])
    data['model_path'] = os.path.join(data['model_dir'], data['model_filename'] + '.cmf')
    data['model_path_raw'] = os.path.join(data['model_dir'], data['model_filename'] + '.dnn')
    data['log_path'] = os.path.join(data['model_dir'], data['model_filename'] + '.log')
    data['xmnl_path'] = os.path.join(data['model_dir'], data['model_filename'] + '.xmnl')
    data['eval_file_path'] = os.path.join(data['eval_dir'], data['model_filename'] + '_eval.p')

    print('Starting script with model folder: {0}'.format(data['model_folder']))


# Evaluates a single image using the provided model
def eval_single_image(loaded_model, image_path, input_size, true_label):
    # Processing features - expecting one per line.
    # Read image and convert to numpy array: channel X hight X width format
    input_image = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(image_path))
    #SimpleITK.Show(SimpleITK.GetImageFromArray(input_image))
    # imageCHW = SimpleITK.GetArrayFromImage(self.transformation.trans(inputImage))
    input_resized = []
    if input_image.shape != tuple(input_size):
        if input_image.shape[1:3] != tuple(input_size)[1:3]:
            input_image = resize(input_image, tuple([input_image.shape[0]] + input_size[1:3]),
                                 preserve_range=True)
        if input_image.shape[0] != tuple(input_size)[0]:
            ret = np.empty(tuple(input_size), dtype=np.uint8)
            for i in range(input_size[0]):
                ret[i, :, :] = input_image
            input_resized = ret
        else:
            input_resized = input_image

    #SimpleITK.Show(SimpleITK.GetImageFromArray(input_resized))
    #SimpleITK.WriteImage(SimpleITK.GetImageFromArray(input_resized), 'E:\\normal_image.nii')
    #input('Wait')
    image_as_float = np.asarray(input_resized, dtype=np.float32)
    #SimpleITK.Show(SimpleITK.GetImageFromArray(input_resized))
    #SimpleITK.WriteImage(SimpleITK.GetImageFromArray(input_resized), 'E:\\float_image.nii')
    # converting class into the one-hot label
    labels = np.asarray(true_label, dtype=np.float32)[..., np.newaxis]
    class_ind = [labels == class_number for class_number in range(2)]
    labels = np.asarray(np.hstack(class_ind), dtype=np.float32)

    # compute model output
    arguments = {loaded_model.arguments[0]: [image_as_float], loaded_model.arguments[1]: [labels]}
    output = loaded_model.eval(arguments)

    # return softmax probabilities
    for key in output.keys():
        if 'prediction' in key.name:
            sm = cntk.softmax(output[key])
            return sm.eval()


def format_output_line(img_name, true_class, probs, class_mapping, top_n=2):
    class_probs = np.column_stack((probs, class_mapping)).tolist()
    class_probs.sort(key=lambda x: float(x[0]), reverse=True)
    top_n = min(top_n, len(class_mapping)) if top_n > 0 else len(class_mapping)
    true_class_name = class_mapping[true_class] if true_class >= 0 else 'unknown'
    line = {'class': true_class_name, 'predictions': {}, 'image': []}
    #line = '[{"class": "%s", "predictions": {' % true_class_name
    for i in range(0, top_n):
        line['predictions'][class_probs[i][1]] = float(class_probs[i][0])
        #line = '%s"%s":%.3f, ' % (line, class_probs[i][1], float(class_probs[i][0]))
    line['image'] = img_name
    # line = '%s}, "image": "%s"}]\n' % (line[:-2], img_name)
    return line


def eval_cnn(data, params):
    from cntk.ops.functions import load_model
    import numpy as np
    import json

    num_images = sum(1 for line in open(data['datafile_path_test']))
    print("Evaluating model output for {0} images.".format( num_images))
    print(data['model_path'])
    loaded_model = load_model(data['model_path'])

    # Make directory
    if not os.path.exists(data['eval_dir']):
        os.makedirs(data['eval_dir'])

    pred_count = 0
    correct_count = 0
    test_out = []
    with open(data['datafile_path_test'], "r") as input_file:
        for line in input_file:
            line = line.strip()
            seq_id, data_input = line.split('\t', 1)
            data_input = data_input.split("\t")
            true_label = int(data_input[1])
            probs = eval_single_image(loaded_model, data_input[0], params['input_sizes'], true_label)

            formatted_line = format_output_line(os.path.basename(data_input[0]), true_label, probs[0], params['class_mapping'])
            if true_label == 1:
                print(formatted_line)
                #input('wait')
            test_out.append(formatted_line)

            pred_count += 1

            predicted_label = np.argmax(probs)
            if predicted_label == true_label:
                correct_count += 1
    with open(data['eval_file_path'], 'w') as results_file:
        json.dump(test_out, results_file, indent=4)

    print("{0} out of {1} predictions were correct {2}.".format(correct_count, pred_count,
                                                                (float(correct_count) / pred_count)))
    print("Done. Wrote output to {0}".format(data['eval_file_path']))

@ex.automain
def main(data, params, phase, restore, rerun):
    # --------------------------
    #   SANITY CHECKS
    # --------------------------
    def check_data_found(datafile_path, required_rows):
        data_dir = os.path.dirname(datafile_path)
        with open(datafile_path, 'r') as datafile_path:
            lines = datafile_path.readlines()
            for line in lines:
                for path in [p.strip() for p in line.split('\t')][1:required_rows]:
                    assert os.path.isfile(path), 'File not found: {}'.format(path)
    check_data_found(data['datafile_path_train'], 1)
    check_data_found(data['datafile_path_test'], 1)
    # --------------------------
    #   ACTION
    # --------------------------
    if phase == 'train':

        # --------------------------
        #   DATA SOURCE
        # --------------------------
        # Initialise data source
        minibatch_source_train = MinibatchSourceITK(data['datafile_path_train'], params)
        minibatch_source_test = MinibatchSourceITK(data['datafile_path_test'], params, phase='test')

        if os.path.isfile(data['model_path']) and not rerun:
            print('Model already exists: {0}'.format(data['model_path']))
            pass
        else:
            train(data, params, minibatch_source_train, minibatch_source_test, restore=restore)

    elif phase == 'create':
        if os.path.isfile(data['xmnl_path']) and False:  # TODO REMOVE
            print('XMNL file already exists: {0}'.format(data['xmnl_path']))
        else:
            print('Creating model {0}...'.format(data['xmnl_path']))
            import shutil
            shutil.copy(data['model_path'], data['model_path'] + '.bak')

            # noinspection PyUnresolvedReferences
            padding = tuple(params['input_paddings'][l][-1] for l in range(len(params['levels'])))
            # noinspection PyTypeChecker
            call = ct.createfcn(model=data['model_path'], arch='FCNN', out=data['xmnl_path'],
                                padding=padding, size=params['patch_size'] + (1,))
            if call['State'] != 'finished':
                ct.print_results(call, True)

            # Convert
            with open(data['xmnl_path'], 'r') as xmnl_file:
                content = xmnl_file.read()
            content = content.replace('inputL1', 'input_l0')
            content = content.replace('inputL2', 'input_l1')
            content = content.replace('inputL3', 'input_l2')
            if params['image_normalization'] == 'intensitywindow':
                wl_block = '<pfh-IntensityWindowStep>\n' \
                           '                    <InputWindow value="{0}" />\n' \
                           '                    <InputLevel value="{1}" />\n' \
                           '                    <OutputWindow value="6" />\n' \
                           '                    <OutputLevel value="0" />\n' \
                           '                </pfh-IntensityWindowStep>'.format(
                               params['input_window'], params['input_level'])
                content = content.replace('<pfh-StandardizeStep />', wl_block)
                content = content.replace('<Background value="0" />', '<Background value="-3" />')
            with open(data['xmnl_path'], 'w') as xmnl_file:
                xmnl_file.write(content)

            shutil.copy(data['model_path'] + '.bak', data['model_path'])

    elif phase == 'eval':
        eval_cnn(data, params)


    elif phase == 'plot':
        #from examples.architectures.fcnn.fcnn_plot import fcnn_plot
        #fcnn_plot(data)
        pass
