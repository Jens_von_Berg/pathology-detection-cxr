# -*- coding: utf-8 -*-
""" The Philips Artificial Intelligence Research (PAIR) package. This package builds upon Microsoft's CNTK.

:Authors: Alexander Schmidt-Richberg <alexander.schmidt-richberg@philips.com>
          Tom Brosch <tom.brosch@philips.com>
:Address: Philips GmbH Innovative Technologies\n
          Research Laboratories\n
          Roentgenstrasse 24-26\n
          22335 Hamburg / Germany
:Date: 2017-03
:Copyright: This file contains proprietary information of Philips GmbH
            Innovative Technologies.  Copying or reproduction without prior
            written approval is prohibited.

            Philips internal use only - no distribution outside
            Philips allowed
:Platform: Unix, Windows
"""
# from __future__ import absolute_import, division, print_function, unicode_literals
# __author__ = 'Alexander Schmidt-Richberg <alexander.schmidt-richberg@philips.com>' \
#              'Tom Brosch <tom.brosch@philips.com>, '
# __status__ = 'production'
# __version__ = '0.1'
# __date__ = 'March 2017'
# __docformat__ = 'restructuredtext en'
