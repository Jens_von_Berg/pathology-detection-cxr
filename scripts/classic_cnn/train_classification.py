from __future__ import absolute_import, division, print_function, unicode_literals
import os.path

import cntk

from cntk.learners import adam
from cntk.train.trainer import Trainer
from cntk.logging import ProgressPrinter, log_number_of_parameters, TensorBoardProgressWriter
from cntk.train.training_session import training_session, minibatch_size_schedule, CheckpointConfig
from cntk.layers import  MaxPooling, Dense, Convolution, BatchNormalization, AveragePooling
from cntk import relu, he_normal

from pair.util import Timer

from classic_cnn.classic_cnn_models import *

def train(experiment, params, minibatch_source_training, minibatch_source_testing, restore=False, do_training_session=True):
    print('Starting training...')

    # Make directory
    if not os.path.exists(experiment['model_dir']):
        os.makedirs(experiment['model_dir'])

    # Input variables denoting the features and label data
    image_input = cntk.input_variable(shape=tuple(params['input_sizes']))
    label_input = cntk.input_variable(shape=(params['num_classes']),)

    #
    # Define network and metrics
    #

    # apply model to input
    # remove mean value and normalize the input
    #feature_scale = 1.0 / 255.0
    #input_var_norm = cntk.ops.element_times(feature_scale, image_input)
    input = cntk.ops.minus(image_input, cntk.constant(127))
   # input = image_input
    z = None
    if params['model_type'] == 'resNet16':
        z = create_resnet16_model(input, params['num_classes'])
    elif params['model_type'] == 'resNet18':
        z = create_resnet18_model(input, params['num_classes'])
    elif params['model_type'] == 'resNet34':
        z = create_resnet34_model(input, params['num_classes'])
    elif params['model_type'] == 'resNet50':
        z = create_resnet50_model(input, params['num_classes'])
    elif params['model_type'] == 'resNet101':
        z = create_resnet101_model(input, params['num_classes'])
    elif params['model_type'] == 'resNet152':
        z = create_resnet152_model(input, params['num_classes'])
    elif params['model_type'] == 'alexnet':
        z = create_alexnet(input, params['num_classes'])
    elif params['model_type'] == 'vgg16':
        z = create_vgg16(input, params['num_classes'])
    elif params['model_type'] == 'bn_inception':
        z = create_bn_inception(input, params['num_classes'])
    log_number_of_parameters(z)

    # loss and metric
    ce = cntk.losses.cross_entropy_with_softmax(z, label_input)
    pe = cntk.metrics.classification_error(z, label_input)
    pe5 = cntk.metrics.classification_error(z, label_input, topN=5)

    #
    # Instantiate the trainer object to drive the model training
    #
    learner = adam(parameters=z.parameters, lr=cntk.learners.learning_rate_schedule(params['learning_rate'], cntk.learners.UnitType.minibatch),
                   momentum=cntk.learners.momentum_schedule(params['momentum']))
    progress_printers = [ProgressPrinter(freq=minibatch_source_training.num_datasets, tag='Training', num_epochs=params['num_epochs'],
                                         metric_is_pct=True),
                         ProgressPrinter(freq=minibatch_source_training.num_datasets, tag='Training', num_epochs=params['num_epochs'],
                                         metric_is_pct=True, log_to_file=experiment['log_path']),
                         TensorBoardProgressWriter(model=z, freq=minibatch_source_training.num_datasets, log_dir=experiment['model_dir'])]

    trainer = Trainer(model=z,
                      criterion=(ce, pe),
                      parameter_learners=learner,
                      progress_writers=progress_printers)

    if restore and os.path.isfile(experiment['model_path']):
        trainer.restore_from_checkpoint(experiment['model_path'])

    # Perform model training

    #
    # Train CNTK MinibatchSource
    #
    if do_training_session:
        # Todo get epoch size from parameters
        # If minibatch_source inherits from CNTK class, the CNTK training session can be used for training
        session = training_session(
            trainer=trainer,
            mb_source=minibatch_source_training,
            mb_size=minibatch_size_schedule(params['minibatch_size']),
            model_inputs_to_streams={
                image_input: minibatch_source_training.fsi,
                label_input: minibatch_source_training.lsi
            },
            max_samples=params['num_epochs'] * minibatch_source_training.num_datasets,
            progress_frequency=minibatch_source_training.num_datasets,
            checkpoint_config=CheckpointConfig(filename=experiment['model_path'],
                                               restore=restore,
                                               frequency=minibatch_source_training.num_datasets * params[
                                                   'validation_test'],
                                               preserve_all=True),
            cv_config=cntk.CrossValidationConfig(source=minibatch_source_testing,
                                                 mb_size=params['minibatch_size'],
                                                 max_samples=minibatch_source_testing.num_datasets,
                                                 frequency=minibatch_source_training.num_datasets * params[
                                                     'validation_test']),
        )
        session.train()

    #
    # Train python-based MinibatchSource
    #
    else:
        # define mapping from reader streams to network inputs
        input_map = {
            image_input: minibatch_source.fsi,
            label_input: minibatch_source.lsi
        }
        # define mapping from intput streams to network inputs
        for epoch in range(params['num_epochs']):
            timer_next_mb = Timer('NextMB')
            timer_train = Timer('Train')

            sample_count = 0
            while sample_count < minibatch_source.num_datasets:
                # Get minibatch from minibatch source
                minibatch_size = min(params['minibatch_size'], minibatch_source.num_datasets - sample_count)

                with timer_next_mb:
                    mb = minibatch_source.next_minibatch(minibatch_size, input_map=None)

                # Execute training on minibatch
                with timer_train:
                    trainer.train_minibatch(input_map)
                sample_count += minibatch_size

            # Print training progress
            trainer.summarize_training_progress()

            # Print time measurements
            timer_read, timer_resample, timer_norm = minibatch_source.get_and_reset_timers()
            print('Timer: {0} ({1}, {2}, {3}), {4}, '.format(
                timer_next_mb, timer_read, timer_resample, timer_norm, timer_train))

            # Save checkpoint
            trainer.save_checkpoint(experiment['model_path'])