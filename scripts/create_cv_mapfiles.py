import csv
import numpy as np

# load file list
annotation_csv_path = 'E:/datasets/Indiana_University/NLMCXR_reports/csv_mesh/annotation_lat-front_1-depth_files.csv'
inputReader = csv.reader(open(annotation_csv_path))
data_list = list(inputReader)


list_wrong_images = ['227_IM-0859-4004.dcm', '800_IM-2334-4004-0001.dcm', '901_IM-2409-3001.dcm',
                     '1102_IM-0069-4004.dcm', '1230_IM-0154-1001.dcm', '1375_IM-0241-1001.dcm',
                     '1459_IM-0297-1001.dcm', '2218_IM-0823-2001.dcm', '2398_IM-0947-12012.dcm',
                     '3245_IM-1537-1001.dcm', '3245_IM-1537-4001.dcm', '3819_IM-1926-4004.dcm',
                     '3912_IM-1988-2001-0002.dcm']
front_cardio_list = []
front_other_list = []
for row in data_list:
    if 'Cardiomegaly' in row and 'front' in row:
        skip = False
        for wi in list_wrong_images:
            if wi in row[1]:
                skip = True
        if skip:
            continue
        if row[0] in front_cardio_list:
            print(row)
        front_cardio_list.append(row)
    if 'Cardiomegaly' not in row and 'front' in row:
        if row[0] in front_cardio_list:
            print(row)
        front_other_list.append(row)

print(len(front_cardio_list))
print(len(front_other_list))
seed = "0xCAFFEE"
np.random.seed(int(seed, 16))
np.random.shuffle(front_cardio_list)
np.random.shuffle(front_other_list)
num_cv = 5
cv_split_cardio = [front_cardio_list[i::num_cv] for i in range(num_cv)]
cv_split_other = [front_other_list[i::num_cv] for i in range(num_cv)]

for a in cv_split_cardio:
    print(len(a))
for a in cv_split_other:
    print(len(a))
input('wait')
for i in range(num_cv):
    train_list = sum([cv_split_cardio[ii - i] for ii in range(4)],[]) + sum([cv_split_other[ii - i] for ii in range(4)], [])
    np.random.shuffle(train_list)
    test_list = cv_split_cardio[num_cv - i - 1] + cv_split_other[num_cv - i - 1]
    np.random.shuffle(test_list)
    with open("./Indiana_front_Cardiomegaly_training_{0}.txt".format(i), 'w') as f:
        # configure writer to write standard csv file
        writer = csv.writer(f, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        for item in train_list:
            # Write item to f
            if 'Cardiomegaly' in item:
                writer.writerow([item[0], item[1], 1])
            else:
                writer.writerow([item[0], item[1], 0])
    with open("./Indiana_front_Cardiomegaly_testing_{0}.txt".format(i), 'w') as f:
        # configure writer to write standard csv file
        writer = csv.writer(f, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        for item in test_list:
            # Write item to f
            if 'Cardiomegaly' in item:
                writer.writerow([item[0], item[1], 1])
            else:
                writer.writerow([item[0], item[1], 0])


print(cv_split_cardio[0])