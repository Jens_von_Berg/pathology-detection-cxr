from minibatch_source_ITK import *
from tensorpack.dataflow import common
import os

from sacred import Experiment

ex = Experiment('Transfer-Learning')


@ex.config
def configuration():
    data = {'datafile_path_training': 'E:/experiment/Indiana_dataset/data/Indiana_front_raw_images_cardio-vs-normal_training_short.txt'}
    params = {'data_type': 'float32', 'input_sizes': [3, 224, 224], 'num_classes': 2, 'mean_file': None,
              'pre_fatch_data': True, 'pre_resize': False, 'data_augmentation': 't', 'minibatch_size': 1,
              'data_augmentation_save_batches': 0}


@ex.automain
def my_main(data, params):
    #data = {'datafile_path_training': 'E:/experiment/Indiana_dataset/data/Indiana_front_raw_images_cardio-vs-normal_training_short.txt'}
    #params = {'data_type': 'float32', 'input_sizes': [3, 224, 224], 'num_classes': 2, 'mean_file': None,
    #          'pre_fatch_data': True, 'pre_resize': False, 'data_augmentation': 'Test', 'minibatch_size': 128,
    #          'data_augmentation_save_batches': 0}

    os.chdir(os.path.dirname(data['datafile_path_training']))
    minibatch_source_train = MinibatchSourceITK(data['datafile_path_training'], num_classes=params['num_classes'], input_sizes=params['input_sizes'],
                minibatch_size=params['minibatch_size'], mean_file=params['mean_file'], phase='training',
                data_augmentation=params['data_augmentation'], data_augmentation_save_batches=params['data_augmentation_save_batches'],
                pre_resize=params['pre_resize'], pre_fatch_data=params['pre_fatch_data'])

    speed = common.TestDataSpeed(minibatch_source_train.batch_data, size=500000)
    speed.start()
