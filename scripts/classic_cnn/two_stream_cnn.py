from cntk.logging import *
from cntk.train.training_session import *
from cntk import *
from cntk.train.distributed import *
from cntk.io import ImageDeserializer, MinibatchSource, StreamDef, StreamDefs, FULL_DATA_SWEEP
import cntk.io.transforms as xforms
from cntk.layers import Convolution2D, Activation, MaxPooling, Dense, Dropout, default_options, Sequential, For, Convolution, BatchNormalization, AveragePooling
from cntk.initializer import normal

def conv_bn(input, filter_size, num_filters, name, strides=(1,1), pad=True, bnTimeConst=4096, init=he_normal()):
    c = Convolution(filter_size, num_filters, activation=None, init=init, pad=pad, strides=strides, bias=False, name=name)(input)
    r = BatchNormalization(map_rank=1, normalization_time_constant=bnTimeConst, use_cntk_engine=False, name=name)(c)
    return r

def conv_bn_relu(input, filter_size, num_filters, name, strides=(1,1), pad=True, bnTimeConst=4096, init=he_normal()):
    r = conv_bn(input, filter_size, num_filters, name, strides, pad, bnTimeConst, init)
    return relu(r)

# The basic ResNet block contains two 3x3 convolutions, which is added to the orignal input
# of the block.
def resnet_basic(input, outChannels):
    c1 = conv_bn_relu(input, (3,3), outChannels)
    c2 = conv_bn(c1, (3,3), outChannels)
    p  = c2 + input
    return relu(p)


# A block to reduce the feature map resolution. Two 3x3 convolutions with stride, which is
# added to the original input with 1x1 convolution and stride
def resnet_basic_inc(input, outChannels, stride=(2,2)):
    c1 = conv_bn_relu(input, (3,3), outChannels, stride)
    c2 = conv_bn(c1, (3,3), outChannels, (1,1))
    s  = conv_bn(input, (1,1), outChannels, stride)
    p  = c2 + s
    return relu(p)


def resnet_basic_stack(input, num_stack_layers, outChannels):
    assert (num_stack_layers >= 0)
    l = input
    for _ in range(num_stack_layers):
        l = resnet_basic(l, outChannels)
    return l

def create_ts_resnet18_model(input_front, input_lat, num_classes):
    c_map = [64, 128, 256, 512]
    # conv1 and max pooling
    conv = conv_bn_relu(input_front, (7, 7), num_filters=c_map[0], strides=(2, 2))
    convMax = MaxPooling((3, 3), strides=2, pad=True)(conv)
    r1_1 = resnet_basic_stack(convMax, 2, c_map[0])

    r2_1 = resnet_basic_inc(r1_1, c_map[1])
    r2_2 = resnet_basic(r2_1, c_map[1])

    r3_1 = resnet_basic_inc(r2_2, c_map[2])
    r3_2 = resnet_basic(r3_1, c_map[2])

    r4_1 = resnet_basic_inc(r3_2, c_map[3])
    r4_2 = resnet_basic(r4_1, c_map[3])

    # Global average pooling and output
    front_pool = AveragePooling(filter_shape=(7, 7))(r4_2)

    # conv1 and max pooling
    lat_conv = conv_bn_relu(input_lat, (7, 7), num_filters=c_map[0], strides=(2, 2))
    lat_convMax = MaxPooling((3, 3), strides=2, pad=True)(lat_conv)
    lat_r1_1 = resnet_basic_stack(lat_convMax, 2, c_map[0])

    lat_r2_1 = resnet_basic_inc(lat_r1_1, c_map[1])
    lat_r2_2 = resnet_basic(lat_r2_1, c_map[1])

    lat_r3_1 = resnet_basic_inc(lat_r2_2, c_map[2])
    lat_r3_2 = resnet_basic(lat_r3_1, c_map[2])

    lat_r4_1 = resnet_basic_inc(lat_r3_2, c_map[3])
    lat_r4_2 = resnet_basic(lat_r4_1, c_map[3])

    # Global average pooling and output
    lat_pool = AveragePooling(filter_shape=(7, 7))(lat_r4_2)

    front_lat_featurs = splice(front_pool, lat_pool, axis=1)
    z = Dense(num_classes, name='prediction')(front_lat_featurs)
    return z