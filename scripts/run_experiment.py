# -*- coding: utf-8 -*-
""" Test a simple CNN for classification of abdominal AC planes

:Authors: Alexander Schmidt-Richberg <alexander.schmidt-richberg@philips.com>
:Address: Philips GmbH Innovative Technologies\n
          Research Laboratories\n
          Roentgenstrasse 24-26\n
          22335 Hamburg / Germany
:Date: 2016-11
:Copyright: This file contains proprietary information of Philips GmbH
            Innovative Technologies.  Copying or reproduction without prior
            written approval is prohibited.

            Philips internal use only - no distribution outside
            Philips allowed
:Platform: Unix, Windows
"""
from __future__ import absolute_import, division, print_function, unicode_literals
import os.path
from filecmp import cmp
import argparse
import numpy as np

from cntk.initializer import glorot_normal
from cntk.learners import learning_rate_schedule, UnitType, momentum_schedule

#import pair.cntk_tools as ct
#from examples.architectures.fcnn.fcnn_segment import fcnn_segment
#from examples.architectures.fcnn.fcnn_eval import fcnn_eval

from minibatch_source_ITK import *
from classic_cnn.train_classification import *
from imgaug import augmenters as iaa

from sacred import Experiment
from sacred.observers import MongoObserver

CONFIGS = {
    # Patch-Size 64, Kernels 5, Maps 16-32-32
    0: {'patch_size': (64, 64, 64),
        'levels': [{'kernel': (5, 5, 5), 'maps': 16, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 32, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 32, 'strides': (2, 2, 2)}]},
    # Patch-Size 64, Kernels 5, Maps 16-32-48
    1: {'patch_size': (64, 64, 64),
        'levels': [{'kernel': (5, 5, 5), 'maps': 16, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 32, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 48, 'strides': (2, 2, 2)}]},
    # Patch-Size 64, Kernels 5, Maps 16-48-48
    2: {'patch_size': (64, 64, 64),
        'levels': [{'kernel': (5, 5, 5), 'maps': 16, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 48, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 48, 'strides': (2, 2, 2)}]},
    # Patch-Size 64, Kernels 5, Maps 32-48-48
    3: {'patch_size': (64, 64, 64),
        'levels': [{'kernel': (5, 5, 5), 'maps': 32, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 48, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 48, 'strides': (2, 2, 2)}]},
    # Patch-Size 72, Kernels 5, Maps 16-48-48
    4: {'patch_size': (72, 72, 72),
        'levels': [{'kernel': (5, 5, 5), 'maps': 16, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 48, 'strides': (2, 2, 2)},
                   {'kernel': (5, 5, 5), 'maps': 48, 'strides': (2, 2, 2)}]},
    # Patch-Size 66, Kernels 7, Maps 16-48-48
    5: {'patch_size': (66, 66, 66),
        'levels': [{'kernel': (7, 7, 7), 'maps': 16, 'strides': (2, 2, 2)},
                   {'kernel': (7, 7, 7), 'maps': 48, 'strides': (2, 2, 2)},
                   {'kernel': (7, 7, 7), 'maps': 48, 'strides': (2, 2, 2)}]}
}

ex = Experiment('ResNet-50')
ex.observers.append(MongoObserver.create())

@ex.config
def configuration():
    # choices=['train', 'create', 'segment', 'eval', 'plot'], help='phase of the experiment')
    phase = 'train'
    #p.add_argument('--domain', type=str, default='Indiana_front-lat', help='the image domain for the training')
    domain = 'Indiana_front-lat'
    #p.add_argument('--pathology', type=str,  default='Cardiomegaly', help='the pathology to be classified')
    pathology = 'Cardiomegaly'
    #p.add_argument('--learning_rate', type=float, default=0.001, help='the learning rate')
    learning_rate = 0.001
    #p.add_argument('--normalizer', type=str, default='intensitywindow', help='the grey value normalizer')
    normalizer = 'intensitywindow'
    #p.add_argument('--config', type=int, default=0, help='the shape (kernels, sizes etc.) of the network')
    config = 0
    #p.add_argument('--num_epochs', type=int, default=1000, help='the number of epochs')
    num_epochs = 1000
    #p.add_argument('--restore', action='store_true', help='restore existing training')
    restore = False
    #p.add_argument('--rerun', action='store_true', help='rerun existing training')
    rerun = False
    #p.add_argument('--cache_images', action='store_true', help='cache images in the minibatch source')
    cache_images = False
    #p.add_argument('--model_folder', type=str, default=None, help='overwrite automatic model_folder')
    model_folder = None
    # choices=[resNet50, resNet101, resNet152, alexnet, vgg16, bn_inception]
    model_type = 'resNet50'

    # Ensure we always get the same amount of randomness
    #np.random.seed(0)

    # --------------------------
    # NETWORK PARAMS
    # --------------------------

    # Sometimes(0.5, ...) applies the given augmenter in 50% of all cases,
    # e.g. Sometimes(0.9, GaussianBlur(0.3)) would blur roughly every second image.
    sometimes = lambda aug: iaa.Sometimes(0.9, aug)
    seq = [
                iaa.Crop(px=(0, 16)), # crop images by 0-16px of their height/width
                sometimes(iaa.Affine(
                    scale={"x": (0.9, 1.1), "y": (0.9, 1.1)},  # scale images to 90-110% of their size, individually per axis
                    translate_percent={"x": (-0.0, 0.0), "y": (-0.0, 0.0)},  # translate by -0 to +0 percent (per axis)
                    rotate=(-45, 45),  # rotate by -45 to +45 degrees
                    shear=(-0, 0),  # shear by -0 to +0 degrees
                    order=[1],  # use nearest neighbour or bilinear interpolation (fast)
                    cval=(255)#,  # if mode is constant, use a cval between 0 and 255
                    #mode=ia.ALL  # use any of scikit-image's warping modes (see 2nd image from the top for examples)
                )),
                sometimes(iaa.PiecewiseAffine(scale=0.03)), # move pixels locally around (with random strengths)
                iaa.GaussianBlur(sigma=(0, 2.0))  # blur images with a sigma between 0 and 3.0
        ]

    params = {
        # Image reading and augmentation parameters
        # Define data augmentation sequence
        'data_augmentation': seq,

        # Network architecture, 3D shapes are always given in ([c, ] z, y, x)
        'model_type': model_type,
        'num_classes': 2,
        'init_method': glorot_normal(0.01),
        'norm_time_constant': 4096,

        # Input params (computed during 'compute_input_parameters()')
        'input_sizes': [[1, 256, 256]],

        # Training parameters
        'cache_images': cache_images,
        'minibatch_size': 16,
        'num_epochs': num_epochs,
        'learning_rate': learning_rate_schedule(learning_rate, UnitType.minibatch),
        'momentum': momentum_schedule([0.5] + [0.9] * 10 + [0.99])

    }

    # --------------------------
    # EXPERIMENT DATA + PATHS
    # --------------------------
    cross_val = 1
    project_dir = 'E:/experiment/Classification_Indi'
    tensorboard_dir = 'E:/experiment/Tensorboard'
    data = {'model_domain': domain, 'model_structure': pathology, 'model_filename': pathology,
            'model_folder': '{0}_{1}_Shape-{2}_LR-{3}_{4}_CV{5}'.format(
                domain, pathology, params['input_sizes'][0],
                learning_rate, model_type, cross_val) if model_folder is None else model_folder,
            'data_dir': os.path.join(project_dir, 'data'),
            #'datafile_path_train': os.path.join(project_dir, '{0}_{1}_training.txt'.format(domain, pathology)),
            #'datafile_path_test': os.path.join(project_dir, '{0}_{1}_testing.txt'.format(domain, pathology))
            'datafile_path_train': os.path.join('E:\experiment\Classification_Indi\cntk-input', 'train_map_normal-vs-cardio_{}.txt'.format(cross_val)),
            'datafile_path_test': os.path.join('E:\experiment\Classification_Indi\cntk-input', 'test_map_normal-vs-cardio_{}.txt'.format(cross_val))
            }

    data['model_dir'] = os.path.join(project_dir, 'models', data['model_folder'])
    data['tensorboard_dir'] = os.path.join(tensorboard_dir, data['model_folder'])
    data['eval_dir'] = os.path.join(project_dir, 'eval', data['model_folder'])
    data['model_path'] = os.path.join(data['model_dir'], data['model_filename'] + '.cmf')
    data['log_path'] = os.path.join(data['model_dir'], data['model_filename'] + '.log')
    data['xmnl_path'] = os.path.join(data['model_dir'], data['model_filename'] + '.xmnl')
    data['seg_file_path'] = os.path.join(data['eval_dir'], data['model_filename'] + '_seg.p')
    data['eval_file_path'] = os.path.join(data['eval_dir'], data['model_filename'] + '_eval.p')

    print('Starting script with model folder: {0}'.format(data['model_folder']))

@ex.automain
def main(data, params, phase, restore, rerun):
    # --------------------------
    #   SANITY CHECKS
    # --------------------------
    def check_data_found(datafile_path, required_rows):
        data_dir = os.path.dirname(datafile_path)
        with open(datafile_path, 'r') as datafile_path:
            lines = datafile_path.readlines()
            for line in lines:
                for path in [p.strip() for p in line.split('\t')][1:required_rows]:
                    assert os.path.isfile(path), 'File not found: {}'.format(path)
    check_data_found(data['datafile_path_train'], 1)
    check_data_found(data['datafile_path_test'], 1)

    # --------------------------
    #   DATA SOURCE
    # --------------------------
    # Initialise data source
    # minibatch_source = FoveaMinibatchSource(data['datafile_path_train'], params)
    # minibatch_source.print_parameters()
    minibatch_source_train = MinibatchSourceITK(data['datafile_path_train'], params)
    minibatch_source_test = MinibatchSourceITK(data['datafile_path_test'], params, phase='test')

    # --------------------------
    #   ACTION
    # --------------------------
    if phase == 'train':
        if os.path.isfile(data['model_path']) and not rerun:
            #print(SKIP, 'Model already exists: {0}'.format(data['model_path']))
            pass
        else:
            train(data, params, minibatch_source_train, minibatch_source_test, restore=restore)

    elif phase == 'create':
        if os.path.isfile(data['xmnl_path']) and False:  # TODO REMOVE
            print('XMNL file already exists: {0}'.format(data['xmnl_path']))
        else:
            print('Creating model {0}...'.format(data['xmnl_path']))
            import shutil
            shutil.copy(data['model_path'], data['model_path'] + '.bak')

            # noinspection PyUnresolvedReferences
            padding = tuple(params['input_paddings'][l][-1] for l in range(len(params['levels'])))
            # noinspection PyTypeChecker
            call = ct.createfcn(model=data['model_path'], arch='FCNN', out=data['xmnl_path'],
                                padding=padding, size=params['patch_size'] + (1,))
            if call['State'] != 'finished':
                ct.print_results(call, True)

            # Convert
            with open(data['xmnl_path'], 'r') as xmnl_file:
                content = xmnl_file.read()
            content = content.replace('inputL1', 'input_l0')
            content = content.replace('inputL2', 'input_l1')
            content = content.replace('inputL3', 'input_l2')
            if params['image_normalization'] == 'intensitywindow':
                wl_block = '<pfh-IntensityWindowStep>\n' \
                           '                    <InputWindow value="{0}" />\n' \
                           '                    <InputLevel value="{1}" />\n' \
                           '                    <OutputWindow value="6" />\n' \
                           '                    <OutputLevel value="0" />\n' \
                           '                </pfh-IntensityWindowStep>'.format(
                               params['input_window'], params['input_level'])
                content = content.replace('<pfh-StandardizeStep />', wl_block)
                content = content.replace('<Background value="0" />', '<Background value="-3" />')
            with open(data['xmnl_path'], 'w') as xmnl_file:
                xmnl_file.write(content)

            shutil.copy(data['model_path'] + '.bak', data['model_path'])

    elif phase == 'segment':
        #fcnn_segment(data, params['num_classes'], rerun=rerun)
        pass

    elif phase == 'eval':
        #fcnn_eval(data, params['num_classes'], pathology=pathology, rerun=rerun)
        pass

    elif phase == 'plot':
        #from examples.architectures.fcnn.fcnn_plot import fcnn_plot
        #fcnn_plot(data)
        pass
