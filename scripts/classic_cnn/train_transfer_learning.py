from __future__ import absolute_import, division, print_function, unicode_literals
import os.path

import cntk

from cntk.learners import adam
from cntk.train.trainer import Trainer
from cntk.logging import ProgressPrinter, log_number_of_parameters, TensorBoardProgressWriter
from cntk.train.training_session import training_session, minibatch_size_schedule, CheckpointConfig
#from cntk.layers import  MaxPooling, Dense, Convolution, BatchNormalization, AveragePooling
#from cntk import relu, he_normal
from cntk.logging.graph import find_by_name, get_node_outputs
from cntk.ops.functions import CloneMethod

from pair.util import Timer

from classic_cnn.classic_cnn_models import *


# Creates the network model for transfer learning
def create_model(input_features, params):
    # Load the pretrained classification net and find nodes
    base_model   = cntk.load_model(params['base_model_file'])
    feature_node = find_by_name(base_model, params['feature_node_name'])
    last_node    = find_by_name(base_model, params['last_hidden_node_name'])

    # Clone the desired layers with fixed weights
    cloned_layers = cntk.combine([last_node.owner]).clone(
        CloneMethod.freeze if params['freeze'] else CloneMethod.clone,
        {feature_node: cntk.placeholder(name='features')})

    # Add new dense layer for class prediction
    feat_norm  = cntk.ops.minus(input_features, cntk.constant(127))
    cloned_out = cloned_layers(feat_norm)
    z          = Dense(params['num_classes'], activation=None, name='prediction')(cloned_out)

    return z


def train(experiment, params, minibatch_source_training, minibatch_source_testing, restore=False, do_training_session=True):
    print('Starting training...')

    # Make directory
    if not os.path.exists(experiment['model_dir']):
        os.makedirs(experiment['model_dir'])

    # Input variables denoting the features and label data
    image_input = cntk.input_variable(shape=tuple(params['input_sizes']))
    label_input = cntk.input_variable(shape=(params['num_classes']),)

    #
    # Define network and metrics
    #
    tl_model = create_model(image_input, params)
    log_number_of_parameters(tl_model)

    # loss and metric
    ce = cntk.losses.cross_entropy_with_softmax(tl_model, label_input)
    pe = cntk.metrics.classification_error(tl_model, label_input)
    pe5 = cntk.metrics.classification_error(tl_model, label_input, topN=5)

    #
    # Instantiate the trainer object to drive the model training
    #
    learner = adam(parameters=tl_model.parameters, lr=cntk.learners.learning_rate_schedule(params['learning_rate'], cntk.learners.UnitType.minibatch),
                   momentum=cntk.learners.momentum_schedule(params['momentum']))
    progress_printers = [ProgressPrinter(freq=minibatch_source_training.num_datasets, tag='Training',
                                         num_epochs=params['num_epochs'], metric_is_pct=True),
                         ProgressPrinter(freq=minibatch_source_training.num_datasets, tag='Training',
                                         num_epochs=params['num_epochs'], metric_is_pct=True, log_to_file=experiment['log_path']),
                         TensorBoardProgressWriter(model=tl_model, freq=minibatch_source_training.num_datasets, log_dir=experiment['tensorboard_dir'])]

    trainer = Trainer(model=tl_model,
                      criterion=(ce, pe),
                      parameter_learners=learner,
                      progress_writers=progress_printers)

    if restore and os.path.isfile(experiment['model_path']):
        trainer.restore_from_checkpoint(experiment['model_path'])

    # Perform model training

    #
    # Train CNTK MinibatchSource
    #
    if do_training_session:
        # Todo get epoch size from parameters
        # If minibatch_source inherits from CNTK class, the CNTK training session can be used for training
        session = training_session(
            trainer=trainer,
            mb_source=minibatch_source_training,
            mb_size=minibatch_size_schedule(params['minibatch_size']),
            model_inputs_to_streams={
                image_input: minibatch_source_training.fsi,
                label_input: minibatch_source_training.lsi
            },
            max_samples=params['num_epochs'] * minibatch_source_training.num_datasets,
            progress_frequency=minibatch_source_training.num_datasets,
            checkpoint_config=CheckpointConfig(filename=experiment['model_path'],
                                               restore=restore,
                                               frequency=minibatch_source_training.num_datasets*params['validation_test'],
                                               preserve_all=True),
            cv_config=cntk.CrossValidationConfig(source=minibatch_source_testing,
                                                 mb_size=params['minibatch_size'],
                                                 max_samples=minibatch_source_testing.num_datasets,
                                                 frequency=minibatch_source_training.num_datasets*params['validation_test']),
        )
        session.train()
    #
    # Train python-based MinibatchSource
    #
    else:
        # define mapping from reader streams to network inputs
        input_map = {
            image_input: minibatch_source.fsi,
            label_input: minibatch_source.lsi
        }
        # define mapping from intput streams to network inputs
        for epoch in range(params['num_epochs']):
            timer_next_mb = Timer('NextMB')
            timer_train = Timer('Train')

            sample_count = 0
            while sample_count < minibatch_source.num_datasets:
                # Get minibatch from minibatch source
                minibatch_size = min(params['minibatch_size'], minibatch_source.num_datasets - sample_count)

                with timer_next_mb:
                    mb = minibatch_source.next_minibatch(minibatch_size, input_map=None)

                # Execute training on minibatch
                with timer_train:
                    trainer.train_minibatch(input_map)
                sample_count += minibatch_size

            # Print training progress
            trainer.summarize_training_progress()

            # Print time measurements
            timer_read, timer_resample, timer_norm = minibatch_source.get_and_reset_timers()
            print('Timer: {0} ({1}, {2}, {3}), {4}, '.format(
                timer_next_mb, timer_read, timer_resample, timer_norm, timer_train))

            # Save checkpoint
            trainer.save_checkpoint(experiment['model_path'])