import dicom
import os
import csv
import numpy
import errno
import skimage
import numpy as np
from skimage.io import imread, imsave
from skimage import transform
from skimage import exposure
from skimage import util
from skimage import img_as_uint
import xml.etree.cElementTree as et
import xml.dom.minidom
import SimpleITK

from imgaug import augmenters as iaa

################################################
################################################

base_folder = os.path.dirname(os.path.realpath(__file__))
output_path = os.path.join("./data")
output_path_image = os.path.join("./data", "images")

input_training = os.path.join(output_path, "Indiana_front_Cardiomegaly_training_4.txt")
input_testing = os.path.join(output_path, 'Indiana_front_Cardiomegaly_testing_4.txt')

def argument_images(file_in):
    sometimes = lambda aug: iaa.Sometimes(0.9, aug)
    seq = iaa.Sequential([
        iaa.Crop(px=(0, 16)),  # crop images by 0-16px of their height/width
        iaa.Affine(
            scale={"x": (0.9, 1.1), "y": (0.9, 1.1)},  # scale images to 90-110% of their size, individually per axis
            translate_percent={"x": (-0.0, 0.0), "y": (-0.0, 0.0)},  # translate by -0 to +0 percent (per axis)
            rotate=(-3, 3),  # rotate by -45 to +45 degrees
            shear=(-1, 1),  # shear by -0 to +0 degrees
            order=[1],  # use nearest neighbour or bilinear interpolation (fast)
            cval=(255)  # ,  # if mode is constant, use a cval between 0 and 255
            # mode=ia.ALL  # use any of scikit-image's warping modes (see 2nd image from the top for examples)
        ),
        iaa.PiecewiseAffine(scale=(0.0, 0.01), cval=255),  # move pixels locally around (with random strengths)
        iaa.GaussianBlur(sigma=(0.0, 1.5))  # blur images with a sigma between 0 and 3.0
    ])

    images = []
    names = []
    with open(file_in) as list_data:
        for line in list_data:
            line = line.strip()
            data = line.split('\t')
            if data[-1] == '1':
                input_image = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(data[1]))
                images.append(np.transpose(input_image, [1, 2, 0]))
                names.append([data[1], data[2]])


    new_images = []
    print(len(images))
    counter = 1
    rerun = False
    for i in range(9):
        print(i)
        if rerun:
            seq_det = seq.to_deterministic()  # call this for each batch again, NOT only once at the start
            batch = seq_det.augment_images(images)
        for ii in range(len(images)):
            new_name = os.path.basename(names[ii][0]).split('.')[0] + '_{0}.nii'.format(i)
            if rerun:
                out_image = SimpleITK.GetImageFromArray(np.transpose(batch[ii], [2, 0, 1]))
                SimpleITK.WriteImage(out_image, os.path.join('./data/images/balanced', new_name))
            new_images.append([4000+counter, os.path.join('./data/images/balanced', new_name), names[ii][1]])
            counter += 1

    out_map = open(os.path.join(output_path, os.path.basename(file_in).split('.')[0] + '_balanced.txt'), 'w')
    writer = csv.writer(out_map, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
    with open(file_in) as list_data:
        for line in list_data:
            line = line.strip()
            data = line.split('\t')
            writer.writerow(data)
        for new in new_images:
            writer.writerow(new)


argument_images(input_testing)
argument_images(input_training)
