import SimpleITK
import random
import numpy as np
from skimage.transform import resize
import os

from cntk.io import UserMinibatchSource, StreamInformation, MinibatchData
from cntk import Value

from tensorpack.dataflow import prefetch, image, common, base, imgaug
import cv2


class MyDataFlow(base.RNGDataFlow):
    def __init__(self, data, img_size, classes, data_type, phase, pre_resize=False):
        self.data_type = data_type
        self.f_dim = img_size
        self.l_dim = classes
        self.pre_resize = pre_resize
        self.data = data
        assert phase in ['training', 'testing'], phase

        self.sweep_end = False

    def get_data(self):
        idxs = np.arange(len(self.data))
        # TODO: Add reading from disk

        for i in idxs:
            # Processing features - expecting one per line.
            # Read image and convert to numpy: channel X hight X width format
            input_image = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(self.data[i]['features']))
            if len(input_image.shape) == 2:
                input_image = input_image[np.newaxis, :, :]

            if self.pre_resize:
                if input_image.shape[1:3] != tuple(self.f_dim)[1:3]:
                    input_image = resize(input_image, tuple([input_image.shape[0]] + self.f_dim[1:3]),
                                         preserve_range=True)

            if input_image.shape[0] != self.f_dim[0]:
                ret = np.empty((self.f_dim[0], input_image.shape[1], input_image.shape[2]), dtype=self.data_type)
                ret[0, :, :] = ret[1, :, :] = ret[2, :, :] = input_image
                input_image = ret

            yield [np.transpose(input_image, [2, 1, 0]), self.data[i]['labels']]
            self.sweep_end = True

    def size(self):
        return len(self.data)


class RepeatedDataInfinite(base.ProxyDataFlow):
    """ Take data points from another DataFlow and produce them infinite times. i.e.:
        dp1, dp2, .... dpn, dp1, dp2, ....dpn
    """

    def __init__(self, ds):
        """
        Args:
            ds (DataFlow): input DataFlow
        """
        super(RepeatedDataInfinite, self).__init__(ds)

    def size(self):
        """
        Raises:
            :class:`ValueError` infinite dataflow.
        """
        return -1
        # raise ValueError("size() is unavailable for infinite dataflow")

    def get_data(self):
        while True:
            try:
                for dp in self.ds.get_data():
                    yield dp
            except StopIteration:
                pass


class TwoStreamDataFlow(base.RNGDataFlow):
    def __init__(self, map_file, map_file2, img_size, classes, data_type, phase, pre_fatch_data=False, pre_resize=False):
        self.map_file = map_file
        self.map_file2 = map_file2
        self.data_type = data_type
        self.f_dim = img_size
        self.l_dim = classes
        self.pre_resize = pre_resize

        self.data = {}
        self.pre_fatch_data = pre_fatch_data
        self.newID = 4000
        if self.pre_fatch_data:
            self._load_seqid()
        else:
            with open(self.map_file, "r") as input_file:
                with open(self.lat_map_file, "r") as input_file_lat:
                    for line in input_file:
                        line = line.strip()
                        if not line:
                            continue

                        line_lat = input_file_lat.readline()
                        line_lat = line_lat.strip()
                        if not line_lat:
                            continue
                        counter += 1

                        seq_id, data = line.split('\t', 1)
                        seq_id = int(seq_id)
                        data = data.split("\t")

                        seq_id_lat, data_lat = line_lat.split('\t', 1)
                        seq_id_lat = int(seq_id_lat)
                        data_lat = data_lat.split("\t")
                        if seq_id != seq_id_lat:
                            raise Exception('Front and Lat IDs dont match')

                        if seq_id not in self.data:
                            self.data[seq_id] = {'front': [], 'lat': [], 'labels': []}
                        # TODO:: Change to be generic
                        elif int(data[1]) == 1:
                            seq_id = seq_id_lat = self.newID
                            self.newID += 1
                            if seq_id not in self.data:
                                self.data[seq_id] = {'front': [], 'lat': [], 'labels': []}
                        else:
                            raise Exception('Not all ids are unique!!')

                        self.data[seq_id]['front'] = data[0]
                        self.data[seq_id]['lat'] = data_lat[0]
                        labels = np.asarray(data[1], dtype=np.float32)[..., np.newaxis]
                        class_ind = [labels == class_number for class_number in range(self.l_dim)]
                        labels = np.asarray(np.hstack(class_ind), dtype=np.float32)
                        self.data[seq_id]['labels'] = labels

        assert phase in ['training', 'testing'], phase
        if phase == 'training':
            self.shuffle = True
        else:
            self.shuffle = False

        self.sweep_end = False

    def get_data(self):
        self.sweep_end = False
        idxs = list(self.data.keys())
        if self.shuffle:
            self.rng.shuffle(idxs)
        # TODO: Add reading from disk
        if self.pre_fatch_data:
            for i in idxs:
                yield [self.data[i]['front'], self.data[i]['lat'], self.data[i]['labels']]
                self.sweep_end = True
        else:
            for i in idxs:
                # Processing features - expecting one per line.
                # Read image and convert to numpy: channel X hight X width format
                out = []
                for key in ['front', 'lat']:
                    input_image = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(self.data[i][key]))
                    if len(input_image.shape) == 2:
                        input_image = input_image[np.newaxis, :, :]

                    if self.pre_resize:
                        if input_image.shape[1:3] != tuple(self.f_dim)[1:3]:
                            input_image = resize(input_image, tuple([input_image.shape[0]] + self.f_dim[1:3]),
                                                 preserve_range=True)

                    if input_image.shape[0] != self.f_dim[0]:
                        ret = np.empty((self.f_dim[0], input_image.shape[1], input_image.shape[2]), dtype=self.data_type)
                        ret[0, :, :] = ret[1, :, :] = ret[2, :, :] = input_image
                        input_image = ret

                    out.append(input_image)
                out.append(self.data[i]['labels'])
                yield out
                self.sweep_end = True

    def size(self):
        return len(self.data)

    def _load_image(self, seq_id, data, projection):
        # Processing features - expecting one per line.
        # Read image and convert to numpy: channel X hight X width format
        input_image = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(data[0]))
        if len(input_image.shape) == 2:
            input_image = input_image[np.newaxis, :, :]

        if self.pre_resize:
            if input_image.shape[1:3] != tuple(self.f_dim)[1:3]:
                input_image = resize(input_image, tuple([input_image.shape[0]] + self.f_dim[1:3]),
                                     preserve_range=True)

        if input_image.shape[0] != self.f_dim[0]:
            ret = np.empty((self.f_dim[0], input_image.shape[1], input_image.shape[2]), dtype=self.data_type)
            ret[0, :, :] = ret[1, :, :] = ret[2, :, :] = input_image
            input_image = ret

        self.data[seq_id][projection] = np.transpose(input_image, [2, 1, 0])

        # Process label, if exists
        if len(data) == 2:
            # converting class into the one-hot label
            labels = np.asarray(data[1], dtype=np.float32)[..., np.newaxis]
            class_ind = [labels == class_number for class_number in range(self.l_dim)]
            labels = np.asarray(np.hstack(class_ind), dtype=np.float32)
            if len(self.data[seq_id]['labels']) == 0:
                self.data[seq_id]['labels'] = labels
            else:
                if not np.array_equal(self.data[seq_id]['labels'], labels):
                    print(self.data[seq_id]['labels'])
                    print(labels)
                    raise Exception('Labels dont match!!')

    def _load_seqid(self):
        """
        Load all images into memory for faster processing
        """
        print("Load all images into memory for faster processing")
        seq_id = 0
        with open(self.map_file, "r") as input_file:
            with open(self.map_file2, "r") as input_file_lat:
                counter = 0
                for line in input_file:
                    line = line.strip()
                    if not line:
                        continue

                    line_lat = input_file_lat.readline()
                    line_lat = line_lat.strip()
                    if not line_lat:
                        continue
                    counter += 1

                    seq_id, data = line.split('\t', 1)
                    seq_id = int(seq_id)
                    data = data.split("\t")

                    seq_id_lat, data_lat = line_lat.split('\t', 1)
                    seq_id_lat = int(seq_id_lat)
                    data_lat = data_lat.split("\t")
                    if seq_id != seq_id_lat:
                        raise Exception('Front and Lat IDs dont match')

                    if seq_id not in self.data:
                        self.data[seq_id] = {'front': [], 'lat': [], 'labels': []}
                    # TODO:: Change to be generic
                    elif int(data[1]) == 1:
                        seq_id = seq_id_lat = self.newID
                        self.newID += 1
                        if seq_id not in self.data:
                            self.data[seq_id] = {'front': [], 'lat': [], 'labels': []}
                    else:
                        print(line)
                        print(line_lat)
                        raise Exception('Not all ids are unique!!')

                    self._load_image(seq_id, data, 'front')
                    self._load_image(seq_id_lat, data_lat, 'lat')


class CropAndResize(imgaug.ImageAugmentor):
    """
    crop 8%~100% of the original image
    See `Going Deeper with Convolutions` by Google.
    """
    def __init__(self, crop_area_fraction=0.08,
                 aspect_ratio_low=0.75, aspect_ratio_high=1.333):
        self._init(locals())

    def _augment(self, img, _):
        h, w = img.shape[:2]
        area = h * w
        for _ in range(10):
            targetArea = self.rng.uniform(self.crop_area_fraction, 1.0) * area
            aspectR = self.rng.uniform(self.aspect_ratio_low, self.aspect_ratio_high)
            ww = int(np.sqrt(targetArea * aspectR) + 0.5)
            hh = int(np.sqrt(targetArea / aspectR) + 0.5)
            if self.rng.uniform() < 0.5:
                ww, hh = hh, ww
            if hh <= h and ww <= w:
                x1 = 0 if w == ww else self.rng.randint(0, w - ww)
                y1 = 0 if h == hh else self.rng.randint(0, h - hh)
                out = img[y1:y1 + hh, x1:x1 + ww]
                out = cv2.resize(out, (224, 224), interpolation=cv2.INTER_CUBIC)
                return out
        out = imgaug.ResizeShortestEdge(224, interp=cv2.INTER_CUBIC).augment(img)
        out = imgaug.CenterCrop(224).augment(out)
        return out


class MinibatchSourceITK(UserMinibatchSource):
    def __init__(self, map_file, minibatch_size, num_classes, input_sizes, mean_file, phase,
                 data_augmentation, data_augmentation_save_batches, pre_resize, pre_fatch_data, data_type='float32'):
        assert phase in ['training', 'testing', 'val'], phase
        self.phase = phase

        self.map_file = map_file
        os.chdir(os.path.join(os.path.dirname(map_file), '../'))
        if data_type == 'float32':
            self.data_type = np.float32
        elif data_type == 'uint8':
            self.data_type = np.uint8
        self.f_dim = input_sizes
        self.l_dim = num_classes

        self.fsi = StreamInformation("features", 0, 'dense', np.float32, tuple(self.f_dim,))
        self.lsi = StreamInformation("labels", 1, 'dense', np.float32, (self.l_dim,))

        # check data
        self.mean_file = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(mean_file)) if mean_file is not None else None

        # Load file list
        self.data = {}
        with open(self.map_file, "r") as input_file:
            seq_id = 0
            for line in input_file:
                line = line.strip()
                if not line:
                    continue
                seq_id_, data = line.split('\t', 1)
                data = data.split("\t")

                self.data[seq_id] = {'features': data[0], 'labels': []}

                labels = np.asarray(data[1], dtype=np.float32)[..., np.newaxis]
                class_ind = [labels == class_number for class_number in range(self.l_dim)]
                labels = np.asarray(np.hstack(class_ind), dtype=np.float32)
                self.data[seq_id]['labels'] = labels
                seq_id += 1

        # Data augmentations
        self.df = MyDataFlow(self.data, input_sizes, num_classes, self.data_type, phase, pre_resize=pre_resize)
        if pre_fatch_data:
            self.df = common.CacheData(self.df, True if phase == 'training' else False)
        self.df1 = RepeatedDataInfinite(self.df)

        self.num_datasets = self.df.size()
        print("Loaded {0} images into memory".format(self.df.size()))
        if data_augmentation:
            print("Using data augmentation")
        if phase == 'training':
            #cpu = min(15, multiprocessing.cpu_count())
            cpu = 2
            if data_augmentation:
                augmentors = [
                    #imgaug.Rotation(max_deg=3.0),
                    #imgaug.ResizeShortestEdge(256, cv2.INTER_CUBIC),
                    #imgaug.RandomCrop(tuple(self.f_dim[1:3])),
                    CropAndResize(crop_area_fraction=0.08),
                    #imgaug.RandomOrderAug(
                    #    [imgaug.BrightnessScale((0.6, 1.4), clip=False),
                    #     imgaug.Contrast((0.6, 1.4), clip=False),
                    #     imgaug.Saturation(0.4, rgb=False)]),
                    #imgaug.Flip(horiz=True)
                ]
                augmentor = imgaug.AugmentorList(augmentors)
                def mapf(dp):
                    img, cls = dp
                    #im = cv2.imread(fname, cv2.IMREAD_COLOR)
                    img = augmentor.augment(img)
                    return img, cls
                ds1 = prefetch.MultiThreadMapData(self.df1, nr_thread=cpu,
                    map_func=mapf, buffer_size=self.num_datasets, strict=True)
                #ds1 = image.AugmentImageComponent(self.df1, augmentors, index=0, copy=False)
            else:
                ds1 = self.df1
            ds1 = prefetch.PrefetchData(ds1, nr_prefetch=50, nr_proc=5)
            #ds1 = prefetch.PrefetchDataZMQ(ds1, nr_proc=1)
            self.batch_data = common.BatchData(ds1, minibatch_size, remainder=True, use_list=True)


        else:
            #cpu = min(10, multiprocessing.cpu_count())
            cpu = 10
            if data_augmentation :
                augmentors = [
                    imgaug.ResizeShortestEdge(256, cv2.INTER_CUBIC),
                    imgaug.CenterCrop(tuple(self.f_dim[1:3])),
                ]
                augmentor = imgaug.AugmentorList(augmentors)

                def mapf(dp):
                    img, cls = dp
                    #im = cv2.imread(fname, cv2.IMREAD_COLOR)
                    img = augmentor.augment(img)
                    return img, cls
                ds1 = prefetch.MultiThreadMapData(self.df1, nr_thread=cpu,
                    map_func=mapf, buffer_size=self.num_datasets, strict=True)
            else:
                ds1 = self.df1

            ds1 = common.BatchData(ds1, minibatch_size, remainder=True, use_list=True)
            self.batch_data = prefetch.PrefetchData(ds1, nr_prefetch=50, nr_proc=1)

        self.batch_data.reset_state()
        self.data_generator = self.batch_data.get_data()

        self.save_batch_to_disk = data_augmentation_save_batches
        super(MinibatchSourceITK, self).__init__()

    def stream_infos(self):
        return [self.fsi, self.lsi]

    def load_batch(self, num_samples):
        # Note that in this example we do not yet make use of number_of_workers or
        # worker_rank, which will limit the minibatch source to single GPU / single node
        # scenarios.
        features = []
        labels = []

        sweep_end = False

        f_sample_count = l_sample_count = 0

        while max(f_sample_count, l_sample_count) < num_samples:
            if self.next_seq_idx == len(self.sequences):
                if self.randomize:
                    random.shuffle(self.sequences)
                sweep_end = True
                self.next_seq_idx = 0

            seq_id = self.sequences[self.next_seq_idx]

            f_data = self.data[seq_id]['features']
            l_data = self.data[seq_id]['labels']
            if (features or labels) and max(f_sample_count + len(f_data),
                                            l_sample_count + len(l_data)) > num_samples:
                break
            f_sample_count += len(f_data)
            features += f_data

            l_sample_count += len(l_data)
            labels += l_data

            self.next_seq_idx += 1

        num_seq = len(features)
        return features, labels, num_seq, f_sample_count, l_sample_count, sweep_end

    def next_minibatch(self, num_samples, number_of_workers=1, worker_rank=0, device=None, input_map=None):
        try:
            batch = next(self.data_generator)
        except StopIteration:
            self.data_generator = self.batch_data.get_data()
            batch = next(self.data_generator)

        if self.save_batch_to_disk > 0:
            cntk_batch = np.asarray(np.transpose(batch[0], [0, 3, 2, 1]), dtype=np.float32)
            for i in range(len(cntk_batch)):
                SimpleITK.WriteImage(SimpleITK.GetImageFromArray(cntk_batch[i][0]), os.path.join(os.path.dirname(self.map_file), 'dataAug_inspection' ,'{0}_batch_{1}-{2}.nii'.format(self.phase, self.save_batch_to_disk, i)))
            self.save_batch_to_disk -= 1

        f_data = Value(batch=np.transpose(np.asarray(batch[0], dtype=np.float32), [0, 3, 2, 1]))
        l_data = Value(batch=np.asarray(batch[1], dtype=np.float32))
        num_seq = f_sample_count = l_sample_count = len(batch[0])
        sweep_end = False

        mb = {self.fsi: MinibatchData(f_data, num_seq, f_sample_count, sweep_end),
              self.lsi: MinibatchData(l_data, num_seq, l_sample_count, sweep_end)}
        if input_map is None:
            return mb
        else:
            return {key: mb[value] for (key, value) in input_map.items()}


class MinibatchSourceITKTwoStream(UserMinibatchSource):
    def __init__(self, front_map_file, lat_map_file, params, phase):
        assert phase in ['training', 'testing', 'val'], phase
        self.phase = phase
        self.front_map_file = front_map_file
        self.lat_map_file = lat_map_file
        os.chdir(os.path.join(os.path.dirname(front_map_file), '../'))
        params['data_type'] = 'float32'
        if params['data_type'] == 'float32':
            self.data_type = np.float32
        elif params['data_type'] == 'uint8':
            self.data_type = np.uint8

        self.f_dim = params['input_sizes']
        self.l_dim = params['num_classes']
        self.frontsi = StreamInformation("front", 0, 'dense', np.float32, tuple(self.f_dim, ))
        self.latsi = StreamInformation("lat", 1, 'dense', np.float32, tuple(self.f_dim, ))
        self.lsi = StreamInformation("labels", 2, 'dense', np.float32, (self.l_dim,))

        # check data
        self.mean_file = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(params['mean_file'])) if params['mean_file'] is not None else None
        # Data augmentations
        self.df = TwoStreamDataFlow( self.front_map_file, self.lat_map_file, params['input_sizes'], params['num_classes'], self.data_type, phase,
                             pre_fatch_data=params['pre_fatch_data'], pre_resize=params['pre_resize'])
        self.num_datasets = self.df.size()
        print("Loaded {0} images into memory".format(self.df.size()))
        if phase == 'training':
            cpu = min(20, multiprocessing.cpu_count())
            #cpu = 20
            if params['data_augmentation']:
                augmentors = [GoogleNetResize(),
                              # imgaug.RandomOrderAug(
                              #    [imgaug.BrightnessScale((0.6, 1.4), clip=False),
                              #     imgaug.Contrast((0.6, 1.4), clip=False),
                              #     imgaug.Saturation(0.4, rgb=False)])
                              ]

                #ds1 = AugmentImageComponent(self.df, augmentors, index=0, copy=False)
                augmentor = imgaug.AugmentorList(augmentors)
                def mapf(dp):
                    im, mask, cls = dp
                    # im = cv2.imread(fname, cv2.IMREAD_COLOR)
                    im = augmentor.augment(im)
                    mask = augmentor.augment(mask)
                    return im, mask, cls

                ds1 = ThreadedMapData(
                    self.df, nr_thread=cpu,
                    map_func=mapf, buffer_size=self.num_datasets, strict=True)
            else:
                ds1 = self.df
            ds1 = PrefetchData(ds1, nr_prefetch=50, nr_proc=cpu)
            #ds1 = PrefetchDataZMQ(ds1, nr_proc=cpu)
            self.batch_data = BatchData(ds1, params['minibatch_size'], remainder=True, use_list=True)
            #self.batch_data = PrefetchData(ds1, nr_prefetch=50, nr_proc=1)
        else:
            cpu = 10
            if params['data_augmentation'] :
                augmentors = [
                    imgaug.ResizeShortestEdge(256, cv2.INTER_CUBIC),
                    imgaug.CenterCrop((224, 224)),
                ]
                augmentor = imgaug.AugmentorList(augmentors)

                def mapf(dp):
                    im, mask, cls = dp
                    # im = cv2.imread(fname, cv2.IMREAD_COLOR)
                    im = augmentor.augment(im)
                    mask = augmentor.augment(mask)
                    return im, mask, cls

                ds1 = ThreadedMapData(
                    self.df, nr_thread=cpu,
                    map_func=mapf, buffer_size=self.num_datasets, strict=True)
            else:
                ds1 = self.df

            ds1 = BatchData(ds1, params['minibatch_size'], remainder=True, use_list=True)
            self.batch_data = PrefetchData(ds1, nr_prefetch=50, nr_proc=1)

        self.batch_data.reset_state()
        self.data_generator = self.batch_data.get_data()

        self.save_batch_to_disk = params['data_augmentation_save_batches']
        super(MinibatchSourceITKTwoStream, self).__init__()

    def _load_image(self, seq_id, data, projection):
        # Processing features - expecting one per line.
        # Read image and convert to numpy: channel X hight X width format
        input_image = SimpleITK.GetArrayFromImage(SimpleITK.ReadImage(data[0]))
        if input_image.shape != tuple(self.f_dim):
            if input_image.shape[1:3] != tuple(self.f_dim)[1:3]:
                input_image = resize(input_image, tuple([input_image.shape[0]] + self.f_dim[1:3]),
                                     preserve_range=True)
            if input_image.shape[0] != tuple(self.f_dim)[0]:
                ret = np.empty(tuple(self.f_dim), dtype=np.float32)
                for i in range(self.f_dim[0]):
                    ret[i, :, :] = input_image
                self.data[seq_id][projection].append(ret)
            else:
                self.data[seq_id][projection].append(input_image)

        # Process label, if exists
        if len(data) == 2:
            # converting class into the one-hot label
            labels = np.asarray(data[1], dtype=np.float32)[..., np.newaxis]
            class_ind = [labels == class_number for class_number in range(self.l_dim)]
            labels = np.asarray(np.hstack(class_ind), dtype=np.float32)
            if len(self.data[seq_id]['labels']) == 0:
                self.data[seq_id]['labels'].append(labels)
            else:
                if not np.array_equal(self.data[seq_id]['labels'][0], labels):
                    print(self.data[seq_id]['labels'][0])
                    print(labels)
                    raise Exception('Labels dont match!!')

    def _load(self):
        """
        Load all images into memory for faster processing
        """
        print("Load all front images into memory for faster processing")
        with open(self.front_map_file, "r") as input_file:
            with open(self.lat_map_file, "r") as input_file_lat:
                counter = 0
                for line in input_file:
                    line = line.strip()
                    if not line:
                        continue

                    line_lat = input_file_lat.readline()
                    line_lat = line_lat.strip()
                    if not line_lat:
                        continue
                    counter += 1

                    seq_id, data = line.split('\t', 1)
                    seq_id = int(seq_id)
                    data = data.split("\t")

                    seq_id_lat, data_lat = line_lat.split('\t', 1)
                    seq_id_lat = int(seq_id_lat)
                    data_lat = data_lat.split("\t")
                    if seq_id != seq_id_lat:
                        raise Exception('Front and Lat IDs dont match')

                    if seq_id not in self.data:
                        self.data[seq_id] = {'front': [], 'lat': [], 'labels': []}
                    elif int(data[1]) == 1:
                        seq_id = seq_id_lat = self.newID
                        self.newID += 1
                        if seq_id not in self.data:
                            self.data[seq_id] = {'front': [], 'lat': [], 'labels': []}
                    else:
                        raise Exception('Not all ids are unique!!')

                    self._load_image(seq_id, data, 'front')
                    self._load_image(seq_id_lat, data_lat, 'lat')
                print(counter)

    def stream_infos(self):
        return [self.frontsi, self.latsi, self.lsi]

    def load_batch(self, num_samples):
        # Note that in this example we do not yet make use of number_of_workers or
        # worker_rank, which will limit the minibatch source to single GPU / single node
        # scenarios.
        front = []
        lat = []
        labels = []

        sweep_end = False

        f_sample_count = l_sample_count = 0

        while max(f_sample_count, l_sample_count) < num_samples:
            if self.next_seq_idx == len(self.sequences):
                if self.randomize:
                    random.shuffle(self.sequences)
                sweep_end = True
                self.next_seq_idx = 0

            seq_id = self.sequences[self.next_seq_idx]

            front_data = self.data[seq_id]['front']
            lat_data = self.data[seq_id]['lat']
            l_data = self.data[seq_id]['labels']
            if (front or labels) and max(f_sample_count + len(front_data), l_sample_count + len(l_data)) > num_samples:
                break
            f_sample_count += len(front_data)
            front += front_data
            lat += lat_data

            l_sample_count += len(l_data)
            labels += l_data

            self.next_seq_idx += 1

        num_seq = len(front)
        return front, lat, labels, num_seq, f_sample_count, l_sample_count, sweep_end

    def next_minibatch(self, num_samples, number_of_workers=1, worker_rank=0, device=None, input_map=True):
        try:
            batch = next(self.data_generator)
        except StopIteration:
            self.data_generator = self.batch_data.get_data()
            batch = next(self.data_generator)
        if self.save_batch_to_disk > 0:
            cntk_batch = np.asarray(np.transpose(batch[0], [0, 3, 2, 1]), dtype=np.float32)
            for i in range(len(cntk_batch)):
                SimpleITK.WriteImage(SimpleITK.GetImageFromArray(cntk_batch[i][0]), os.path.join(os.path.dirname(self.map_file), 'dataAug_inspection' ,'{0}_batch_{1}-{2}.nii'.format(self.phase, self.save_batch_to_disk, i)))
            self.save_batch_to_disk -= 1

        num_seq = f_sample_count = l_sample_count = len(batch[0])
        #sweep_end = self.df.sweep_end
        sweep_end = False

        front_data = Value(batch=np.transpose(np.asarray(batch[0], dtype=np.float32), [0, 3, 2, 1]))
        lat_data = Value(batch=np.transpose(np.asarray(batch[1], dtype=np.float32), [0, 3, 2, 1]))
        l_data = Value(batch=np.asarray(batch[2], dtype=np.float32))
        if input_map:
            return {
                self.frontsi: MinibatchData(front_data, num_seq, f_sample_count, sweep_end),
                self.latsi: MinibatchData(lat_data, num_seq, f_sample_count, sweep_end),
                self.lsi: MinibatchData(l_data, num_seq, l_sample_count, sweep_end)
            }
        else:
            return MinibatchData(front_data, num_seq, f_sample_count, sweep_end), \
                   MinibatchData(lat_data, num_seq, f_sample_count, sweep_end),  \
                   MinibatchData(l_data, num_seq, l_sample_count, sweep_end)
    # def next_minibatch(self, num_samples, number_of_workers=1, worker_rank=0, device=None, input_map=None):
    #     # Note that in this example we do not yet make use of number_of_workers or
    #     # worker_rank, which will limit the minibatch source to single GPU / single node
    #     # scenarios.
    #
    #     features = []
    #     labels = []
    #
    #     sweep_end = False
    #
    #     f_sample_count = l_sample_count = 0
    #
    #     while max(f_sample_count, l_sample_count) < num_samples:
    #         if self.next_seq_idx == len(self.sequences):
    #             if self.randomize:
    #                 random.shuffle(self.sequences)
    #             sweep_end = True
    #             self.next_seq_idx = 0
    #
    #         seq_id = self.sequences[self.next_seq_idx]
    #
    #         f_data = self.data[seq_id]['features']
    #         l_data = self.data[seq_id]['labels']
    #         if (features or labels) and max(f_sample_count + len(f_data), l_sample_count + len(l_data)) > num_samples:
    #             break
    #         f_sample_count += len(f_data)
    #         features += f_data
    #
    #         #l_sample_count += len(l_data)
    #         l_sample_count  =  f_sample_count
    #         labels += [l_data, l_data]
    #
    #         self.next_seq_idx += 1
    #
    #     num_seq = len(features)
    #     #print(np.shape(features))
    #     #SimpleITK.WriteImage(SimpleITK.GetImageFromArray(features), 'E:/test.nii')
    #     #self.seq.show_grid(np.transpose(features, [0, 2, 3, 1]).astype(dtype=np.float32), cols=8, rows=8)
    #     #input('wait')
    #     if self.seq is not None:
    #         features = np.transpose(self.seq.augment_images(np.transpose(features, [0, 2, 3, 1]).astype(dtype=np.uint8)), [0, 3, 1, 2])
    #     #for f in features:
    #         #aug_features.append(np.transpose(self.seq.augment_images(np.transpose(f, [0, 2, 3, 1]).astype(dtype=np.uint8)), [0, 3, 1, 2]))
    #
    #     f_data = Value(batch=np.asarray(features, dtype=np.float32))
    #     l_data = Value(batch=np.asarray(labels, dtype=np.float32))
    #
    #     if input_map is None:
    #         return{
    #             self.fsi: MinibatchData(f_data, num_seq, f_sample_count, sweep_end),
    #             self.lsi: MinibatchData(l_data, num_seq, l_sample_count, sweep_end)
    #         }
