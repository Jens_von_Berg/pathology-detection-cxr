# -*- coding: utf-8 -*-
""" Evaluate cxr8 predictive accuracy.

Accuracy measures by Enlitic:
  https://arxiv.org/abs/1710.10501

Created: Tue Jan 16 17:09:27 2018 by Hannes Nickisch, Philips Research Hamburg.
"""

import os
import numpy as np
import json
import sklearn.metrics as slm
import matplotlib.pyplot as plt
from glob import glob as glob

# some different model output as nested JSON dict: p,y
# 15 independent predicted [0,1] and groundtruth labels {0,1} for 20% test set
fpath = 'P:/import/UKEDeepLearning/Experiments/NIHCXR/multilabel'
# fpath = 'C:/temp'

thr = 0.2                        # probability threshold when p is considered 1

def sensitivity(y,p):
    p = p>thr                                                        # binarize
    if np.sum(y)==0: return 1.0 # to be argued
    return np.sum(y*p)/np.sum(y)

def specificity(y,p):
    p = p>thr                                                        # binarize
    if np.sum(1-y)==0: return 1.0 # to be argued
    return np.sum(1-y*p)/np.sum(1-y)

def dice_score(y,p):
    p = p>thr                                                        # binarize
    return 2*np.sum(y*p)/(np.sum(y)+np.sum(p))

fnames = glob(fpath+'/eval/*/multi-label_eval.evl')
for fi,fnam in enumerate(fnames):
    print( "%d) %s"%(fi,os.path.basename(os.path.dirname(fnam))) )

    # read data
    fp = open(fnam,'r')
    data = json.load(fp)
    fp.close()

    # extract labels in matrix form
    y = [d['class'] for d in data[1:]]                            # groundtruth
    f = np.array(sorted(y[0].keys()))
    for i,yi in enumerate(y): y[i] = [yi[k] for k in sorted(yi)]
    y = np.array(y)
    p = [d['predictions'] for d in data[1:]]                       # prediction
    for i,pi in enumerate(p): p[i] = [pi[k] for k in sorted(pi)]
    p = np.array(p)
    idx = [i for i,fi in enumerate(f) if fi!='No_Findings']
    p,y,f = p[:,idx],y[:,idx],f[idx]                 # get rid of "No_findings"

    # compute measures
    nll = [slm.log_loss(y[:,i],p[:,i]) for i in range(y.shape[1])]
    print(" avg  NLL=%1.4f"%np.mean(nll))

    auc = [slm.roc_auc_score(y[:,i],p[:,i]) for i in range(y.shape[1])]
    print(" avg  AUC=%1.4f"%np.mean(auc))

    dice = [dice_score(y[:,i],p[:,i]) for i in range(y.shape[1])]
    print(" avg DICE=%1.4f"%np.mean(dice))

    pess = [0.5*(sensitivity(y[i],p[i])+
                 specificity(y[i],p[i])) for i in range(y.shape[0])]
    print(" avg PESS=%1.4f"%np.mean(pess))

    pcss = [0.5*(sensitivity(y[:,i],p[:,i])+
                 specificity(y[:,i],p[:,i])) for i in range(y.shape[1])]
    print(" avg PCSS=%1.4f"%np.mean(pcss))