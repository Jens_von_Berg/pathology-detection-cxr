
% Initial abstract: 250 words + 100 words + 2-4 pages "supplement"
% http://spie.org/conferences-and-exhibitions/medical-imaging/for-authors-and-presenters/abstract-submission-guidelines-program-placement-and-publication
%
% Final manuscript: 6-20 pages, see
% http://spie.org/conferences-and-exhibitions/authors-and-presenters/format-your-manuscript-multimedia-files-and-references

%  article.tex (Version 3.3, released 19 January 2008)
%  Article to demonstrate format for SPIE Proceedings
%  Special instructions are included in this file after the
%  symbol %>>>>
%  Numerous commands are commented out, but included to show how
%  to effect various options, e.g., to print page numbers, etc.
%  This LaTeX source file is composed for LaTeX2e.

%  The following commands have been added in the SPIE class 
%  file (spie.cls) and will not be understood in other classes:
%  \supit{}, \authorinfo{}, \skiplinehalf, \keywords{}
%  The bibliography style file is called spiebib.bst, 
%  which replaces the standard style unstr.bst.  

\documentclass[nocompress]{spie}  %>>> use for US letter paper
%%\documentclass[a4paper]{spie}  %>>> use this instead for A4 paper
%%\documentclass[nocompress]{spie}  %>>> to avoid compression of citations
%% \addtolength{\voffset}{9mm}   %>>> moves text field down
%% \renewcommand{\baselinestretch}{1.65}   %>>> 1.65 for double spacing, 1.25 for 1.5 spacing 
%  The following command loads a graphics package to include images 
%  in the document. It may be necessary to specify a DVI driver option,
%  e.g., [dvips], but that may be inappropriate for some LaTeX 
%  installations. 
%% MATHEMATISCHE NOTATION:
\usepackage{dsfont}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amstext}
\usepackage{subcaption}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{tikz}
\usepackage[]{graphicx}
\usetikzlibrary{plotmarks, arrows, calc}
\usepackage{tikzscale}
\usepackage{grffile}
\newlength\figureheight 
\newlength\figurewidth 
\setlength\figureheight{8cm} 
\setlength\figurewidth{0.99\textwidth}

%% 0, short version of the paper for review
%% 1, long version of the paper for Final manuscript submission
\def\versionLong{1}

% mess with the format in case of missing space
\if\versionLong0
\textheight 9.4in % 8.74in
\voffset -0.3in
%\textwidth 7.1in % 6.75in
%\hoffset -0.3in
\fi 

\title{Orientation Regression in Hand Radiographs:\\ A Transfer Learning Approach} 

%>>>> The author is responsible for formatting the 
%  author list and their institutions.  Use  \skiplinehalf 
%  to separate author list from addresses and between each address.
%  The correspondence between each author and his/her address
%  can be indicated with a superscript in italics, 
%  which is easily obtained with \supit{}.

\author{Ivo M. Baltruschat\supit{a},\supit{c}, Axel Saalbach\supit{b}, Mattias P. Heinrich\supit{a}, \\Hannes Nickisch\supit{b}, and Sascha Jockel\supit{c}
\skiplinehalf
\supit{a}Institute of Medical Informatics, Universit\"{a}t zu L\"{u}beck, L\"{u}beck, Germany; \\
\supit{b}Philips GmbH Innovative Technologies, Hamburg, Germany; \\
\supit{c}Philips Medical Systems DMC GmbH, Hamburg, Germany; \\
}

%>>>> Further information about the authors, other than their 
%  institution and addresses, should be included as a footnote, 
%  which is facilitated by the \authorinfo{} command.

%\authorinfo{Further author information: (Send correspondence to A.A.A.)\\A.A.A.: E-mail: aaa@tbk2.edu, Telephone: 1 505 123 1234\\  B.B.A.: E-mail: bba@cmp.com, Telephone: +33 (0)1 98 76 54 32}
%%>>>> when using amstex, you need to use @@ instead of @
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%>>>> uncomment following for page numbers
% \pagestyle{plain}    
%>>>> uncomment following to start page numbering at 301 
%\setcounter{page}{301} 
 
\begin{document} 
\maketitle 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{abstract}
Most radiologists prefer an upright orientation of the anatomy in a digital X-ray image for consistency and quality reasons. In almost half of the clinical cases, the anatomy is not upright orientated, which is why the images must be digitally rotated by radiographers. Earlier work has shown that automated orientation detection results in small error rates, but requires specially designed algorithms for individual anatomies. In this work, we propose a novel approach to overcome time-consuming feature engineering by means of Residual Neural Networks (ResNet), which extract generic low-level and high-level features, and provide promising solutions for medical imaging. Our method uses the learned representations to estimate the orientation via linear regression, and can be further improved by fine-tuning selected ResNet layers. The method was evaluated on 926 hand X-ray images and achieves a state-of-the-art mean absolute error of $2.79^{\circ}$.
\end{abstract}

%>>>> Include a list of keywords after the abstract
\keywords{deep learning, transfer learning, orientation regression, X-ray, residual neural network}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}
\label{sec:intro}  
A major challenge in medical imaging is workflow optimization as hospitals are keen to improve their patient throughput at a radiography system. In the radiography workflow of most examinations, a common task is to manually rotate digital X-ray images to a preferred orientation suitable for diagnostic reading. This reduces the number of patients that can be analyzed in a given time frame. An automatic alignment system for clinical X-ray images can help to ensure the correct orientation, but is still an open problem.
\if\versionLong1
We identify three major challenges: First, in clinical routine we observed up to 23 different examinations, consisting of 13 anatomies (e.g. hand, chest, ...) and 4 projection types (e.g. posterior-anterior (PA), anterior-posterior (AP), lateral, and oblique), makes this task very diverse. Second, computation time must be as small as possible and third, a high alignment accuracy is needed.
\fi

\begin{figure}[ht!]
	\begin{center}
		\begin{tabular}{c}
			\includegraphics[width=\textwidth]{fig/overview}
		\end{tabular}
	\end{center}
	\caption[Overview] 
	{ \label{fig:Overview} Top row: A neural network is trained on a large dataset of natural images (ImageNet). The model learns low- and high-level features. Bottom row: The model trained until convergence for natural image classification, is adapted for an orientation regression in medical image domain and a linear transformation is used to correct the image orientation in a final step. Early layers of the ImageNet model are directly transferred with same weights, while later layers are fine-tuned for our medical imaging application.}
\end{figure}

A lot of work has been done for orientation detection in X-ray imaging, in particular for chest examinations \cite{boone1992recognition}\cite{pietka1992orientation}\cite{luo2006automatic}\cite{nose2012simple} as they are most common examination. However, most of these methods allow only for the identification of a limited set of orientations. Furthermore, they are often specifically designed for specific examinations. Luo and Luo\cite{luo2006robust} presented a generic framework for orientation detection which supports multiple examination types. They tested the method on a large dataset of 12000 radiographies and reported a success rate of 96.1\%. In contrast to our approach, they only perform a classification in four regions (e.g. $0^{\circ}$, $90^{\circ}$, $180^{\circ}$, $270^{\circ}$) and not a precise orientation prediction.
An in-house study conducted at Philips Healthcare investigated specifically designed methods for orientation classification in chest and hand AP/PA images. The first method employed hand-crafted features, while the later used the Generalised Hough transform\cite{BALLARD1981111} in combination with a Canny edge detection. The methods achieved a good accuracy, but both approaches are not generic and thus cannot easily extended to other examination types. The development of such hand-crafted approaches has to be repeated for every examination type, which is a difficult and time intensive task. In addition, the method for hand AP/PA suffered from long computation time of 202ms. 
\if\versionLong1
In early work, we demonstrated that a simple geometrical method with hand-crafted features for hand AP/PA and oblique examinations can reduce the computation time down to 2ms but at the cost of alignment accuracy\cite{Baltruschat2016}. Anatomy orientations were classified in four categories, with a subsequent alignment in $90^{\circ}$ steps. We tested this feature-engineered method twice. First, only on AP/PA and oblique examination, which resulted in a mean accuracy of $96.73\%$. Secondly, we also added lateral examinations to the test set. Thus, we evaluated on the same data as in our later experiments. This reduced the mean accuracy to $92.14\%$.
\fi


Our goal is to present a generic method for orientation estimation in clinical X-ray images while retaining a high alignment accuracy and low computation time. To deal with these challenges we propose to employ deep Convolutional Neural Networks (CNNs) together with transfer learning\cite{Yosinski2014,Menkovski2015}. CNNs are a generic method for machine learning and usually need large amounts of annotated image data for training. Recent work showed that transfer learning can be used for medical applications\cite{Menkovski2015, Bar2015, Shin2016, Ginneken2015} without the need of a large training dataset. In this contribution, we present a novel approach for orientation regression and automatic alignment of X-ray images. We use a ResNet-50\cite{He2015,He2016} trained on more than 1 million images with 1000 class labels on the ImageNet dataset, popular in computer vision\cite{Russakovsky2015}, as generic high-level feature extractor for an orientation regression in the medical domain. When applied to unseen data such as X-ray images, our approach automatically estimates a real-valued orientation and can therefore align examinations into any preferred orientation. An overview of our method is given in Figure~\ref{fig:Overview}. Importantly, using this approach, our model can be trained even for examination types where only a small labeled dataset exists. 



Open-source datasets are rare in the medical domain compared to computer vision. As no dataset met our requirements nor even existed for training and testing deep neural networks regarding our objective, we created a dedicated dataset for orientation detection. We restricted the dataset to three examination types and labeled 424 hand AP/PA, 397 hand oblique, and 105 hand lateral images. Therefore, our orientation dataset contains 926 images in total. The distribution of the original image orientations in our dataset is given in Figure~\ref{fig:Orientation_dis}. 
In our dataset images are 16-bit gray scale and have a varying pixel size of $3001\times3001$, $3000\times2372$, or $2846\times2330$ depending on the size of the X-ray detector used during acquisition. We labeled each image with a precise orientation, which was measured between x-axis and main orientation line. Figure~\ref{fig:datasetExampels} illustrates our labelling strategy on some examples from our dataset.
All X-ray images in this work are presented and learned without any post-processing. Hence, the shutter area is not shown in black and the image contrast is not enhanced. We applied our automatic alignment method to the above data and show significantly improved accuracy compared to hand-crafted feature methods and off-the-shelf feature extractor in Section~\ref{EXPERIMENTS}.

\begin{figure}[h!]
	\centering
	\setlength\figureheight{6.0cm} 
	\input{fig/orientation_distribution_hand.tex}
	\caption[Overview]{ \label{fig:Orientation_dis} We labeled 926 hand X-ray images with a precise angle for anatomy orientation. The histogram is plotted over these labeled orientations. Each bin covers a width of five degree orientation angle. The values of the y-axis are normalized by the total number of samples and the x-axis is in degrees. In addition, above each bin its total count in our dataset is plotted.}
\end{figure}
\if\versionLong1
\begin{figure}[h!]
	\centering
	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_97_PA_170.tikz}
		\vspace{-1em}
		\caption{AP/PA,   $\theta = 170^{\circ} $}
	\end{subfigure}
	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_327_PA_288.tikz}
		\vspace{-1em}
		\caption{AP/PA,   $\theta = 288^{\circ}$}
	\end{subfigure}
	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_350_PA_42.tikz}
		\vspace{-1em}
		\caption{AP/PA,   $\theta = 42^{\circ}$}
	\end{subfigure}
	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_560_Lateral_183.tikz}
		\vspace{-1em}
		\caption{Lateral, $\theta = 183^{\circ}$}
	\end{subfigure}

	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_436_Oblique_256.tikz}
		\vspace{-1em}
		\caption{Oblique, $\theta = 256^{\circ}$}
	\end{subfigure}
	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_773_Oblique_85.tikz}
		\vspace{-1em}
		\caption{Oblique, $\theta = 85^{\circ}$}
	\end{subfigure}
	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_888_Oblique_131.tikz}
		\vspace{-1em}
		\caption{Oblique, $\theta = 131^{\circ}$}
	\end{subfigure}
	\begin{subfigure}[b]{0.245\textwidth}
		\input{fig/dataset_example/Hand_907_Lateral_240.tikz}
		\vspace{-1em}
		\caption{Lateral, $\theta = 240^{\circ}$}
	\end{subfigure}

	\caption[Examples of the orientation dataset]{Eight examples illustrating a large diversity of appearance in hand X-rays. Images are shown without any post-processing. The X-ray detector size my vary between examples and hence image size is also different (e.g. (a) has a smaller height than (b)). In addition, our labeling strategy is shown by a red arrow as main orientation line and a measured angle $\theta$ to x-axis. The coordinate system's origin is in the center of the X-ray detector for each example. Beneath each image, its projection type and annotated angle $\theta$ is plotted.}\label{fig:datasetExampels}
\end{figure}
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{METHOD}
Deep CNNs achieve state-of-the-art results in a variety of image processing tasks. These CNNs are trained for a task in a specific image domain. In a CNN, earlier layers extract generic low-level features and later layers extract domain and task specific features as illustrated in Figure~\ref{fig:Overview}. Transferring a pre-trained neural network to a new task in a different image domain is possible, if either the domains or tasks share similarities \cite{Menkovski2015, Bar2015}. In a transfer learning setup, parts of the network are replaced or retrained. If only the last layer is adapted, and the original network is used primarily as a feature extractor, this is often considered as an off-the-shelf approach. Using fine-tuning, additional (i.e. deep layer) of the network are retrained as well, based on the new data.

For the automatic alignment of the input images $F$ to a preferred orientation $\alpha \in [0^{\circ}, 360^{\circ})$, we use a two-step approach. First, we employ the adapted ResNet-50\cite{He2015,He2016} (see Table~\ref{tab:ResNetArchitecture}) for orientation regression, where an angle $\phi_{i} \in [0^{\circ}, 360^{\circ})$ is predicted for each test sample $F_{i}$. Second, we calculate the aligned image $F'_{i}$ by a linear transformation $T^{rot}_{\phi_i} : \mathbb{R}^2 \to \mathbb{R}^2$ with
\begin{equation}
\label{eq:rot}
T^{rot}_{\phi_i} = \begin{pmatrix}
\cos(\phi_i - \alpha) & -\sin(\phi_i - \alpha)  \\
\sin(\phi_i - \alpha) &  \cos(\phi_i - \alpha)
\end{pmatrix} 
\end{equation}
and $F'_{i} = T^{rot}_{\phi_i}(F_{i})$. Figure~\ref{fig:Overview} illustrates an overview of our automatic alignment method at the bottom row. 

\paragraph{Orientation Regression:} For learning purposes, ground truth information (i.e. the angle) was estimated manually for a representative set of images, resulting in $N$ training samples $\{F_{j}, \theta_{j}\}, j \in N$. During training, we use the mean squared error (MSE) as a loss function to compare the ground truth angle $\theta$ with the estimate $\phi$. The MSE puts more emphasis on large errors but cannot handle a circular angle representation as it assigns a large penalty to the pair $(2^{\circ},359^{\circ})$ even though they are very similar. We hence represent angles $\theta$ in Cartesian coordinates in the complex plane via the embedding $z(\theta)=(\cos(\theta),\sin(\theta))$ resulting in the empirical average:
\begin{equation}
  E = \dfrac{1}{N} \sum_{j=1}^{N} \norm{z(\theta_j)-z(\phi_j)}_{2}^{2} 
	  = \dfrac{1}{N} \sum_{j=1}^{N} \left[\cos(\theta_j)-\cos(\phi_j)\right]^2 + \left[\sin(\theta_j)-\sin(\phi_j)\right]^2
\end{equation}
which we use as a training criterion.
The classifier part is the last fully-connected layer in a ResNet. For the prediction of the orientation, we replaced the last layer by a new fully-connected layer with two outputs for a regression of the 2-D Cartesian coordinates $z(\theta)$.
\if\versionLong1 Table~\ref{tab:ResNetArchitecture} shows our new network architecture compared to the original network. \fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{EXPERIMENTS}
\label{EXPERIMENTS} 
We evaluated both transfer learning methods on our dataset and compared their results. Due to the limited size of our dataset, we employed a five times random sub-sampling and split the dataset into 70\% training and 30\% testing data in order to assess the performance of the method\cite{Molinaro2005}. 
\if\versionLong1
In our fine-tuning experiment, we retrained the conv5-layers as shown in Table~\ref{tab:ResNetArchitecture}.
\begin{table}[h!]
\caption[Architecture of the original, off-the-shelf, and fine-tuned ResNets.]{\label{tab:ResNetArchitecture} In our experiments, we used the ResNet-50. This table shows differences between the original architecture and ours (off-the-shelf and fine-tuned ResNet-50). If there is no difference to the original network, the word "same" is written in the table. The violet text emphasizes, which parts of the network are changed for our application. The conv3\_1, conv4\_1, and conv5\_1 layers perform a down-sampling of the spatial size with a stride of 2.}
\vspace{1ex}
\centering
\begin{footnotesize}
\begin{tabular}{c |c | c | c | c}
\textbf{Layer name}  & \textbf{Output size} & \textbf{Original 50-layer} & \textbf{Off-the-shelf 50-layer} & \textbf{Fine-tuned 50-layer} \\
\hline \hline
conv1 	& $112 \times 112$ & $7 \times 7$, 64-d, stride 2 & same & same\\
\hline
pooling1 	& $56 \times 56$ & $3 \times 3$, 64-d, max pool, stride 2  & same & same\\
\hline
conv2\_x & $56 \times 56$   & $\begin{bmatrix} 1 \times 1 , \text{ 64-d} \\ 3 \times 3 , \text{ 64-d} \\ 1 \times 1, \text{ 256-d}  \end{bmatrix} \times 3 $  & same & same\\
\hline
conv3\_x & $28 \times 28$   & $\begin{bmatrix} 1 \times 1, \text{ 128-d} \\ 3 \times 3, \text{ 128-d} \\ 1 \times 1, \text{ 512-d}  \end{bmatrix} \times 4 $  & same & same \\
\hline
conv4\_x & $14 \times 14$   & $\begin{bmatrix} 1 \times 1, \text{ 256-d} \\ 3 \times 3, \text{ 256-d} \\ 1 \times 1, \text{ 1024-d} \end{bmatrix} \times 6 $  & same & same\\
\hline
conv5\_x & $7 \times 7$     & $\begin{bmatrix} 1 \times 1, \text{ 512-d} \\ 3 \times 3, \text{ 512-d} \\ 1 \times 1, \text{ 2048-d} \end{bmatrix} \times 3 $  & same & \color{violet}\textbf{fine-tuned}\\
\hline
pooling2 & $1 \times 1$ & $7 \times 7$, 2048-d, average pool, stride 1  & same & same\\
\hline
	fc & $1 \times 1$  & 1000-d, fully-connected & \multicolumn{2}{c}{\color{violet}\textbf{2-d, fully-connected}} \\
		\hline
			loss & $1 \times 1$  & 1000-d, softmax & \multicolumn{2}{c}{\color{violet}\textbf{2-d, MSE}}\\
		\hline
	\end{tabular}
	\end{footnotesize}
	 \vspace{2ex}
\end{table}
\fi

\subsection{Preprocessing and data augmentation}
ResNet expects a $224\times224\times3$ dimensional input image of 8-bit color depth. We thus performed some pre-processing steps to meet the network's input requirement. First, a linear intensity transformation from 16-bit to 8-bit color depth is applied. Secondly, each image is resized to $256\times256$ pixels and converted into a 3-channel image by copying the same information in all channels. Hence, we did not preserve image aspect ratios. In this work, geometric transformations are applied as data augmentation methods. Each image is rotated by 90, 180, and 270 and then flipped horizontally. Furthermore, a random cropping from $256\times256\times3$ to $224\times224\times3$ pixels was employed. 

\paragraph{Training Parameters:}
We followed mostly the training of the original ResNet-50. In the training process, our data is zero-centered by subtracting the mean. We initialize the weights with the converged ResNet-50 of He et al.\cite{He2015} and trained the last fully-connected layer from scratch after random initialization. As optimization method, we employed Adaptive Moment Estimation (Adam) with a mini-batch size of 256. The parameters of Adam were set to $\beta_{1} = 0.9$, $\beta_{2} = 0.999$, and $\epsilon = 10^{-8}$. We reduced the learning rate $\eta$ by a factor of 10 compared to the original training and set it to $\eta = 0.01$. After each epoch, the training data is shuffled.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{RESULTS}

The runtime of our algorithm is a crucial aspect for the application and therefore we measured the runtime of 50 applications of the network to an image. The test is performed on the Intel Xeon CPU E3-1246 v3 with four 3.5Ghz cores, eight threads and a single image. Our tests resulted in an average runtime of 176.30ms.
\begin{figure}[ht!]
	\centering
	\setlength\figurewidth{0.45\textwidth}
	\setlength\figureheight{4.0cm}
	\begin{subfigure}[c]{0.495\textwidth}
		\input{fig/handRegressionOTSLogPlotMeanResample.tikz}
		\caption{ }
		\label{fig:handRegressionOTSLogPlotMeanResample}
	\end{subfigure}
	\begin{subfigure}[c]{0.495\textwidth}
		\input{fig/handRegressionFTLogPlotMeanResample.tikz}
		\caption{ }
		\label{fig:handRegressionFTLogPlotMeanResample}
	\end{subfigure}
\caption
%>>>> use \label inside caption to get Fig. number with \ref{}
{We trained five off-the-shelf and five fine-tuned ResNet-50. Each of the five with a different re-sample split, but same split between off-the-shelf and fine-tuned. In scatter plot (a) and (b), averaged training and testing curves with Euclidean loss (y-axis) against training epoch (x-axis) are shown. (a) shows training from the off-the-shelf experiment and achieved the best test loss at epoch 30. (b) shows training of our fine-tuning experiment and achieved the best test loss at epoch 21.}
\end{figure}

\paragraph*{Off-the-shelf:} Figure~\ref{fig:handRegressionOTSLogPlotMeanResample} illustrates the training process. It can be seen that the train and test loss decreased over time, while after epoch 18 only minor improvements could be observed. The train loss fluctuated around $0.51$ after epoch 26 and the test lost saturated at $1.17$ after epoch 30. Because of this, the training was stopped at epoch 32. Since the test loss saturated after epoch 30 on average, the corresponding networks were used for further investigation and they produced the following results.
The off-the-shelf ResNet-50 demonstrates impressive initial results for a precise angle prediction. Figure~\ref{fig:handRegressionOTSTestHistoResample} shows that the histogram of deviations is almost centered around zero, but the standard deviation of $9.47^{\circ}$ is high. This is caused by $0.66\%$ outliers in the range $[-180,-30)$ and $(30,180]$, which heavily influence the standard deviation. Nonetheless, $64.1\%$ of the samples are within $[-4^{\circ}, 4^{\circ}]$ and the mean absolute error (MAE) is only $4.25^{\circ}$.

\paragraph*{Fine-tuning:} Figure~\ref{fig:handRegressionFTLogPlotMeanResample} presents the results of the fine-tuning process. The test loss at epoch 0 is at $1.2$, as the final off-the-shelf ResNet-50 was used as initialization. After an initial increase of train and test loss, both losses decreased again. The test loss saturated around $0.56$ after epoch 21, whereas the train loss decreased continually until the training was stopped at epoch 32. It reached a minimum loss of $0.07$. As a result, we concluded that the trained network at epoch 21 merits further investigation. The results show that fine-tuning the regression ResNet-50 improved the outcome in terms of precision of angle prediction. In Figure~\ref{fig:handRegressionFTTestHistoResample}, the standard deviation of the distribution is $0.59^{\circ}$ smaller than that of the off-the-shelf ResNet-50. In addition, $84.4\%$ of the samples are in the interval $[-4^{\circ}, 4^{\circ}]$ and the MAE improved to $2.79^{\circ}$. Comparing this to the off-the-shelf version the MAE decreased by $1.46$ percentage points.

Previous methods only classify orientation in four classes. Hence, we binned our results to four classes and calculated a mean accuracy of $98.04\pm0.44\%$.

\begin{figure}[h!]
	\centering
	\setlength\figurewidth{0.40\textwidth}
	\setlength\figureheight{4.0cm}
	\begin{subfigure}[c]{0.49\textwidth}
		\input{fig/handRegressionOTSTestHistoResample.tikz}
		\caption{ }
		\label{fig:handRegressionOTSTestHistoResample}
	\end{subfigure}
	\begin{subfigure}[c]{0.49\textwidth}
		\input{fig/handRegressionFTTestHistoResample.tikz}
		\caption{ }
		\label{fig:handRegressionFTTestHistoResample}
	\end{subfigure}

	\caption
	{As evaluation metric, we calculated the difference $e$ between predicted angle and our labeled angle for each test sample. Panel (a) shows the resulting histogram from the off-the-shelf experiment and panel (b) displays the resulting histogram of our fine-tuning experiment. $e$ is on the x-axis in degrees and the frequency of each bin is on the y-axis. In both histograms, $\sigma$ is heavily influenced by outliers in the range $[-180^{\circ},-30^{\circ})$ and $(30^{\circ},180^{\circ})$. The bin size for the interval $[-20^{\circ},20^{\circ}]$ is one degree, whereas the intervals $[-180^{\circ},-20^{\circ})$ and $(20^{\circ},180^{\circ}]$ are reduced to a single bin for visualization purposes.}
\end{figure}  

\begin{figure}[ht!]
\centering
\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_31_PA_291.tikz}
	\vspace{-1em}
	\caption{$\|e\| = 3^{\circ}$}
\end{subfigure}
\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_289_PA_215.tikz}
	\vspace{-1em}
	\caption{$\|e\| = 1^{\circ}$}
\end{subfigure}
\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_750_Oblique_195.tikz}
	\vspace{-1em}
	\caption{$\|e\| = 14^{\circ}$}
\end{subfigure}
\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_488_Oblique_212.tikz}
	\vspace{-1em}
	\caption{$ \|e\| = 186^{\circ}$}
\end{subfigure}

\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_600_Lateral_56.tikz}
	\vspace{-1em}
	\caption{$\|e\| = 2^{\circ}$}
\end{subfigure}
\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_774_Oblique_12.tikz}
	\vspace{-1em}
	\caption{$\|e\| = 1^{\circ}$}
\end{subfigure}
\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_913_Lateral_342.tikz}
	\vspace{-1em}
	\caption{$ \|e\| = 14^{\circ}$}
\end{subfigure}
\begin{subfigure}[b]{0.245\textwidth}
	\input{fig/error_overview/Hand_388_Oblique_178.tikz}
	\vspace{-1em}
	\caption{$ \|e\| = 64^{\circ}$}
\end{subfigure}

\caption{Eight results calculated on samples from our test set. The images size varies because different X-ray detectors were used. Center point of the coordinate system lies in the center point of the detector. The red arrow shows our labeled orientation and the yellow arrow displays the predicted orientation. Image orientations in the fist two columns are correctly predicted with respect to a small error $\|e\| = \|\theta - \phi\| \in [0^{\circ}, 3^{\circ}]$. In the third column, two examples with a medium error $\|e\| \in (3^{\circ}, 45^{\circ}]$ are shown. The last column presents examples with a large error $\|e\| \in (45^{\circ}, 180^{\circ}]$.}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{CONCLUSION} 
Our work demonstrates that a pre-trained ResNet-50 has remarkable potential for orientation regression of anatomical structures in clinical X-ray images. The learned representations of the ResNet are an excellent starting point for an orientation regression. Our empirical results show that a fine-tuned regression ResNet-50 has a higher mean accuracy of $98.04\pm0.44\%$ than the previous state-of-the-art of $92.14\%$ from Baltruschat et al. (2016) when applied to the same dataset. We tested the method a second time only on AP/PA and oblique examinations. This resulted in a mean accuracy of $96.73\%$ which is still $1.31$ percentage points lower than our best result. In addition, the development time was much lower and this novel approach can be applied to several types of examinations. Comparing the e of our method to others, our method has a high runtime. This can be easily reduced by a magnitude, if GPUs were used.
Future work will include the investigation of additional examination types (e.g. chest and foot) and a comparison of different training techniques (e.g. fine-tuning vs. auto-encoders).

\if\versionLong0
\section{New or breakthrough work to be presented}
\begin{itemize}
	\item Generic method for orientation regression in X-ray images
	\item High alignment accuracy with a MAE of $2.79^{\circ}$ 
\end{itemize}
This work has not been submitted for publication or presentation elsewhere.
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\appendix    %>>>> this command starts appendixes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{MISCELLANEOUS FORMATTING DETAILS} \label{sec:misc}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\if\versionLong1
\acknowledgments     %>>>> equivalent to \section*{ACKNOWLEDGMENTS}       
 
The work has been carried out at Philips Medical Systems DMC GmbH, Hamburg, Germany.
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% References %%%%%

\bibliography{../references/references}   %>>>> bibliography data in report.bib
\bibliographystyle{spiebib}   %>>>> makes bibtex use spiebib.bst

\end{document}