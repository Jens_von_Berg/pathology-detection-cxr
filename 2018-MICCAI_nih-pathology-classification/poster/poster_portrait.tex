\documentclass[portrait,final,paperwidth=70cm,paperheight=90cm,fontscale=0.4]{baposter}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{calc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{relsize}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{bm}
\usepackage{url}
\usepackage[warn]{textcomp}
\usepackage[backend=bibtex,style=nature]{biblatex}
\bibliography{../../references/references}



\usepackage{multicol}
\usepackage{subcaption}

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{tikz}
\usepackage[]{graphicx}
\usetikzlibrary{plotmarks, arrows, calc}
\usepackage{tikzscale}
\usepackage{grffile}
\usepackage{caption}
\newlength\figureheight
\newlength\figurewidth
\setlength\figureheight{6cm}
\setlength\figurewidth{0.45\textwidth}

%\usepackage{times}
%\usepackage{helvet}
%\usepackage{bookman}
\usepackage{palatino}


\graphicspath{{images/}{../images/}}
\usetikzlibrary{calc}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Some math symbols used in the text
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Multicol Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\columnsep}{1.5em}
\setlength{\columnseprule}{0mm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save space in lists. Use this after the opening of the list
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\compresslist}{%
	\setlength{\itemsep}{1pt}%
	\setlength{\parskip}{0pt}%
	\setlength{\parsep}{0pt}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Begin of Document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%% Here starts the poster
	%%%---------------------------------------------------------------------------
	%%% Format it to your taste with the options
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Define some colors
	
	%\definecolor{lightblue}{cmyk}{0.83,0.24,0,0.12}
	%\definecolor{philips}{cmyk}{0.9352, 0.5602, 0, 0.1529}
	%\definecolor{philips}{RGB}{0, 117, 255}
	\definecolor{philips}{RGB}{11, 94, 216}
	\definecolor{philipslight}{cmyk}{0.186, 0.0886, 0, 0.0}
	
	
	%\definecolor{ibilight}{RGB}{193,216,237}
	%\definecolor{ibidark}{cmyk}{1.00,0.69,0.00,0.12}
	
	
	
	\hyphenation{resolution occlusions}
	%%
	\begin{poster}%
		% Poster Options
		{
			% Show grid to help with alignment
			grid=false,
			% Column spacing
			columns=2,
			colspacing=1.em,
			% Color style
			bgColorOne=white,
			bgColorTwo=white,
			borderColor=philipslight,
			headerColorOne=philips,
			headerColorTwo=philips,
			headerFontColor=white,
			boxColorOne=philipslight,
			boxColorTwo=philipslight,
			% Format of textbox
			textborder=none,
			% Format of text header
			eyecatcher=true,
			headerborder=none,
			headerheight=0.15\textheight,
			%  textfont=\sc, An example of changing the text font
			headershape=rectangle,
			%headershade=none,
			headerfont=\Large\bf, %Sans Serif
			textfont={\setlength{\parindent}{0em}},
			%boxshade=none,
			%  background=shade-tb,
			background=none,
			linewidth=2pt
		}
		% Eye Catcher
		{\includegraphics[width=5.0em]{images/Philips-Shield_cut.jpg}}
		%{\includegraphics[height=5em]{images/graph_occluded.pdf}}
		% Title
		{\bf\textsc{Patient\;Data\;Adapted\;Deep\;Learning\;\\
				\vspace{0.2em}for\;Multi-label\;Chest\;X-ray\;Classification}\vspace{0.5em}}
		% Authors
		{ {\bf \underline{Ivo M. Baltruschat}$^{1, 2, 3}$, Hannes Nickisch$^3$, Michael Grass$^3$, Tobias Knopp$^{1,2}$, and Axel Saalbach$^3$ }\\
			{$^1$Section for Biomedical Imaging, University Medical Center Hamburg-Eppendorf\\
				$^2$Institute for Biomedical Imaging, Hamburg University of Technology\\
				$^3$Philips Research, Hamburg, Germany}}
		% University logo
		{% The makebox allows the title to flow into the logo, this is a hack because of the L shaped logo.
			\includegraphics[width=5.0em]{images/uke-tuhh.png}
		}
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%%% Now define the boxes that make up the poster
		%%%---------------------------------------------------------------------------
		%%% Each box has a name and can be placed absolutely or relatively.
		%%% The only inconvenience is that you can only specify a relative position
		%%% towards an already declared box. So if you have a box attached to the
		%%% bottom, one to the top and a third one which should be in between, you
		%%% have to specify the top and bottom boxes before you specify the middle
		%%% box.
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%
		% A coloured circle useful as a bullet with an adjustably strong filling
		\newcommand{\colouredcircle}{%
			\tikz{\useasboundingbox (-0.2em,-0.32em) rectangle(0.2em,0.32em); \draw[draw=black,fill=philipslight,line width=0.03em] (0,0) circle(0.18em);}}
		
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{Abstract}{name=abstract, column=0, row=0, span=2}{
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			The increased availability of X-ray image archives (e.g. the ChestX-ray14 dataset from the NIH Clinical Center) has triggered a growing interest in deep learning techniques. To provide better insight into the different approaches, and their applications to chest X-ray classification, we investigate a powerful network architecture in detail: the ResNet-50. Building on prior work in this domain, we consider transfer learning with fine-tuning as well as the training of a dedicated X-ray network from scratch. To leverage the high spatial resolutions of X-ray data, we also include an extended ResNet-50 architecture, and a network integrating non-image data (patient age, gender and acquisition type) in the classification process.
		
			In a systematic evaluation, using 5-fold re-sampling and a multi-label loss function, we evaluate the performance of the different approaches for pathology classification by ROC statistics. We observe a considerable spread in the achieved performance and conclude that the X-ray-specific ResNet-50, integrating non-image data yields the best overall results.
		}
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{Motivation and Challenges}{name=contribution, column=0, below=abstract}{%, below=abstract}{
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			\colouredcircle\; In the UK, 23,000 chest X-rays (CXRs) were not formally reviewed by a radiologist %or clinician
			
			\colouredcircle\; It is expected that the demand for CXR readings will increase
			
			\colouredcircle\; Providing an system with high sensitivity to reduce the workload is desired
			
			\colouredcircle\; Pathologies in CXRs have a very diverse appearenc (i.e. size, location, etc.)
			
		}
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{Architecture}{name=pretraining, column=0, below=contribution}{
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			\colouredcircle\; CXR-Net based on ResNet-50 architecture:
			\vspace{-1.0em}
			\begin{enumerate}
				\compresslist
				\item Larger input size (i.e. for the detection of small structures, which could be indicative of a pathology)
				\item Included non-image features (i.e. radiologists also use information beyond the image for their diagnosis)
				\item Trained from scratch on ChestXray14 \cite{Wang2017short} (CXR14)  (i.e. learning X-ray specific representation)
			\end{enumerate}
			
			\begin{center}
				\includegraphics[width=\textwidth]{images/networkMiccai_2}
			\end{center}
			
			\colouredcircle\; Class-averaged binary cross entropy as loss function:
			\vspace{-1.0em}
			\begin{equation*}
			\ell (\vec{y}, \vec{f})= \frac{1}{M}\sum_{m=1}^{M} H[y_{m}, f_{m}], \; \text{with} \; H[y,f] = -y \log f -(1-y)\log (1-f).
			\end{equation*}
		}
		
%		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		\headerbox{Architectures}{name=network, column=0, below=pretraining}{
%			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			\colouredcircle\; Representation of orientation angles $\theta$ and $\phi$ via the embedding $z(\theta)=(\cos(\theta),\sin(\theta))$
%			
%			\colouredcircle\; Class-averaged binary cross entropy as loss function:
%			\begin{equation*}
%			\ell (\vec{y}, \vec{f})= \frac{1}{M}\sum_{m=1}^{M} H[y_{m}, f_{m}], \; \text{with} \; H[y,f] = -y \log f -(1-y)\log (1-f).
%			\end{equation*}
%			\colouredcircle\; ResNet\cite{He2015} with 50 layers, trained on the ImageNet\cite{Russakovsky2015} dataset
%			
%			\quad\colouredcircle\; Replacement of last ResNet layer with a linear layer for orientation regression
%			
%			\quad\colouredcircle\; Off-the-shelf: Adaptation of the linear layer only
%			
%			\quad\colouredcircle\; Fine-tuned: Adaptation of the linear layer and the last nine convolutional layers
%			
%		}
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{Results}{name=linTransformation, column=0, below=pretraining}{
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			\colouredcircle\; Evaluation on entire corpus of CXR14 and comparision to baseline experiments
			
			\colouredcircle\; 5 times re-sampling scheme with patient stratification for training, validation and testing (70\%, 10\%, and 30\% respectivly)
			\begin{center}
				\small 
				\begin{tabular}{l c c | c c }
					
					\setlength{\tabcolsep}{5pt}
					\centering
					%\noalign{\smallskip}
					& \multicolumn{2}{c|}{Without non-image features} & \multicolumn{2}{c}{With non-image features} \\
					\textbf{Pathology} &  FT & CXR-Net &  FT & CXR-Net \\
					%\noalign{\smallskip}
					\hline
					
					\textbf{Cardiomegaly}
					& $88.5 \pm 0.7$
					& $89.7 \pm 0.3$
					& $88.4 \pm 0.8$
					& $\mathbf{89.8 \pm 0.8}$ \\
					
					\textbf{Emphysema}
					& $89.2 \pm 1.0$
					& $88.3 \pm 1.3$
					& $\mathbf{89.4 \pm 1.2}$
					& $89.1 \pm 1.2$ \\
					
					\textbf{Edema}
					& $\mathbf{89.1 \pm 0.4}$
					& $88.8 \pm 0.5$
					& $\mathbf{89.1 \pm 0.7}$
					& $88.9 \pm 0.3$ \\
					
					\textbf{Hernia}
					& $85.5 \pm 3.8$
					& $87.5 \pm 4.5$
					& $88.2 \pm 3.2$
					& $\mathbf{89.6 \pm 4.4}$ \\
					
					\textbf{Pneumothorax}
					& $\mathbf{87.0 \pm 0.8}$
					& $85.9 \pm 0.9$
					& $86.5 \pm 0.6$
					& $85.9 \pm 1.1$ \\
					
					\textbf{Effusion}
					& $87.1 \pm 0.2$
					& $\mathbf{87.6 \pm 0.2}$
					& $87.2 \pm 0.3$
					& $87.3 \pm 0.3$ \\
					
					\textbf{Mass}
					& $82.2 \pm 1.0$
					& $\mathbf{83.9 \pm 0.9}$
					& $82.2 \pm 1.0$
					& $83.2 \pm 0.3$ \\
					
					\textbf{Fibrosis}
					& $\mathbf{80.0 \pm 0.9}$
					& $79.2 \pm 1.6$
					& $\mathbf{80.0 \pm 0.9}$
					& $78.9 \pm 0.5$ \\
					
					\textbf{Atelectasis}
					& $\mathbf{80.3 \pm 0.7}$
					& $79.2 \pm 0.7$
					& $80.1 \pm 0.6$
					& $79.1 \pm 0.4$ \\
					
					\textbf{Consolidation}
					& $79.5 \pm 0.5$
					& $\mathbf{80.0 \pm 0.3}$
					& $79.6 \pm 0.5$
					& $\mathbf{80.0 \pm 0.7}$ \\
					
					\textbf{Pleural Thicken.}
					& $\mathbf{79.0 \pm 0.7}$
					& $78.0 \pm 1.1$
					& $78.6 \pm 1.1$
					& $77.1 \pm 1.3$ \\
					
					\textbf{Nodule}
					& $72.6 \pm 0.9$
					& $75.1 \pm 1.3$
					& $74.7 \pm 0.6$
					& $\mathbf{75.8 \pm 1.4}$ \\
					
					\textbf{Pneumonia}
					& $74.4 \pm 1.6$
					& $75.3 \pm 2.2$
					& $73.3 \pm 1.3$
					& $\mathbf{76.7 \pm 1.5}$ \\
					
					\textbf{Infiltration}
					& $69.9 \pm 0.6$
					& $\mathbf{70.2 \pm 0.5}$
					& $\mathbf{70.2 \pm 0.2}$
					& $70.0 \pm 0.7$\\
					
					\hline
					%\noalign{\smallskip}
					
					\textbf{Average}
					& $81.7 \pm 1.0$
					& $82.1 \pm 1.2$
					& $82.0 \pm 0.9$
					& $\mathbf{82.2 \pm 1.1}$\\
					
					\textbf{No Findings}
					& $76.9 \pm 0.5$
					& $\mathbf{77.1 \pm 0.4}$
					& $76.8 \pm 0.4$
					& $\mathbf{77.1 \pm 0.3}$\\
					
				\end{tabular}

			\end{center}
			\colouredcircle\; All our experiments with non-image features slightly increase the AUC on average to its counterpart
		}	
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{Comparision to Others}{name=results, column=1, below=abstract}{%, below=abstract}{
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			\colouredcircle\; AUC for diffrent pathologies (min and max over folds as error bars)
			\begin{center}
				\includegraphics[width=0.53\textwidth]{images/auc_compare_star}
			\end{center}
		}
	
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{Visual Example of Grad-CAM Results}{name=example, column=1, below=results}{
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			\colouredcircle\;  Example image of "pneumothorax" with a drain from the testset
			\begin{center}
				\begin{minipage}[b]{\textwidth}
					\centering
					\includegraphics[width=0.49\textwidth]{images/00000827_019.png_pneumo0_cut.png}
					\quad
					\includegraphics[width=0.403\textwidth]{images/00000827_019.png-pneumo0.jpg}
				\end{minipage}\hfill
				%\vspace{-2.0em}
				%\begin{multicols}{1}
				
				
				%\end{multicols}
				
			\end{center}
			\colouredcircle\; As discussed by \cite{Luke2017} the quality of the labels, and their precise medical interpretation might be a limiting factor
		}
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{Conclusion and Outlook}{name=outlook, column=1, below=example}{
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
			\colouredcircle\; Best overall results can be reported for the model that is exclusively trained with CXRs and incorporating non-image data
			
			\colouredcircle\; Our fine-tuned ResNet-50 model achieves state-of-the-art results in four out of fourteen classes compared to \cite{Rajpurkar2017short}
			\\
			
			Future work:\\
			\quad\colouredcircle\; Investigation of practical use of deep learning in clinical practice\\
			\quad\colouredcircle\; New architectures for leveraging label dependencies\\
			\quad\colouredcircle\; Incorporating segmentation information
		}
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\headerbox{References}{name=refAck, column=1 ,below=outlook}{
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			\renewcommand*{\bibfont}{\scriptsize}
			\setlength{\bibitemsep}{0.1em}
			\printbibliography[heading=none]
		}
		
%		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		\headerbox{References}{name=refAck, column=0 ,below=linTransformation}{
%			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			\renewcommand*{\bibfont}{\scriptsize}
%			\setlength{\bibitemsep}{0.1em}
%			\printbibliography[heading=none]
%		}
		
%		\setkeys[ba]{posterbox}{
%			% Position
%			column=0,row=0,span=1,
%			below=notset,above=notset,
%			bottomaligned=notset,
%			aligned=notset,
%			height=auto,
%			% Name
%			name=noname,
%			% Box design: border:
%			linewidth=2pt,
%			borderColor=yellow,
%			cornerradius=1em,
%			% text box:
%			textfont={},
%			boxshade=plain,
%			boxColorOne=white,
%			boxColorTwo=white,
%			textborder=none,
%			boxpadding=0.0em,
%			% header
%			headerfont=\scshape\Large,% or headerfont=\color{white}\textsf\textbf
%			headerFontColor=black,
%			headerColorOne=red,
%			headerColorTwo=brown,
%			headershape=rectangle,
%			headershade=shadeLR,
%			headerborder=none,
%			boxheaderheight=0em,
%		}{}
%		
%			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		\headerbox{}{name=logo, column=0 ,below=refAck}{
%			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			\begin{minipage}{\linewidth}
%				%\hspace{5em}
%				\includegraphics[height=5.5em]{images/Philips_logo.png}
%			\end{minipage}
%		}
%		
%		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%		\headerbox{}{name=logo, column=1 ,below=refAck}{
%			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			\begin{minipage}{\linewidth}
%				%\hspace{5em}
%				\includegraphics[height=5.5em]{images/logo_imi_cut.png}
%				\vspace{0.3em}
%			\end{minipage}
%		}
		
	\end{poster}
	
\end{document}
