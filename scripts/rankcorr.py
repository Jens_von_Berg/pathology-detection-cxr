# -*- coding: utf-8 -*-
""" Compare classifiers using rank correlation.

Created: Fri Feb 23 10:27:59 2018 by Hannes Nickisch, Philips Research Hamburg.
"""

import numpy as np
import scipy.stats

n,d = 22000,2                                # number of images and pathologies
x = np.random.randn(n,d)                     # scores of classifier 1
y = x + 0.2*np.random.randn(n,d)             # scores of classifier 2

""" compare classifier scores """
r,p = scipy.stats.spearmanr(x,y)          # compute Spearman's rank correlation
r,p = np.diag(r[d:,:d]),np.diag(p[d:,:d])                  # clip relevant part

print(np.mean(r))                                    # show average correlation

c = x>0                                      # class labels

print scipy.stats.kendalltau(x,y)