import os
import csv
import numpy
import errno
import skimage
import numpy as np
from skimage.io import imread, imsave
from skimage import transform
from skimage import exposure
from skimage import util
from skimage import img_as_uint
import xml.etree.cElementTree as et
import xml.dom.minidom
import SimpleITK

################################################
################################################

base_folder = os.path.dirname(os.path.realpath(__file__))
output_path = os.path.join("./data")
output_path_image = os.path.join("./data", "images")

input_training = os.path.join(base_folder, "Indiana_front_Cardiomegaly_training_4.txt")
input_testing = os.path.join(base_folder, 'Indiana_front_Cardiomegaly_testing_4.txt')

################################################
################################################

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def saveMeanCNTK(fname, data):
    imgSize = 224
    channels = 1
    root = et.Element('opencv_storage')
    et.SubElement(root, 'Channel').text = str(channels)
    et.SubElement(root, 'Row').text = str(imgSize)
    et.SubElement(root, 'Col').text = str(imgSize)
    meanImg = et.SubElement(root, 'MeanImg', type_id='opencv-matrix')
    et.SubElement(meanImg, 'rows').text = '1'
    et.SubElement(meanImg, 'cols').text = str(imgSize * imgSize * channels)
    et.SubElement(meanImg, 'dt').text = 'f'
    print(data.shape)
    et.SubElement(meanImg, 'data').text = ' '.join(['%e' % n for n in numpy.reshape(data, (imgSize * imgSize * channels))])

    tree = et.ElementTree(root)
    tree.write(fname)
    x = xml.dom.minidom.parse(fname)
    with open(fname, 'w') as f:
        f.write(x.toprettyxml(indent = '  '))

def pre_processing(input_data, meanFile=True, rerun = False, image_size = 256):
    out_map = open(os.path.join(output_path, os.path.basename(input_data)), 'w')
    writer = csv.writer(out_map, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
    # load file list
    with open(input_data) as list_data:
        # loop through all the files
        make_sure_path_exists(output_path_image)

        for line in list_data:
            print(line)
            line = line.strip()
            seq_id, data = line.split('\t', 1)
            data = data.split('\t')

            # PNG and Txt-Filename
            out_file = os.path.basename(data[0]).split('.')[0] + '.nii'
            if not os.path.isfile(os.path.join(output_path_image, out_file)) or rerun:
                # read the file. SimpleITK images are ordered in x, y, z
                raw_data = SimpleITK.ReadImage(data[0])
                ## pre processing

                # resize to x,y
                if raw_data.GetDimension() == 3:
                    t = SimpleITK.ScaleTransform(3, (raw_data.GetWidth() / image_size, raw_data.GetHeight() / image_size, 1))
                    resized_image = SimpleITK.RegionOfInterest(SimpleITK.Resample(raw_data, t, SimpleITK.sitkLinear, SimpleITK.sitkFloat32), [image_size, image_size, 1], [0, 0, 0])
                else:
                    t = SimpleITK.ScaleTransform(2, (raw_data.GetWidth() / image_size, raw_data.GetHeight() / image_size))
                    resized_image = SimpleITK.RegionOfInterest(SimpleITK.Resample(raw_data, t, SimpleITK.sitkLinear, SimpleITK.sitkFloat32), [image_size, image_size], [0, 0])

                # in numpy, an array is indexed in the opposite order (z, y, x).
                np_image = SimpleITK.GetArrayFromImage(resized_image)
                # check if image need to be inverted
                try:
                    photometric_interpretation = raw_data.GetMetaData("0028|0004")
                    if photometric_interpretation == 'MONOCHROME2 ':
                        np_image = skimage.util.invert(np_image)
                except:
                    pass

                # linear intensity transformation from 16bit to 8bit
                v_min, v_max = np.percentile(np_image, (0.5, 99.5))
                img_contrast = exposure.rescale_intensity(np_image, (v_min, v_max), (0, 255))
                    #img_eq = exposure.equalize_hist(img_contrast)
                out_image = SimpleITK.Cast(SimpleITK.GetImageFromArray(img_contrast), SimpleITK.sitkUInt8)
                SimpleITK.WriteImage(out_image,  os.path.join(output_path_image, out_file))
            writer.writerow([seq_id, os.path.join(output_path_image, out_file)] + data[1:])

    if meanFile and (not os.path.isfile(os.path.join(output_path, "training_cv-0_mean.nii")) or rerun):
        mean_image = SimpleITK.Image([256, 256, 1], SimpleITK.sitkFloat64)
        counter = 0
        # load file list
        with open(input_data) as list_data:
            # loop through all the files
            for line in list_data:
                print(line)
                line = line.strip()
                seq_id, data = line.split('\t', 1)
                data = data.split('\t')
                file_name = os.path.basename(data[0]).split('.')[0] + '.nii'
                input_image = SimpleITK.ReadImage(os.path.join(output_path_image, file_name), SimpleITK.sitkFloat64)
                mean_image += input_image
                counter += 1
        mean_image = SimpleITK.Cast(mean_image / counter, SimpleITK.sitkUInt8)
        SimpleITK.WriteImage(mean_image, os.path.join(output_path, "training_crossVal0_meanImage.nii"))

if __name__ == "__main__":
    pre_processing(input_training, meanFile=True)
    pre_processing(input_testing, meanFile=False)